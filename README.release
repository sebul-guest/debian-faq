
Changes to make in FAQ in preparation for a release:
----------------------------------------------------

- Modify debian-faq.ent  to adjust the name of &release;, &releasename;,
  &oldreleasename; and &nextreleasename;
  
- Adjust the number of developers. 
 
  This is a difficult number to calculate, there are several ways:

  + Use the numbers provided by https://contributors.debian.org

  + Use the numbers from Debian LDAP database

  + Use the number from latest Debian election

  + calculate the number based on the number of GPG keys in the keyring, check
    www.debian.org/devel/people for a complete list and remove the groups. The
    following perl snippet  might be of use:

  $ lynx -source  http://www.debian.org/devel/people >/tmp/people 
  $ cat /tmp/people |
    perl -ne  'if ( /^\<dt\>/ && /\<strong\>(.*?)\<\/strong\>/) \
               { $name=$1; $name=~ s/\<.*?\>//g; print $name."\n"; }' |
    wc -l
  
  and substract 140 (the current number of group maintainers) from the
  count (look for DDs whose contact e-mail is Alioth since these
  tend to be team addresses)

- Adjust the package numbers of debian-faq.ent:

  For packages:
  ( the count-release.sh script should do this automatically if
    properly adjusted )

  [ First calculate the number of packages in the distribution 
  (main+contrib+non-free): ]
   $ total=`grep-available -s Package -F section -r ".*" |wc -l`
  [ Then contrib and non-free ]
   $ nonfree=`grep-available -s Package -F section -r "non-free/.*" |wc -l`
   $ contrib=`grep-available -s Package -F section -r "contrib/.*" |wc -l`
 
  contrib-nonfree-pkgs   --> $(($nonfree+$contrib))
  main-pkgs   --> $(($total-$nonfree-$contrib))
  all-pkgs    --> $(($total-$nonfree))

  Note: The grep-available runs should be done in a &release; system
  of course or providing it a Packages file from
  /debian/dists/$RELEASE/main/binary-$ARCH/Packages
  /debian/dists/$RELEASE/contrib/binary-$ARCH/Packages
  /debian/dists/$RELEASE/non-free/binary-$ARCH/Packages

  The biggest $ARCH should be used here (typically, amd64) since the numbers
  will vary from architecture to architecture.

- Review the number of architectures release. Check the Release Notes at:
  https://www.debian.org/releases/stable/

- Review the "oldcodenames" section in en/ftparchive.dbk to point to the
  oldreleasename and make sure that the "sourceforcodenames" section
  contains &releasename; and the next release's name's 

- Review the contents of en/nextrelease.dbk. There is not always discussion on
  debian-devel of wanted features for the next release so spark the
  discussion if needed be to get a list of things that _might_ end up
  in the next release. Use the Release Goals set by release managers
  if there are any.

  More information for Release Goals here: https://wiki.debian.org/ReleaseGoals/
  
-- Javier Fernandez-Sanguino <jfs@debian.org>
 Fri, 12 Jul 2019 00:57:24 +0200
