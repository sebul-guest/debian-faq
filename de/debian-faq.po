# Translation of debian-faq into German.
#
# Copyright © Free Software Foundation, Inc.
# This file is distributed under the same license as the debian-faq package.
#
# Translators:
# Christian Britz <cbritz@gmx.net>
# Nico Golde
# Claas Felix Beyersdorf <claas@beyersdorf-bs.de>
# Andre Appel <andre@family-appel.de>
# Franziska Schröder <fran-online@gmx.de>
# Jonas E. Huber <jonas@hubjo.org>
# Jens Seidel <jensseidel@users.sf.net>
# Benjamin Eckenfels <benjamin.eckenfels@gmx.de
# Martin Horoba <mail4mh@gmx.de>
# Helge Kreutzmann <debian@helgefjell.de>
# Holger Wansing <linux@wansing-online.de>, 2015 - 2017.
# Dr. Tobias Quathamer <toddy@debian.org>, 2019, 2020.
msgid ""
msgstr ""
"Project-Id-Version: debian-faq 5.0.3\n"
"POT-Creation-Date: 2020-12-28 22:11+0100\n"
"PO-Revision-Date: 2020-12-28 21:59+0100\n"
"Last-Translator: Dr. Tobias Quathamer <toddy@debian.org>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.08.2\n"

#. type: Attribute 'lang' of: <book>
#: en/debian-faq.dbk:9
msgid "en"
msgstr "de"

#. type: Content of: <book><title>
#: en/debian-faq.dbk:11
msgid "The &debian; FAQ"
msgstr "Die &debian;-FAQ"

#. type: Content of: <book><bookinfo><authorgroup><author><authorblurb><para>
#: en/debian-faq.dbk:18
msgid ""
"Authors are listed at <link linkend=\"authors\">Debian FAQ Authors</link>."
msgstr ""
"Die Autoren finden Sie in <link linkend=\"authors\">Autoren der Debian-FAQ</"
"link>."

#. type: Content of: <book><bookinfo><releaseinfo>
#: en/debian-faq.dbk:23
msgid "Version 11.0"
msgstr "Version 11.0"

#. type: Content of: <book><bookinfo><pubdate>
#: en/debian-faq.dbk:24
msgid "January 2021"
msgstr "Januar 2021"

#. type: Content of: <book><bookinfo><abstract><para>
#: en/debian-faq.dbk:28
msgid "This document answers questions frequently asked about &debian;."
msgstr "Dieses Dokument beantwortet häufig gestellte Fragen über &debian;."

#. type: Content of: <book><bookinfo>
#: en/debian-faq.dbk:32
msgid ""
"<copyright> <year>1996-2021</year> <holder>Software in the Public Interest</"
"holder> </copyright>"
msgstr ""
"<copyright> <year>1996-2021</year> <holder>Software in the Public Interest</"
"holder> </copyright>"

#. type: Content of: <book><bookinfo><legalnotice><para>
#: en/debian-faq.dbk:39
msgid ""
"Permission is granted to make and distribute verbatim copies of this "
"document provided the copyright notice and this permission notice are "
"preserved on all copies."
msgstr ""
"Permission is granted to make and distribute verbatim copies of this "
"document provided the copyright notice and this permission notice are "
"preserved on all copies."

#. type: Content of: <book><bookinfo><legalnotice><para>
#: en/debian-faq.dbk:44
msgid ""
"Permission is granted to copy and distribute modified versions of this "
"document under the conditions for verbatim copying, provided that the entire "
"resulting derived work is distributed under the terms of a permission notice "
"identical to this one."
msgstr ""
"Permission is granted to copy and distribute modified versions of this "
"document under the conditions for verbatim copying, provided that the entire "
"resulting derived work is distributed under the terms of a permission notice "
"identical to this one."

#. type: Content of: <book><bookinfo><legalnotice><para>
#: en/debian-faq.dbk:50
msgid ""
"Permission is granted to copy and distribute translations of this document "
"into another language, under the above conditions for modified versions, "
"except that this permission notice may be included in translations approved "
"by the Free Software Foundation instead of in the original English."
msgstr ""
"Permission is granted to copy and distribute translations of this document "
"into another language, under the above conditions for modified versions, "
"except that this permission notice may be included in translations approved "
"by the Free Software Foundation instead of in the original English."

#~ msgid "12 August 2019"
#~ msgstr "12. August 2019"
