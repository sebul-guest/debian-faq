<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
    "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
    <!ENTITY % shareddata SYSTEM "../debian-faq.ent" > %shareddata;
]>

<chapter id="basic-defs"><title>Definitions and overview</title>
<section id="whatisfaq"><title>What is this FAQ?</title>
<para>
This document gives frequently asked questions (with their answers!) about the
Debian distribution (&debian; and others) and about the Debian project.
If applicable, pointers to other documentation will be given: we won't quote
large parts of external documentation in this document.  You'll find out that
some answers assume some knowledge of Unix-like operating systems.  We'll try
to assume as little prior knowledge as possible: answers to general beginners
questions will be kept simple.
</para>
<para>
If you can't find what you're looking for in this FAQ, be sure to check out
<xref linkend="debiandocs"/>.  If even that doesn't help, refer to <xref
linkend="feedback"/>.
</para>
</section>

<section id="whatisdebian"><title>What is &debian;?</title>
<para>
&debian; is a particular <emphasis>distribution</emphasis> of the Linux
operating system, and numerous packages that run on it.
</para>
<para>
&debian; is:
</para>
<itemizedlist>
<listitem>
<para>
<emphasis role="strong">full featured</emphasis>: Debian includes more than
&packages-total; software packages at present.  Users can select which packages to
install; Debian provides a tool for this purpose.  You can find a list and
descriptions of the packages currently available in Debian at any of the Debian
<ulink url="https://www.debian.org/distrib/ftplist">mirror sites</ulink>.
</para>
</listitem>
<listitem>
<para>
<emphasis role="strong">free to use and redistribute</emphasis>: There is no
consortium membership or payment required to participate in its distribution
and development.  All packages that are formally part of &debian; are
free to redistribute, usually under terms specified by the GNU General Public
License.
</para>
<para>
The Debian archives also carry approximately &packages-contrib-nonfree; software packages (in the
<literal>non-free</literal> and <literal>contrib</literal> sections), which are
distributable under specific terms included with each package.
</para>
</listitem>
<listitem>
<para>
<emphasis role="strong">dynamic</emphasis>: With about &developers; volunteers
constantly contributing new and improved code, Debian is evolving rapidly.  The
archives are updated twice every day.
</para>
</listitem>
</itemizedlist>
<para>
Most Linux users run a specific <emphasis>distribution</emphasis> of Linux,
like &debian;.  However, in principle, users could obtain the Linux
kernel via the Internet or from elsewhere, and compile it themselves.  They
could then obtain source code for many applications in the same way, compile
the programs, then install them into their systems.  For complicated programs,
this process can be not only time-consuming but error-prone.  To avoid it,
users often choose to obtain the operating system and the application packages
from one of the Linux distributors.  What distinguishes the various Linux
distributors are the software, protocols, and practices they use for packaging,
installing, and tracking applications packages on users' systems, combined with
installation and maintenance tools, documentation, and other services.
</para>
<para>
&debian; is the result of a volunteer effort to create a free,
high-quality Unix-compatible operating system, complete with a suite of
applications.  The idea of a free Unix-like system originates from the GNU
project, and many of the applications that make &debian; so useful were
developed by the GNU project.
</para>
<para>
For Debian, free has the GNUish meaning (see the <ulink
url="https://www.debian.org/social_contract#guidelines">Debian Free Software
Guidelines</ulink>).  When we speak of free software, we are referring to
freedom, not price.  Free software means that you have the freedom to
distribute copies of free software, that you receive source code or can get it
if you want it, that you can change the software or use pieces of it in new
free programs; and that you know you can do these things.
</para>
<para>
The Debian Project was created by Ian Murdock in 1993, initially under the
sponsorship of the Free Software Foundation's GNU project.  Today, Debian's
developers think of it as a direct descendent of the GNU project.
</para>
<para>
Although &debian; itself is free software, it is a base upon which
value-added Linux distributions can be built.  By providing a reliable,
full-featured base system, Debian provides Linux users with increased
compatibility, and allows Linux distribution creators to eliminate duplication
of effort and focus on the things that make their distribution special.  See
<xref linkend="childistro"/> for more information.
</para>
</section>

<section id="linux"><title>OK, now I know what Debian is... what is Linux?!</title>
<para>
In short, Linux is the kernel of a Unix-like operating system.  It was
originally designed for 386 (and better) PCs; today Linux also runs on a dozen
of other systems.  Linux is written by Linus Torvalds and many computer
scientists around the world.
</para>
<para>
Besides its kernel, a "Linux" system usually has:
</para>
<itemizedlist>
<listitem>
<para>
a file system that follows the Linux Filesystem Hierarchy Standard <ulink url="&url-pathname-fhs;"/>.
</para>
</listitem>
<listitem>
<para>
a wide range of Unix utilities, many of which have been developed by the GNU
project and the Free Software Foundation.
</para>
</listitem>
</itemizedlist>
<para>
The combination of the Linux kernel, the file system, the GNU and FSF
utilities, and the other utilities are designed to achieve compliance with the
POSIX (IEEE 1003.1) standard; see <xref linkend="otherunices"/>.
</para>
<para>
For more information about Linux, see <ulink
url="https://www.linux.org/info/">What is Linux</ulink> by <ulink
url="https://www.linux.org/">Linux Online</ulink>.
</para>
</section>

<section id="non-linux"><title>Does Debian just do GNU/Linux?</title>
<para>
Currently, Debian is only available for Linux, but with Debian GNU/Hurd and
Debian on BSD kernels, we have started to offer non-Linux-based OSes as a
development, server and desktop platform, too.  However, these non-linux ports
are not officially released yet.
</para>
<para>
The oldest porting effort is Debian GNU/Hurd.
</para>
<para>
The Hurd is a set of servers running on top of the GNU Mach microkernel.
Together they build the base for the GNU operating system.
</para>
<para>
Please see <ulink url="&url-gnu-hurd;"/>
for more information about the GNU/Hurd in general, and <ulink url="&url-debian-hurd;"/>
for more information about Debian GNU/Hurd.
</para>
<para>
A second effort is the port to a BSD kernel.  People are working with the
FreeBSD kernel.
</para>
<para>
See <ulink url="https://www.debian.org/ports/#nonlinux"/>
for more information about these non-linux ports.
</para>
</section>

<section id="difference"><title>What is the difference between &debian; and other Linux distributions? Why should I choose Debian over some other distribution?</title>
<para>
These key features distinguish Debian from other Linux distributions:
</para>
<variablelist>
<varlistentry>
<term>Freedom:</term>
<listitem>
<para>
As stated in the <ulink url="&url-debian-social-contract;">Debian
Social Contract</ulink>, Debian will remain 100% free.  Debian is very strict
about shipping truly free software.  The guidelines used to determine if a work
is "free" are provided in <ulink
url="https://www.debian.org/social_contract#guidelines">The Debian Free Software
Guidelines (DFSG)</ulink>.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term>The Debian package maintenance system:</term>
<listitem>
<para>
The entire system, or any individual component of it, can be upgraded in place
without reformatting, without losing custom configuration files, and (in most
cases) without rebooting the system.  Most Linux distributions available today
have some kind of package maintenance system; the Debian package maintenance
system is unique and particularly robust (see <xref linkend="pkg-basics"/>).
</para>
</listitem>
</varlistentry>
<varlistentry>
<term>Open development:</term>
<listitem>
<para>
Whereas many other Linux distributions are developed by individuals, small,
closed groups, or commercial vendors, Debian is a major Linux distribution that
is being developed by an association of individuals who have made common cause
to create a free operating system, in the same spirit as Linux and other free
software.
</para>
<para>
More than &developers; volunteer package maintainers are working on over &packages-total; packages
and improving &debian;.  The Debian developers contribute to the
project not by writing new applications (in most cases), but by packaging
existing software according to the standards of the project, by communicating
bug reports to upstream developers, and by providing user support.  See also
additional information on how to become a contributor in <xref
linkend="contributing"/>.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term>The Universal Operating System:</term>
<listitem>
<para>
Debian comes with <ulink url="https://packages.debian.org/stable/">more than
&packages-total; packages</ulink> and runs on <ulink url="https://www.debian.org/ports/">&number-archs;
architectures</ulink>.  This is far more than is available for any other
GNU/Linux distribution.  See <xref linkend="apps"/> for an overview of the
provided software and see <xref linkend="arches"/> for a description of the
supported hardware platforms.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term>The Bug Tracking System:</term>
<listitem>
<para>
The geographical dispersion of the Debian developers required sophisticated
tools and quick communication of bugs and bug-fixes to accelerate the
development of the system.  Users are encouraged to send bugs in a formal
style, which are quickly accessible by WWW archives or via e-mail.  See
additional information in this FAQ on the management of the bug log in <xref
linkend="buglogs"/>.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term>The Debian Policy:</term>
<listitem>
<para>
Debian has an extensive specification of our standards of quality, the Debian
Policy.  This document defines the qualities and standards to which we hold
Debian packages.
</para>
</listitem>
</varlistentry>
</variablelist>
<para>
For additional information about this, please see our web page about <ulink
url="https://www.debian.org/intro/why_debian">reasons to choose Debian</ulink>.
</para>
</section>

<section id="gnu"><title>How does the Debian project fit in or compare with the Free Software Foundation's GNU project?</title>
<para>
The Debian system builds on the ideals of free software first championed by the
<ulink url="https://www.gnu.org/">Free Software Foundation</ulink> and in
particular by <ulink url="https://www.stallman.org/">Richard Stallman</ulink>.
FSF's powerful system development tools, utilities, and applications are also a
key part of the Debian system.
</para>
<para>
The Debian Project is a separate entity from the FSF, however we communicate
regularly and cooperate on various projects.  The FSF explicitly requested that
we call our system "&debian;", and we are happy to comply with that
request.
</para>
<para>
The FSF's long-standing objective is to develop a new operating system called
GNU, based on <ulink url="&url-gnu-hurd;">Hurd</ulink>.
Debian is working with FSF on this system, called <ulink
url="&url-debian-hurd;">Debian GNU/Hurd</ulink>.
</para>
</section>

<section id="pronunciation"><title>How does one pronounce Debian and what does this word mean?</title>
<para>
The project name is pronounced Deb'-ee-en, with a short e in Deb, and emphasis
on the first syllable.  This word is a contraction of the names of Debra and
Ian Murdock, who founded the project.  (Dictionaries seem to offer some
ambiguity in the pronunciation of Ian (!), but Ian prefers ee'-en.)
</para>
</section>

</chapter>

