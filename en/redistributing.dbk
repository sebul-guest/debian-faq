<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
    "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
    <!ENTITY % shareddata SYSTEM "../debian-faq.ent" > %shareddata;
]>

<chapter id="redistributing"><title>Redistributing &debian; in a commercial product</title>
<section id="sellcds"><title>Can I make and sell Debian CDs?</title>
<para>
Go ahead.  You do not need permission to distribute anything we have
<emphasis>released</emphasis>, so that you can master your CD as soon as the
beta-test ends.  You do not have to pay us anything.  Of course, all CD
manufacturers must honor the licenses of the programs in Debian.  For example,
many of the programs are licensed under the GPL, which requires you to
distribute their source code.
</para>
<para>
Also, we will publish a list of CD manufacturers who donate money, software,
and time to the Debian project, and we will encourage users to buy from
manufacturers who donate, so it is good advertising to make donations.
</para>
</section>

<section id="packagednonfree"><title>Can Debian be packaged with non-free software?</title>
<para>
Yes.  While all the main components of Debian are free software, we provide a
non-free directory for programs that are not freely redistributable.
</para>
<para>
CD manufacturers <emphasis>may</emphasis> be able to distribute the programs we
have placed in that directory, depending on the license terms or their private
arrangements with the authors of those software packages.  CD manufacturers can
also distribute the non-free software they get from other sources on the same
CD.  This is nothing new: free and commercial software are distributed on the
same CD by many manufacturers now.  Of course we still encourage software
authors to release the programs they write as free software.
</para>
</section>

<section id="childistro"><title>I am making a special Linux distribution for a "vertical market". Can I use &debian; for the guts of a Linux system and add my own applications on top of it?</title>
<para>
Yes.  Debian-derived distributions are being created both in close cooperation
with the Debian project itself and by external parties.  One can use the <ulink
url="https://www.debian.org/blends/">Debian Pure Blends</ulink> framework to
work together with Debian; <ulink
url="https://wiki.debian.org/DebianEdu/">DebianEdu/Skolelinux</ulink> is one
such project.
</para>
<para>
There are several other Debian-derived distributions already on the market,
such as grml, LMDE (Linux Mint Debian Edition), Knoppix and Ubuntu, that are
targeted at a different kind of audience than the original &debian; is,
but use most of our components in their product.
</para>
<para>
Debian also provides a mechanism to allow developers and system administrators
to install local versions of selected files in such a way that they will not be
overwritten when other packages are upgraded.  This is discussed further in the
question on <xref linkend="divert"/>.
</para>
</section>

<section id="commercialdebs"><title>Can I put my commercial program in a Debian "package" so that it installs effortlessly on any Debian system?</title>
<para>
Go right ahead.  The package tool is free software; the packages may or may not
be free software, it can install them all.
</para>
</section>

</chapter>

