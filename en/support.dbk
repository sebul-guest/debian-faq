<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
    "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
    <!ENTITY % shareddata SYSTEM "../debian-faq.ent" > %shareddata;
]>

<chapter id="support"><title>Getting support for &debian;</title>
<section id="debiandocs"><title>What other documentation exists on and for a Debian system?</title>
<itemizedlist>
<listitem>
<para>
Installation instructions for the current release: see <ulink url="&url-debian-installmanual;"/>.
</para>
</listitem>
<listitem>
<para>
The &debian; reference covers many aspects of system administration
through shell-command examples.  Basic tutorials, tips, and other information
are provided for many different topics ranging from system administration to
programming.
</para>
<para>
Get it from the <systemitem role="package">debian-reference</systemitem>
package, or at <ulink url="https://www.debian.org/doc/user-manuals#quick-reference"/>.
</para>
</listitem>
<listitem>
<para>
The Debian Policy manual documents the policy requirements for the
distribution, i.e. the structure and contents of the Debian archive, several
design issues of the operating system etc.  It also includes the technical
requirements that each package must satisfy to be included in the distribution,
and documents the basic technical aspects of Debian binary and source packages.
</para>
<para>
Get it from the <systemitem role="package">debian-policy</systemitem> package,
or at <ulink url="https://www.debian.org/doc/devel-manuals#policy"/>.
</para>
</listitem>
<listitem>
<para>
Documentation developed by the Debian Documentation Project.  It is available
at <ulink url="&url-debian-doc;"/>
and includes user guides, administration guides and security guides for the
&debian; operating system.
</para>
</listitem>
<listitem>
<para>
Documentation on installed Debian packages: Most packages have files that are
unpacked into <literal>/usr/share/doc/PACKAGE</literal>.
</para>
</listitem>
<listitem>
<para>
Documentation on the Linux project: The Debian package <systemitem
role="package">doc-linux</systemitem> installs all of the most recent versions
of the HOWTOs and mini-HOWTOs from the <ulink url="http://www.tldp.org/">Linux
Documentation Project</ulink>.
</para>
</listitem>
<listitem>
<para>
Unix-style `man' pages: Most commands have manual pages written in the style of
the original Unix 'man' files.  For instance, to see the manual page for the
command `ls', execute <literal>man ls</literal>.  Execute <literal>man
man</literal> for more information on finding and viewing manual pages.
</para>
<para>
New Debian users should note that the 'man' pages of many general system
commands are not available until they install these packages:
</para>
<itemizedlist>
<listitem>
<para>
<literal>man-db</literal>, which contains the <literal>man</literal> program
itself, and other programs for manipulating the manual pages.
</para>
</listitem>
<listitem>
<para>
<literal>manpages</literal>, which contains the system manual pages.  (see
<xref linkend="nonenglish"/>).
</para>
</listitem>
</itemizedlist>
</listitem>
<listitem>
<para>
GNU-style `info' pages: User documentation for many commands, particularly GNU
tools, is available not in `man' pages, but in `info' files which can be read
by the GNU tool <literal>info</literal>, by running <literal>M-x info</literal>
within GNU Emacs, or with some other Info page viewer.
</para>
<para>
Its main advantage over the original `man' pages is that it is a hypertext
system.  It does <emphasis>not</emphasis> require the WWW, however;
<literal>info</literal> can be run from a plain text console.  It was designed
by Richard Stallman and preceded the WWW.
</para>
</listitem>
</itemizedlist>
<para>
Note that you may access a lot of documentation on your system by using a WWW
browser, through `dwww', `dhelp' or `doccentral' commands, found in respective
packages, or by using `yelp'.
</para>
</section>

<section id="onlineresources"><title>Are there any on-line resources for discussing Debian?</title>
<para>
Yes.  In fact, the main method of support Debian provides to our users is by
the way of e-mail.  We'll give some details on that, and mention some other
useful resources.  Even more resources are listed at the <ulink
url="&url-debian-support;">Debian Support webpage</ulink>.
</para>
<section id="s12.2.1"><title>Mailing lists</title>
<para>
There are a lot of <ulink
url="https://www.debian.org/MailingLists/">Debian-related mailing lists</ulink>.
</para>
<para>
On a system with the <systemitem role="package">doc-debian</systemitem> package
installed there is a complete list of mailing lists in
<filename>/usr/share/doc/debian/mailing-lists.txt</filename>.
</para>
<para>
Debian mailing lists are named following the pattern
debian-<replaceable>list-subject</replaceable>.  Examples are debian-announce,
debian-user, debian-news.  To subscribe to any list
debian-<replaceable>list-subject</replaceable>, send mail to
debian-<replaceable>list-subject</replaceable>-request@lists.debian.org with
the word "subscribe" in the Subject: header.  Be sure to remember to add
<emphasis>-request</emphasis> to the e-mail address when using this method to
subscribe or unsubscribe.  Otherwise your e-mail will go to the list itself,
which could be embarrassing or annoying, depending on your point of view.
</para>
<para>
You can subscribe to mailing lists using the <ulink
url="https://www.debian.org/MailingLists/subscribe">WWW form</ulink>.  You can
also un-subscribe using a <ulink
url="https://www.debian.org/MailingLists/unsubscribe">WWW form</ulink>.
</para>
<para>
The list manager's e-mail address is
<email>listmaster@lists.debian.org</email>, in case you have any trouble.
</para>
<para>
The mailing lists are public forums.  All e-mails sent to the lists are also
copied to the public archive, for anybody (even non-subscribers) to browse or
search.  Please make sure you never send any confidential or unlicensed
material to the lists.  This includes things like e-mail addresses.  Of
particular note is the fact that spammers have been known to abuse e-mail
addresses posted to our mailing lists.  See the <ulink
url="https://www.debian.org/MailingLists/#disclaimer">Mailing Lists Privacy
policy</ulink> for more information.
</para>
<para>
Archives of the Debian mailing lists are available via WWW at <ulink url="&url-lists-debian;"/>.
</para>
<section id="mailinglistconduct"><title>What is the code of conduct for the mailing lists?</title>
<para>
When using the Debian mailing lists, please follow these rules:
</para>
<itemizedlist>
<listitem>
<para>
Do not send spam.  See the <ulink
url="https://www.debian.org/MailingLists/#ads">Debian mailing list advertising
policy</ulink>.
</para>
</listitem>
<listitem>
<para>
Do not flame; it is not polite.  The people developing Debian are all
volunteers, donating their time, energy and money in an attempt to bring the
Debian project together.
</para>
</listitem>
<listitem>
<para>
Do not use foul language; besides, some people receive the lists via packet
radio, where swearing is illegal.
</para>
</listitem>
<listitem>
<para>
Make sure that you are using the proper list.  <emphasis>Never</emphasis> post
your (un)subscription requests to the mailing list itself.<footnote><para> Use
the debian-<replaceable>list-subject</replaceable>-REQUEST@lists.debian.org
address for that.  </para> </footnote>
</para>
</listitem>
<listitem>
<para>
See section <xref linkend="bugreport"/> for notes on reporting bugs.
</para>
</listitem>
</itemizedlist>
</section>

</section>

<section id="s12.2.2"><title>Web forum</title>
<para>
<ulink url="http://forums.debian.net/">Debian User Forums</ulink> provides web
forums on which you can submit questions about Debian and have them answered by
other users.  (It is not an officially part of the Debian project.)
</para>
</section>

<section id="s12.2.3"><title>Wiki</title>
<para>
Solutions to common problems, howtos, guides, tips and other documentation can
be found at the constantly changing <ulink url="https://wiki.debian.org/">Debian
Wiki</ulink>.
</para>
</section>

<section id="s12.2.4"><title>Maintainers</title>
<para>
Users can address questions to individual package maintainers using e-mail.  To
reach a maintainer of a package called xyz, send e-mail to
<emphasis>xyz@packages.debian.org</emphasis>.
</para>
</section>

<section id="s12.2.5"><title>Usenet newsgroups</title>
<para>
Users should post non-Debian-specific questions to one of the Linux USENET
groups, which are named comp.os.linux.* or linux.*.  There are several lists of
Linux Usenet newsgroups and other related resources on the WWW, e.g. on the
<ulink url="https://www.linux.org/docs/usenet.html">Linux Online</ulink> and
<ulink url="http://www.linuxjournal.com/helpdesk.php">LinuxJournal</ulink>
sites.
</para>
</section>

</section>

<section id="searchtools"><title>Is there a quick way to search for information on &debian;?</title>
<para>
There is a variety of search engines that serve documentation related to
Debian:
</para>
<itemizedlist>
<listitem>
<para>
<ulink url="https://search.debian.org/">Debian WWW search site</ulink>.
</para>
</listitem>
<listitem>
<para>
<ulink url="https://groups.google.com/">Google Groups</ulink>: a search engine
for newsgroups.
</para>
<para>
For example, to find out what experiences people have had with finding drivers
for NVIDIA graphic cards under Debian, try searching the phrase <literal>NVIDIA
Linux driver</literal>.  This will show you all the posts that contain these
strings, i.e. those where people discussed these topics.  If you add
<literal>Debian</literal> to those search strings, you'll also get the posts
specifically related to Debian.
</para>
</listitem>
<listitem>
<para>
Any of the common web spidering engines, such as <ulink
url="https://duckduckgo.com/">DuckDuckGo</ulink> or <ulink
url="https://www.google.com/">Google</ulink>, as long as you use the right
search terms.
</para>
<para>
For example, searching on the string "evince" gives a more detailed explanation
of this package than the brief description field in its control file.
</para>
</listitem>
</itemizedlist>
</section>

<section id="buglogs"><title>Are there logs of known bugs?</title>
<para>
Reports on unsolved (and closed) issues are publicly available: Debian
promissed to do so by stating "We will not hide problems" in the <ulink
url="&url-debian-social-contract;">Debian Social Contract</ulink>.
</para>
<para>
The &debian; distribution has a bug tracking system (BTS) which files
details of bugs reported by users and developers.  Each bug is given a number,
and is kept on file.  Once it has been dealt with, it is marked as such.
</para>
<para>
Copies of this information are available at <ulink url="https://www.debian.org/Bugs/"/>.
</para>
<para>
A mail server provides access to the bug tracking system database via e-mail.
In order to get the instructions, send an e-mail to request@bugs.debian.org
with "help" in the body.
</para>
</section>

<section id="bugreport"><title>How do I report a bug in Debian?</title>
<para>
If you have found a bug in Debian, please read the instructions for reporting a
bug in Debian.  These instructions can be obtained in one of several ways:
</para>
<itemizedlist>
<listitem>
<para>
From the WWW.  A copy of the instructions is shown at <ulink url="https://www.debian.org/Bugs/Reporting"/>.
</para>
</listitem>
<listitem>
<para>
On any Debian system with the <systemitem
role="package">doc-debian</systemitem> package installed.  The instructions are
in the file <filename>/usr/share/doc/debian/bug-reporting.txt</filename>.
</para>
</listitem>
</itemizedlist>
<para>
You can use the package <systemitem role="package">reportbug</systemitem> that
will guide you through the reporting process and mail the message to the proper
address, with some extra details about your system added automatically.  It
will also show you a list of bugs already reported to the package you are
reporting against in case your bug has been reported previously, so that you
can add additional information to the existing bug report.
</para>
<para>
Expect to get an automatic acknowledgement of your bug report.  It will also be
automatically given a bug tracking number, entered into the bug log and
forwarded to the debian-bugs-dist mailing list.
</para>
</section>

</chapter>

