<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="compat"><title>Cuestiones de Compatibilidad</title>
<section id="arches"><title>¿En qué arquitecturas/sistemas funciona Debian GNU/Linux?</title>
<para>
Debian GNU/Linux incluye el código fuente completo de todos los programas
incluidos, así que debería funcionar en todos los sistemas soportados por el
núcleo Linux; ver la <ulink
url="http://www.cl.cam.ac.uk/users/iwj10/linux-faq/">FAQ sobre Linux</ulink>
para más detalles.
</para>
<para>
La actual entrega de Debian GNU/Linux contiene una distribución completa de
ejecutables para las siguientes arquitecturas:
</para>
<para>
<emphasis>i386</emphasis>: ésta cubre PCs basados en procesadores compatibles
Intel, incluyendo los 386, 486, Pentium, Pentium Pro, Pentium II (tanto Klamath
como Celeron), Pentium III, Pentium IV de Intel y procesadores compatibles de
AMD, Cyrix y otros.
</para>
<para>
<emphasis>m68k</emphasis>: esto cubre Amigas y Ataris con un procesador
Motorola 680x0 para x>=2; con MMU.
</para>
<para>
<emphasis>alpha</emphasis>: sistemas Alpha de Compaq/Digital.
</para>
<para>
<emphasis>sparc</emphasis>: esto cubre sistemas SPARC de Sun y la mayoría de
los sistemas UltraSPARC.
</para>
<para>
<emphasis>powerpc</emphasis>: esto cubre algunas máquinas PowerPC de
IBM/Motorola, incluyendo máquinas CHRP, PowerMac y PReP.
</para>
<para>
<emphasis>arm</emphasis>: máquinas ARM y StrongARM.
</para>
<para>
<emphasis>mips</emphasis>: sistemas MIPS big-endian de SGI, Indy e Indigo2;
<emphasis>mipsel</emphasis>: máquinas MIPS little-endian, estaciones DEC de
Digital.
</para>
<para>
<emphasis>hppa</emphasis>: Máquinas PA-RISC de Hewlett-Packard (712, C3000,
L2000, A500).
</para>
<para>
<emphasis>ia64</emphasis>: sistemas IA-64 de Intel ("Itanium").
</para>
<para>
<emphasis>s390</emphasis>: sistemas "mainframe" S/390 de IBM.
</para>
<para>
Actualmente se está trabajando en el desarrollo de distribuciones de binarios
de Debian para las arquitecturas Sparc64 (UltraSparc nativo) y AMD de 64 bits.
</para>
<para>
Para más información sobre arranque, particionamiento del disco duro,
activación de tarjetas PCMCIA y cuestiones similares por favor siga las
instrucciones del Manual de Instalación, que está disponible en nuestro sitio
WEB en <ulink
url="http://www.debian.org/releases/stable/installmanual">http://www.debian.org/releases/stable/installmanual</ulink>.
</para>
</section>

<section id="otherdistribs"><title>¿Hasta qué punto es Debian compatible con otras distribuciones de Linux?</title>
<para>
Los desarrolladores de Debian se comunican con otros creadores de
distribuciones de Linux, en un esfuerzo para mantener la compatibilidad binaria
entre las distintas distribuciones.  La mayoría de los productos comerciales
corren tan bien sobre Debian como lo hacen sobre los sistemas en los cuales se
compilaron.
</para>
<para>
Debian GNU/Linux se adhiere a la Estructura de Sistema de Ficheros Linux <ulink
url="http://www.pathname.com/fhs/">(Linux File System Structure)</ulink>.  Sin
embargo, hay espacio para la interpretación en algunas de las reglas que
componen esta norma, por lo cual puede haber diferencias entre un sistema
Debian y otros sistemas Linux.
</para>
</section>

<section id="otherunices"><title>¿Hasta qué punto es Debian compatible en código fuente con otros sistemas Unix?</title>
<para>
El código fuente Linux de la mayoría de las aplicaciones es compatible con
otros sistemas Unix.  Casi todo lo que está disponible en los sistemas Unix
System V, y en los sistemas comerciales o libres derivados de BSD, también
puede funcionar en Linux.  Sin embargo, tal aserción no tiene ningún valor en
el mercado comercial de Unix, pues no hay manera de probarla.  En el ámbito
del desarrollo de software, se requiere una compatibilidad completa, y no sólo
en "casi todos" los casos.  Por ese motivo, hace algunos años surgió la
necesidad de normas, y en la actualidad la POSIX.1 (IEEE Standard 1003.1-1990)
es una de las normas más importantes sobre compatibilidad de código fuente en
los sistemas operativos al estilo Unix.
</para>
<para>
Linux está encaminado a adherirse a POSIX.1, pero las normas POSIX cuestan
dinero, y la certificación POSIX.1 (y FIPS 151-2) es bastante cara; esto ha
hecho más difícil el trabajo de los desarrolladores de Linux para obtener
completa conformidad POSIX.  Los costes de certificación hacen poco probable
que Debian obtenga un certificado de conformidad oficial, aún cuando complete
satisfactoriamente el conjunto de pruebas de validación.  (El conjunto de
pruebas de validación puede obtenerse libremente en la actualidad, así que se
espera que más gente trabaje en los temas de POSIX.1.)
</para>
<para>
Unifix GmbH (Braunschweig, Alemania) desarrolló un sistema Linux que ha sido
certificado como conforme a la norma FIPS 151-2 (que es un superconjunto de
POSIX.1).  Esta tecnología estuvo disponible en la propia distribución de
Unifix, denominada Unifix Linux 2.0 y en la distribución Linux-FT de
Lasermoon.
</para>
</section>

<section id="otherpackages"><title>¿Puedo utilizar los paquetes Debian (los ficheros ".deb") en mi sistema Linux RedHat/Slackware/...? ¿Puedo utilizar los paquetes RedHat (los ficheros ".rpm") en mi sistema Debian GNU/Linux?</title>
<para>
Las diferentes distribuciones de Linux utilizan distintos formatos de paquete y
distintos programas de administración de paquetes.
</para>
<variablelist>
<varlistentry>
<term><emphasis role="strong">Usted probablemente puede:</emphasis></term>
<listitem>
<para>
Hay disponible un programa que permite desempaquetar un paquete Debian en una
máquina que se ha instalado con una distribución `extraña', y generalmente
funciona, en el sentido en que desempaquetará los ficheros.  Lo contrario
posiblemente es cierto también, esto es, un programa que desempaquete un
paquete RedHat o Slackware en una máquina basada en Debian Linux probablemente
tendrá éxito desempaquetando el paquete y colocando la mayoría de los
ficheros en los directorios deseados.  La causa de este éxito puede
encontrarse mayormente en la existencia de (y la amplia adhesión a) la Norma
de Sistema de Ficheros Linux (Linux File System Standard).
</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis role="strong">Usted probablemente no quiera:</emphasis></term>
<listitem>
<para>
La mayoría de los administradores de paquetes escriben ficheros
administrativos cuando se utilizan para desempaquetar un archivo.  Dichos
ficheros administrativos en general no están estandarizados.  Por lo tanto, el
efecto de desempaquetar un paquete Debian en un entorno `extraño' puede tener
efectos impredecibles (ciertamente no muy útiles) sobre el administrador de
paquetes de dicho entorno.  De la misma manera, las utilidades provenientes de
otras distribuciones pueden tener éxito al desempaquetar sus archivos en un
sistema Debian, pero probablemente causarán un fallo del sistema de
administración de paquetes de Debian cuando llegue el momento de actualizar o
borrar algunos paquetes, o incluso si se desea simplemente un informe exacto de
los paquetes presentes en el sistema.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis role="strong">El mejor camino:</emphasis></term>
<listitem>
<para>
La Norma de Sistema de Ficheros Linux (y por lo tanto Debian GNU/Linux)
requiere que los subdirectorios bajo <literal>/usr/local/</literal> se usen
enteramente a discreción del usuario.  Así que los usuarios pueden
desempaquetar paquetes `extraños' en este directorio, y luego administrar sus
configuraciones, actualizarlos y borrarlos individualmente.
</para>
</listitem>
</varlistentry>
</variablelist>
</section>

<section id="a.out"><title>¿Es capaz Debian de ejecutar mis viejos programas "a.out"?</title>
<para>
¿Todavía tiene un programa así?  :-) Para <emphasis>ejecutar</emphasis> un
programa cuyo binario está en formato <literal>a.out</literal> (i.e., QMAGIC o
ZMAGIC),
</para>
<itemizedlist>
<listitem>
<para>
Asegúrese de que su núcleo tenga soporte <literal>a.out</literal> incluido,
ya sea directamente (CONFIG_BINFMT_AOUT=y) o como módulo
(CONFIG_BINFMT_AOUT=m).  (el paquete kernel-image de Debian contiene el módulo
<literal>binfmt_aout</literal>.)
</para>
<para>
Si su núcleo soporta binarios <literal>a.out</literal> mediante módulos,
entonces debe asegurarse de que esté cargado el módulo
<literal>binfmt_aout</literal>.  Puede hacer esto en el momento del arranque si
tiene una línea que diga <literal>binfmt_aout</literal> en el fichero
<literal>/etc/modules</literal>.  Puede también hacerlo desde la línea de
órdenes si ejecuta <literal>insmod NOMBREDIR/binfmt_aout.o</literal> donde
<literal>NOMBREDIR</literal> es el nombre del directorio donde se almacenan los
módulos construídos para la versión del núcleo que está corriendo.  En un
sistema con la versión 2.4.27 del núcleo es probable que
<literal>NOMBREDIR</literal> sea <literal>/lib/modules/2.4.27/fs/</literal>.
</para>
</listitem>
<listitem>
<para>
Instale el paquete <literal>libc4</literal>.
</para>
<para>
En el momento en el que se salga Debian 2.0, es muy probable que el paquete
<literal>libc4</literal> haya sido eliminado de la distribución.  Si este es
el caso, usted podría querer buscar un CD-ROM de Debian antiguo (Debian 1.3.1
todavía tenía este paquete).
</para>
</listitem>
<listitem>
<para>
Si el programa que desea ejecutar es un cliente X <literal>a.out</literal>,
entonces instale también el paquete <literal>xcompat</literal>.
</para>
</listitem>
</itemizedlist>
<para>
Si tiene una aplicación comercial en formato <literal>a.out</literal>, puede
ser el momento de pedir que le envíen una actualización a formato
<literal>ELF</literal>.
</para>
</section>

<section id="libc5"><title>¿Puede Debian ejecutar mis programas libc5 antiguos?</title>
<para>
Sí.  Simplemente, instale las bibliotecas necesarias de la sección
<literal>oldlibs</literal> (que contiene paquetes antiguos que se incluyen para
conseguir compatibilidad con aplicaciones antiguas).
</para>
</section>

<section id="libc5-compile"><title>¿Puede utilizarse Debian para compilar programas libc5?</title>
<para>
Sí.  Instale los paquetes <literal>-altdev</literal> necesarios.  El conjunto
mínimo de paquetes que necesitará es: <literal>altgcc</literal>, en la
sección <literal>devel</literal> y <literal>libc5-altdev</literal> en la
sección <literal>oldlibs</literal>.  Entonces tiene que colocar las
herramientas <literal>libc5</literal> antes que las normales en el PATH.  Esto
es, ejecute la orden <literal>export
PATH=/usr/i486-linuxlibc1/bin:$PATH</literal> (Esto no es esencial, sólo
ventajoso).  Si sólo lo va a utilizar una vez, sería suficiente con:
<literal>PATH=/usr/i486-linuxlibc1/bin:$PATH make [objetivo]</literal>.
</para>
<para>
Tenga en cuenta que ya no hay soporte para el entorno libc5.
</para>
</section>

<section id="non-debian-programs"><title>¿Cómo debería instalar un paquete que no es Debian?</title>
<para>
Los ficheros bajo el directorio <literal>/usr/local/</literal> no están bajo
el control del sistema de administración de paquetes de Debian.  Por lo tanto,
es una buena práctica colocar el código fuente de su programa en
<literal>/usr/local/src/</literal>.  Por ejemplo, puede extraer los ficheros de
un paquete denominado "fu.tar" dentro del directorio
<literal>/usr/local/src/fu</literal>.  Después de compilarlo, coloque los
ejecutables en <literal>/usr/local/bin/</literal>, las bibliotecas en
<literal>/usr/local/lib/</literal>, y los ficheros de configuración en
<literal>/usr/local/etc/</literal>.
</para>
<para>
Si sus programas y/o ficheros realmente deben situarse en algún otro
directorio, aún puede colocarlos bajo el directorio
<literal>/usr/local/</literal>, y crear los enlaces simbólicos en los lugares
necesarios que apunten al lugar de instalación en
<literal>/usr/local/</literal>, por ejemplo, puede crear el enlace
</para>
<screen>
ln -s /usr/local/bin/fu /usr/bin/fu
</screen>
<para>
En cualquier caso, si el paquete que obtuvo permite la redistribución en su
licencia, debería considerar la posibilidad de construir un paquete Debian de
él, y enviarlo para que forme parte del sistema Debian.  Las instrucciones
para convertirse en desarrollador de paquetes se encuentran en el Manual de
Normas de Debian (véase <xref linkend="debiandocs"/>).
</para>
</section>

<section id="xlib6"><title>¿Por qué me da el mensaje de error "Can't find libX11.so.6" cuando trato de ejecutar <literal>fu</literal>?</title>
<para>
Este mensaje de error puede significar que el programa está enlazado con la
versión <literal>libc5</literal> de las bibliotecas X11.  En este caso
necesitará instalar el paquete <literal>xlib6</literal>, de la sección
<literal>oldlibs</literal>.
</para>
</section>

<section id="termcap"><title>¿Por qué no puedo compilar programas que requieren libtermcap?</title>
<para>
Debian usa la base de datos <literal>terminfo</literal> y la biblioteca
<literal>ncurses</literal> para manejar la interfaz del terminal, en lugar de
la base de datos <literal>termcap</literal> y la biblioteca
<literal>termcap</literal>.  Los usuarios que compilen programas que requieran
conocimiento de la interfaz del terminal deben reemplazar las referencias a
<literal>libtermcap</literal> con referencias a <literal>libncurses</literal>.
</para>
<para>
Para dar soporte a los ejecutables que hayan sido enlazados con la biblioteca
<literal>termcap</literal>, y de los cuales no tenga el código fuente, Debian
proporciona un paquete denominado <literal>termcap-compat</literal>.  Éste
provee tanto el fichero <literal>libtermcap.so.2</literal> como
<literal>/etc/termcap</literal>.  Instale este paquete si el programa falla al
ejecutarse con el mensaje de error "can't load library 'libtermcap.so.2'", o se
queja de que no encuentra el fichero <literal>/etc/termcap</literal>.
</para>
</section>

<section id="accelx"><title>¿Por qué no puedo instalar AccelX?</title>
<para>
AccelX usa la biblioteca <literal>termcap</literal> durante su instalación.
Vea <xref linkend="termcap"/> más arriba.
</para>
</section>

<section id="motifnls"><title>¿Por qué se cuelgan mis aplicaciones Motif de Xfree 2.1 antiguas?</title>
<para>
Necesita instalar el paquete <systemitem role="package">motifnls</systemitem>,
que proporciona los ficheros de configuración de XFree-2.1 necesarios para que
las aplicaciones compiladas con XFree-2.1 funcionen bajo XFree-3.1.
</para>
<para>
Sin estos ficheros, algunas aplicaciones Motif compiladas en otras máquinas
(como Netscape) se pueden colgar al intentar copiar o pegar desde o hacia un
campo de texto, y también presentan otros problemas.
</para>
<section id="s3.12.1"><title>¿Puedo instalar y compilar un núcleo sin `retoques' específicos para Debian?</title>
<para>
Sí.  Pero debe comprender antes las reglas que tiene Debian acerca de los
ficheros de cabecera.
</para>
<para>
Las bibliotecas C de Debian se construyen con las últimas entregas
<emphasis>estables</emphasis> de los ficheros de encabezado de
<literal>gcc</literal>.  Por ejemplo, la distribución Debian-1.2 usó la
versión 5.4.13 de los ficheros de encabezado.  Esta práctica contrasta con
los paquetes de fuentes del núcleo que se archivan en todos los sitios FTP de
Linux, que utilizan versiones aún más nuevas de los ficheros de encabezado.
Los encabezados del núcleo que se distribuyen junto con los fuentes de dicho
núcleo se encuentran alojados en
<literal>/usr/include/linux/include/</literal>.
</para>
<para>
Si necesitara compilar un programa con encabezados de núcleo más nuevos que
los provistos por <literal>libc6-dev</literal>, entonces tendrá que agregar
<literal>-I/usr/src/linux/include/</literal> a la línea de órdenes al
compilar.  Esto sucede en una situación, por ejemplo, con el paquete del
demonio de automontaje (automounter daemon) (<literal>amd</literal>).  Cuando
los nuevos núcleos cambian las cuestiones internas que manejan el NFS,
<literal>amd</literal> necesita enterarse de tales cambios.  Para ello requiere
la inclusión de las últimas versiones de los encabezados del núcleo.
</para>
</section>

</section>

</chapter>

