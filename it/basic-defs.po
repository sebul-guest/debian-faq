# Translation of debian-faq into Italian.
#
# Copyright © Free Software Foundation, Inc.
# This file is distributed under the same license as the debian-faq package.
#
# Translators:
# Beatrice Torracca <beatricet@libero.it>, 2012-2017, 2019.
msgid ""
msgstr ""
"Project-Id-Version: debian-faq\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-04-19 13:51+0200\n"
"PO-Revision-Date: 2021-02-20 16:50+0100\n"
"Last-Translator: Beatrice Torracca <beatricet@libero.it>\n"
"Language-Team: Italian <debian-l10n-italian@lists.debian.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.4.2\n"

#. type: Content of: <chapter><title>
#: en/basic-defs.dbk:8
msgid "Definitions and overview"
msgstr "Definizioni e introduzione"

#. type: Content of: <chapter><section><title>
#: en/basic-defs.dbk:9
msgid "What is this FAQ?"
msgstr "Cosa sono queste FAQ?"

#. type: Content of: <chapter><section><para>
#: en/basic-defs.dbk:11
msgid ""
"This document gives frequently asked questions (with their answers!) about "
"the Debian distribution (&debian; and others) and about the Debian project.  "
"If applicable, pointers to other documentation will be given: we won't quote "
"large parts of external documentation in this document.  You'll find out "
"that some answers assume some knowledge of Unix-like operating systems.  "
"We'll try to assume as little prior knowledge as possible: answers to "
"general beginners questions will be kept simple."
msgstr ""
"Questo documento contiene domande frequenti (e le risposte!) sulla "
"distribuzione Debian (&debian; e altre) e sul progetto Debian. Se possibile "
"vengono forniti riferimenti ad altra documentazione: in questo testo non "
"vengono citati grossi brani di documentazione esterna. Si potrà notare che "
"alcune risposte danno per scontata una certa conoscenza dei sistemi "
"operativi *nix. Si è cercato di dare per scontata una quantità di conoscenze "
"più piccola possibile: le risposte a domande generiche dei principianti sono "
"mantenute semplici."

#. type: Content of: <chapter><section><para>
#: en/basic-defs.dbk:20
msgid ""
"If you can't find what you're looking for in this FAQ, be sure to check out "
"<xref linkend=\"debiandocs\"/>.  If even that doesn't help, refer to <xref "
"linkend=\"feedback\"/>."
msgstr ""
"Se non si trova ciò che si sta cercando in queste FAQ, assicurarsi di "
"controllare <xref linkend=\"debiandocs\"/>. Se anche ciò non aiuta, fare "
"riferimento a <xref linkend=\"feedback\"/>."

#. type: Content of: <chapter><section><title>
#: en/basic-defs.dbk:26
msgid "What is &debian;?"
msgstr "Cosa è &debian;?"

#. type: Content of: <chapter><section><para>
#: en/basic-defs.dbk:28
msgid ""
"&debian; is a particular <emphasis>distribution</emphasis> of the Linux "
"operating system, and numerous packages that run on it."
msgstr ""
"&debian; è una particolare <emphasis>distribuzione</emphasis> del sistema "
"operativo Linux e di numerosi pacchetti funzionanti su di essa."

#. type: Content of: <chapter><section><para>
#: en/basic-defs.dbk:32
msgid "&debian; is:"
msgstr "&debian; è:"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/basic-defs.dbk:37
msgid ""
"<emphasis role=\"strong\">full featured</emphasis>: Debian includes more "
"than &packages-total; software packages at present.  Users can select which "
"packages to install; Debian provides a tool for this purpose.  You can find "
"a list and descriptions of the packages currently available in Debian at any "
"of the Debian <ulink url=\"https://www.debian.org/distrib/ftplist\">mirror "
"sites</ulink>."
msgstr ""
"<emphasis role=\"strong\">completa</emphasis>: Debian include attualmente "
"più di &packages-total; pacchetti software. Gli utenti possono scegliere "
"quali pacchetti installare; Debian fornisce uno strumento a questo scopo. Si "
"può trovare un elenco con la descrizione dei pacchetti attualmente "
"disponibili in Debian in uno qualsiasi dei <ulink url=\"https://www.debian."
"org/distrib/ftplist\">siti mirror</ulink> di Debian."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/basic-defs.dbk:46
msgid ""
"<emphasis role=\"strong\">free to use and redistribute</emphasis>: There is "
"no consortium membership or payment required to participate in its "
"distribution and development.  All packages that are formally part of "
"&debian; are free to redistribute, usually under terms specified by the GNU "
"General Public License."
msgstr ""
"<emphasis role=\"strong\">liberamente utilizzabile e ridistribuibile</"
"emphasis>: non è necessaria l'affiliazione a nessuna associazione né alcun "
"pagamento per partecipare alla sua distribuzione e al suo sviluppo. Tutti i "
"pacchetti che sono formalmente parte di &debian; sono liberamente "
"ridistribuibili, di solito nei termini specificati dalla licenza GNU GPL "
"(GNU General Public License)."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/basic-defs.dbk:53
msgid ""
"The Debian archives also carry approximately &packages-contrib-nonfree; "
"software packages (in the <literal>non-free</literal> and <literal>contrib</"
"literal> sections), which are distributable under specific terms included "
"with each package."
msgstr ""
"Gli archivi Debian contengono anche approssimativamente &packages-contrib-"
"nonfree; pacchetti software (nelle sezioni <literal>non-free</literal> e "
"<literal>contrib</literal>), che sono distribuibili nei termini specifici "
"indicati in ciascun pacchetto."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/basic-defs.dbk:60
msgid ""
"<emphasis role=\"strong\">dynamic</emphasis>: With about &developers; "
"volunteers constantly contributing new and improved code, Debian is evolving "
"rapidly.  The archives are updated twice every day."
msgstr ""
"<emphasis role=\"strong\">dinamica</emphasis>: con circa &developers; "
"volontari che contribuiscono costantemente con codice nuovo e migliorato, "
"Debian è in rapida evoluzione. Gli archivi vengono aggiornati due volte al "
"giorno."

#. type: Content of: <chapter><section><para>
#: en/basic-defs.dbk:67
msgid ""
"Most Linux users run a specific <emphasis>distribution</emphasis> of Linux, "
"like &debian;.  However, in principle, users could obtain the Linux kernel "
"via the Internet or from elsewhere, and compile it themselves.  They could "
"then obtain source code for many applications in the same way, compile the "
"programs, then install them into their systems.  For complicated programs, "
"this process can be not only time-consuming but error-prone.  To avoid it, "
"users often choose to obtain the operating system and the application "
"packages from one of the Linux distributors.  What distinguishes the various "
"Linux distributors are the software, protocols, and practices they use for "
"packaging, installing, and tracking applications packages on users' systems, "
"combined with installation and maintenance tools, documentation, and other "
"services."
msgstr ""
"La maggior parte degli utenti Linux usa una particolare "
"<emphasis>distribuzione</emphasis> di Linux, come &debian;. In via di "
"principio, però, gli utenti possono ottenere il kernel Linux da Internet o "
"da qualche altra parte e compilarlo loro stessi. Poi, nello stesso modo, "
"possono ottenere il codice sorgente di molte applicazioni, compilare i "
"programmi e installarli nel proprio sistema. Per programmi complessi questo "
"processo può essere non solo un dispendio di tempo, ma può anche comportare "
"degli errori. Per evitare tutto ciò, gli utenti spesso scelgono di ottenere "
"il sistema operativo ed i pacchetti delle applicazioni da uno dei "
"distributori Linux. Quello che distingue i vari distributori Linux è il "
"software, i protocolli ed i procedimenti usati per impacchettare, installare "
"e tenere traccia dei pacchetti applicativi sui sistemi degli utenti, e "
"inoltre gli strumenti di installazione e manutenzione, documentazione ed "
"altri servizi."

#. type: Content of: <chapter><section><para>
#: en/basic-defs.dbk:80
msgid ""
"&debian; is the result of a volunteer effort to create a free, high-quality "
"Unix-compatible operating system, complete with a suite of applications.  "
"The idea of a free Unix-like system originates from the GNU project, and "
"many of the applications that make &debian; so useful were developed by the "
"GNU project."
msgstr ""
"&debian; è il risultato di uno sforzo di volontari per creare un sistema "
"operativo compatibile con Unix, di alta qualità, libero, completato da un "
"insieme di applicazioni. L'idea di un sistema operativo *nix libero ha avuto "
"origine dal progetto GNU e molte applicazioni che rendono &debian; così "
"utile sono state sviluppate dal progetto GNU."

#. type: Content of: <chapter><section><para>
#: en/basic-defs.dbk:87
msgid ""
"For Debian, free has the GNUish meaning (see the <ulink url=\"https://www."
"debian.org/social_contract#guidelines\">Debian Free Software Guidelines</"
"ulink>).  When we speak of free software, we are referring to freedom, not "
"price.  Free software means that you have the freedom to distribute copies "
"of free software, that you receive source code or can get it if you want it, "
"that you can change the software or use pieces of it in new free programs; "
"and that you know you can do these things."
msgstr ""
"Per Debian, libero ha il significato del mondo GNU (si vedano le <ulink url="
"\"https://www.debian.org/social_contract#guidelines\">Linee Guida Debian per "
"il Software Libero</ulink>). Quando parliamo di software libero, ci "
"riferiamo alla libertà, non al prezzo. Software libero significa che si ha "
"la libertà di distribuire copie di software libero, che si riceve il codice "
"sorgente o lo si può ottenere se lo si desidera, che si può cambiare il "
"software o usare parte di esso in nuovi programmi liberi e che si è a "
"conoscenza di poter fare queste cose."

#. type: Content of: <chapter><section><para>
#: en/basic-defs.dbk:96
msgid ""
"The Debian Project was created by Ian Murdock in 1993, initially under the "
"sponsorship of the Free Software Foundation's GNU project.  Today, Debian's "
"developers think of it as a direct descendent of the GNU project."
msgstr ""
"Il Progetto Debian è stato creato da Ian Murdock nel 1993, inizialmente "
"sotto la sponsorizzazione del progetto GNU della Free Software Foundation. "
"Oggi, gli sviluppatori Debian lo vedono come un diretto discendente del "
"progetto GNU."

#. type: Content of: <chapter><section><para>
#: en/basic-defs.dbk:101
msgid ""
"Although &debian; itself is free software, it is a base upon which value-"
"added Linux distributions can be built.  By providing a reliable, full-"
"featured base system, Debian provides Linux users with increased "
"compatibility, and allows Linux distribution creators to eliminate "
"duplication of effort and focus on the things that make their distribution "
"special.  See <xref linkend=\"childistro\"/> for more information."
msgstr ""
"Tuttavia anche se &debian; in sé è software libero, rappresenta una base su "
"cui possono essere costruite distribuzioni di Linux dal valore aggiunto. "
"Fornendo un sistema di base sicuro e completo di funzionalità, Debian "
"fornisce agli utenti Linux una compatibilità crescente e permette ai "
"creatori di distribuzioni Linux di eliminare sforzi duplicati e di "
"concentrarsi sulle caratteristiche che rendono speciale la propria "
"distribuzione. Vedere <xref linkend=\"childistro\"/> per maggiori "
"informazioni."

#. type: Content of: <chapter><section><title>
#: en/basic-defs.dbk:110
msgid "OK, now I know what Debian is... what is Linux?!"
msgstr "OK, ora so cosa è Debian... cosa è Linux?!"

#. type: Content of: <chapter><section><para>
#: en/basic-defs.dbk:112
msgid ""
"In short, Linux is the kernel of a Unix-like operating system.  It was "
"originally designed for 386 (and better) PCs; today Linux also runs on a "
"dozen of other systems.  Linux is written by Linus Torvalds and many "
"computer scientists around the world."
msgstr ""
"In breve, Linux è il kernel di un sistema operativo *nix. È stato "
"originariamente progettato per PC 386 (e superiori); ora può girare anche su "
"dozzine di altri sistemi. Linux è stato scritto da Linus Torvalds e molti "
"altri informatici di tutto il mondo."

#. type: Content of: <chapter><section><para>
#: en/basic-defs.dbk:118
msgid "Besides its kernel, a \"Linux\" system usually has:"
msgstr "Oltre al suo kernel, un sistema \"Linux\" di solito ha:"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/basic-defs.dbk:123
msgid ""
"a file system that follows the Linux Filesystem Hierarchy Standard <ulink "
"url=\"&url-pathname-fhs;\"/>."
msgstr ""
"un file system che segue il Linux Filesystem Hierarchy Standard <ulink url="
"\"&url-pathname-fhs;\"/>;"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/basic-defs.dbk:128
msgid ""
"a wide range of Unix utilities, many of which have been developed by the GNU "
"project and the Free Software Foundation."
msgstr ""
"una vasta gamma di utilità Unix, molte delle quali sono state sviluppate dal "
"progetto GNU e dalla Free Software Foundation."

#. type: Content of: <chapter><section><para>
#: en/basic-defs.dbk:134
msgid ""
"The combination of the Linux kernel, the file system, the GNU and FSF "
"utilities, and the other utilities are designed to achieve compliance with "
"the POSIX (IEEE 1003.1) standard; see <xref linkend=\"otherunices\"/>."
msgstr ""
"La combinazione del kernel Linux, del file system, delle utilità GNU, di "
"quelle FSF e delle altre utilità è stata progettata per raggiungere la "
"compatibilità con lo standard POSIX (IEEE 1003.1); si veda <xref linkend="
"\"otherunices\"/>."

#. type: Content of: <chapter><section><para>
#: en/basic-defs.dbk:139
msgid ""
"For more information about Linux, see <ulink url=\"https://www.linux.org/"
"info/\">What is Linux</ulink> by <ulink url=\"https://www.linux.org/\">Linux "
"Online</ulink>."
msgstr ""
"Per maggiori informazioni su Linux, vedere <ulink url=\"https://www.linux."
"org/info/\">Cosa è Linux</ulink> di <ulink url=\"https://www.linux.org/"
"\">Linux Online</ulink>."

#. type: Content of: <chapter><section><title>
#: en/basic-defs.dbk:145
msgid "Does Debian just do GNU/Linux?"
msgstr "Debian fa solo GNU/Linux?"

#. type: Content of: <chapter><section><para>
#: en/basic-defs.dbk:147
msgid ""
"Currently, Debian is only available for Linux, but with Debian GNU/Hurd and "
"Debian on BSD kernels, we have started to offer non-Linux-based OSes as a "
"development, server and desktop platform, too.  However, these non-linux "
"ports are not officially released yet."
msgstr ""
"Attualmente, Debian è disponibile solo per Linux, ma con Debian GNU/Hurd e "
"Debian su kernel BSD si è iniziato ad offrire come piattaforme di sviluppo, "
"server e desktop anche altri sistemi operativi non basati su Linux. Questi "
"port non-Linux tuttavia non sono ancora rilasciati ufficialmente."

#. type: Content of: <chapter><section><para>
#: en/basic-defs.dbk:153
msgid "The oldest porting effort is Debian GNU/Hurd."
msgstr "Il primo port su cui si è lavorato è Debian GNU/Hurd."

#. type: Content of: <chapter><section><para>
#: en/basic-defs.dbk:156
msgid ""
"The Hurd is a set of servers running on top of the GNU Mach microkernel.  "
"Together they build the base for the GNU operating system."
msgstr ""
"Hurd è un insieme di servizi che funzionano sul microkernel Mach GNU. "
"Insieme costituiscono la base del sistema operativo GNU."

#. type: Content of: <chapter><section><para>
#: en/basic-defs.dbk:160
msgid ""
"Please see <ulink url=\"&url-gnu-hurd;\"/> for more information about the "
"GNU/Hurd in general, and <ulink url=\"&url-debian-hurd;\"/> for more "
"information about Debian GNU/Hurd."
msgstr ""
"Si veda <ulink url=\"&url-gnu-hurd;\"/> per maggiori informazioni su GNU/"
"Hurd in generale e <ulink url=\"&url-debian-hurd;\"/> per maggiori "
"informazioni su Debian GNU/Hurd."

#. type: Content of: <chapter><section><para>
#: en/basic-defs.dbk:165
msgid ""
"A second effort is the port to a BSD kernel.  People are working with the "
"FreeBSD kernel."
msgstr ""
"Un secondo sforzo è stato quello di fare il port per il kernel BSD. Persone "
"stanno lavorando al kernel FreeBSD."

#. type: Content of: <chapter><section><para>
#: en/basic-defs.dbk:169
msgid ""
"See <ulink url=\"https://www.debian.org/ports/#nonlinux\"/> for more "
"information about these non-linux ports."
msgstr ""
"Si veda <ulink url=\"https://www.debian.org/ports/#nonlinux\"/> per maggiori "
"informazioni su questi port non-Linux."

#. type: Content of: <chapter><section><title>
#: en/basic-defs.dbk:174
msgid ""
"What is the difference between &debian; and other Linux distributions? Why "
"should I choose Debian over some other distribution?"
msgstr ""
"Qual è la differenza tra &debian; e le altre distribuzioni Linux? Perché "
"scegliere Debian invece di qualche altra distribuzione?"

#. type: Content of: <chapter><section><para>
#: en/basic-defs.dbk:176
msgid "These key features distinguish Debian from other Linux distributions:"
msgstr ""
"Queste caratteristiche chiave distinguono Debian dalle altre distribuzioni "
"Linux:"

#. type: Content of: <chapter><section><variablelist><varlistentry><term>
#: en/basic-defs.dbk:180
msgid "Freedom:"
msgstr "Libertà:"

#. type: Content of: <chapter><section><variablelist><varlistentry><listitem><para>
#: en/basic-defs.dbk:183
msgid ""
"As stated in the <ulink url=\"&url-debian-social-contract;\">Debian Social "
"Contract</ulink>, Debian will remain 100% free.  Debian is very strict about "
"shipping truly free software.  The guidelines used to determine if a work is "
"\"free\" are provided in <ulink url=\"https://www.debian.org/"
"social_contract#guidelines\">The Debian Free Software Guidelines (DFSG)</"
"ulink>."
msgstr ""
"Come indicato nel <ulink url=\"&url-debian-social-contract;\">Contratto "
"sociale Debian</ulink>, Debian rimarrà libera al 100%. Debian è molto "
"rigorosa per ciò che riguarda la distribuzione di software veramente libero. "
"Le linee guida usate per determinare se un lavoro è \"libero\" sono fornite "
"nelle <ulink url=\"https://www.debian.org/social_contract#guidelines\">Linee "
"guida Debian per il Software Libero (DFSG)</ulink>."

#. type: Content of: <chapter><section><variablelist><varlistentry><term>
#: en/basic-defs.dbk:193
msgid "The Debian package maintenance system:"
msgstr "Il sistema di manutenzione dei pacchetti Debian:"

#. type: Content of: <chapter><section><variablelist><varlistentry><listitem><para>
#: en/basic-defs.dbk:196
msgid ""
"The entire system, or any individual component of it, can be upgraded in "
"place without reformatting, without losing custom configuration files, and "
"(in most cases) without rebooting the system.  Most Linux distributions "
"available today have some kind of package maintenance system; the Debian "
"package maintenance system is unique and particularly robust (see <xref "
"linkend=\"pkg-basics\"/>)."
msgstr ""
"L'intero sistema, o qualsiasi sua componente individuale, può essere "
"aggiornato in situ senza riformattare, senza perdere i file di "
"configurazione personalizzati e, nella maggior parte dei casi, senza "
"riavviare il sistema. La maggior parte delle distribuzioni Linux disponibili "
"attualmente ha qualche tipo di sistema di manutenzione dei pacchetti; il "
"sistema di manutenzione dei pacchetti Debian è unico e particolarmente "
"robusto (si veda <xref linkend=\"pkg-basics\"/>)."

#. type: Content of: <chapter><section><variablelist><varlistentry><term>
#: en/basic-defs.dbk:205
msgid "Open development:"
msgstr "Sviluppo aperto:"

#. type: Content of: <chapter><section><variablelist><varlistentry><listitem><para>
#: en/basic-defs.dbk:208
msgid ""
"Whereas many other Linux distributions are developed by individuals, small, "
"closed groups, or commercial vendors, Debian is a major Linux distribution "
"that is being developed by an association of individuals who have made "
"common cause to create a free operating system, in the same spirit as Linux "
"and other free software."
msgstr ""
"Mentre molte altre distribuzioni Linux sono sviluppate da singoli, gruppi "
"piccoli, chiusi o produttori commerciali, Debian è un'importante "
"distribuzione Linux che è sviluppata da una comunità di individui che hanno "
"fatta propria la causa della creazione di un sistema operativo libero, nello "
"stesso spirito di Linux e di altro software libero."

#. type: Content of: <chapter><section><variablelist><varlistentry><listitem><para>
#: en/basic-defs.dbk:215
msgid ""
"More than &developers; volunteer package maintainers are working on over "
"&packages-total; packages and improving &debian;.  The Debian developers "
"contribute to the project not by writing new applications (in most cases), "
"but by packaging existing software according to the standards of the "
"project, by communicating bug reports to upstream developers, and by "
"providing user support.  See also additional information on how to become a "
"contributor in <xref linkend=\"contributing\"/>."
msgstr ""
"Più di &developers; volontari che mantengono i pacchetti stanno lavorando su "
"più di &packages-total; pacchetti migliorando &debian;. Gli sviluppatori "
"Debian contribuiscono al progetto non scrivendo nuove applicazioni (nella "
"maggior parte dei casi), ma impacchettando software già esistente in accordo "
"con gli standard del progetto, comunicando segnalazioni sui bug agli "
"sviluppatori originali e fornendo supporto agli utenti. Si vedano anche le "
"informazioni aggiuntive su come diventare un contributore in <xref linkend="
"\"contributing\"/>."

#. type: Content of: <chapter><section><variablelist><varlistentry><term>
#: en/basic-defs.dbk:226
msgid "The Universal Operating System:"
msgstr "Il Sistema Operativo Universale:"

#. type: Content of: <chapter><section><variablelist><varlistentry><listitem><para>
#: en/basic-defs.dbk:229
msgid ""
"Debian comes with <ulink url=\"https://packages.debian.org/stable/\">more "
"than &packages-total; packages</ulink> and runs on <ulink url=\"https://www."
"debian.org/ports/\">&number-archs; architectures</ulink>.  This is far more "
"than is available for any other GNU/Linux distribution.  See <xref linkend="
"\"apps\"/> for an overview of the provided software and see <xref linkend="
"\"arches\"/> for a description of the supported hardware platforms."
msgstr ""
"Debian viene fornita con <ulink url=\"https://packages.debian.org/stable/"
"\">più di &packages-total; pacchetti</ulink> e gira su <ulink url=\"https://"
"www.debian.org/ports/\">&number-archs; architetture</ulink>. Ciò è molto più "
"di quello che è disponibile per le altre distribuzioni GNU/Linux. Si veda "
"<xref linkend=\"apps\"/> per una panoramica del software fornito e <xref "
"linkend=\"arches\"/> per una descrizione delle piattaforme hardware "
"supportate."

#. type: Content of: <chapter><section><variablelist><varlistentry><term>
#: en/basic-defs.dbk:239
msgid "The Bug Tracking System:"
msgstr "Il Sistema di tracciamento dei bug:"

#. type: Content of: <chapter><section><variablelist><varlistentry><listitem><para>
#: en/basic-defs.dbk:242
msgid ""
"The geographical dispersion of the Debian developers required sophisticated "
"tools and quick communication of bugs and bug-fixes to accelerate the "
"development of the system.  Users are encouraged to send bugs in a formal "
"style, which are quickly accessible by WWW archives or via e-mail.  See "
"additional information in this FAQ on the management of the bug log in <xref "
"linkend=\"buglogs\"/>."
msgstr ""
"La dispersione geografica degli sviluppatori Debian ha richiesto degli "
"strumenti sofisticati ed una comunicazione rapida dei bug e delle soluzioni "
"per accelerare lo sviluppo del sistema. Gli utenti sono incoraggiati a "
"segnalare i bug in uno stile formale, che sia rapidamente accessibile "
"attraverso gli archivi WWW o via posta elettronica. Si vedano le "
"informazioni aggiuntive in questa FAQ sulla gestione dei log dei bug in "
"<xref linkend=\"buglogs\"/>."

#. type: Content of: <chapter><section><variablelist><varlistentry><term>
#: en/basic-defs.dbk:252
msgid "The Debian Policy:"
msgstr "La Debian Policy:"

#. type: Content of: <chapter><section><variablelist><varlistentry><listitem><para>
#: en/basic-defs.dbk:255
msgid ""
"Debian has an extensive specification of our standards of quality, the "
"Debian Policy.  This document defines the qualities and standards to which "
"we hold Debian packages."
msgstr ""
"Debian ha un'esauriente specifica dei propri standard di qualità, la Debian "
"Policy. Questo documento definisce la qualità e gli standard che sono "
"richiesti ai pacchetti Debian."

#. type: Content of: <chapter><section><para>
#: en/basic-defs.dbk:263
msgid ""
"For additional information about this, please see our web page about <ulink "
"url=\"https://www.debian.org/intro/why_debian\">reasons to choose Debian</"
"ulink>."
msgstr ""
"Per ulteriori informazioni su questo, si veda la pagina web sulle <ulink url="
"\"https://www.debian.org/intro/why_debian\">ragioni per scegliere Debian</"
"ulink>."

#. type: Content of: <chapter><section><title>
#: en/basic-defs.dbk:268
msgid ""
"How does the Debian project fit in or compare with the Free Software "
"Foundation's GNU project?"
msgstr ""
"Come si rapporta il progetto Debian con il progetto GNU della Free Software "
"Foundation?"

#. type: Content of: <chapter><section><para>
#: en/basic-defs.dbk:270
msgid ""
"The Debian system builds on the ideals of free software first championed by "
"the <ulink url=\"https://www.gnu.org/\">Free Software Foundation</ulink> and "
"in particular by <ulink url=\"https://www.stallman.org/\">Richard Stallman</"
"ulink>.  FSF's powerful system development tools, utilities, and "
"applications are also a key part of the Debian system."
msgstr ""
"Il sistema Debian è costruito sugli ideali del software libero fortemente "
"sostenuto in origine dalla <ulink url=\"https://www.gnu.org/\">Free Software "
"Foundation</ulink> ed in particolare da <ulink url=\"https://www.stallman."
"org/\">Richard Stallman</ulink>. I potenti strumenti, utilità e applicazioni "
"per lo sviluppo del sistema della FSF rappresentano anche una parte chiave "
"del sistema Debian."

#. type: Content of: <chapter><section><para>
#: en/basic-defs.dbk:277
msgid ""
"The Debian Project is a separate entity from the FSF, however we communicate "
"regularly and cooperate on various projects.  The FSF explicitly requested "
"that we call our system \"&debian;\", and we are happy to comply with that "
"request."
msgstr ""
"Il Progetto Debian è un'entità separata dalla FSF, comunque i due comunicano "
"regolarmente e cooperano su vari progetti. La FSF ha richiesto "
"esplicitamente che il nostro sistema venisse chiamato \"&debian;\" e siamo "
"felici di soddisfare questa richiesta."

#. type: Content of: <chapter><section><para>
#: en/basic-defs.dbk:283
msgid ""
"The FSF's long-standing objective is to develop a new operating system "
"called GNU, based on <ulink url=\"&url-gnu-hurd;\">Hurd</ulink>.  Debian is "
"working with FSF on this system, called <ulink url=\"&url-debian-hurd;"
"\">Debian GNU/Hurd</ulink>."
msgstr ""
"L'obiettivo di sempre della FSF è stato quello di sviluppare un nuovo "
"sistema operativo chiamato GNU basato su <ulink url=\"&url-gnu-hurd;\">Hurd</"
"ulink>. Debian sta lavorando con la FSF su questo sistema, chiamato <ulink "
"url=\"&url-debian-hurd;\">Debian GNU/Hurd</ulink>."

#. type: Content of: <chapter><section><title>
#: en/basic-defs.dbk:290
msgid "How does one pronounce Debian and what does this word mean?"
msgstr "Come si pronuncia Debian e cosa significa questa parola?"

#. type: Content of: <chapter><section><para>
#: en/basic-defs.dbk:292
msgid ""
"The project name is pronounced Deb'-ee-en, with a short e in Deb, and "
"emphasis on the first syllable.  This word is a contraction of the names of "
"Debra and Ian Murdock, who founded the project.  (Dictionaries seem to offer "
"some ambiguity in the pronunciation of Ian (!), but Ian prefers ee'-en.)"
msgstr ""
"Il nome del progetto si pronuncia Deb'-i-en, con una e breve in Deb e con "
"l'accento sulla prima sillaba. Questa parola è la contrazione dei nomi di "
"Debra e Ian Murdock, che fondarono il progetto. (I dizionari sembrano essere "
"ambigui sulla pronuncia di Ian (!), ma Ian preferisce i'-en.)"
