# Translation of debian-faq into Dutch.
#
# Copyright © Free Software Foundation, Inc.
# This file is distributed under the same license as the debian-faq package.
#
# Translators:
# Frans Spiesschaert <Frans.Spiesschaert@yucom.be>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: debian-faq_8.1_\n"
"POT-Creation-Date: 2019-07-06 14:47+0200\n"
"PO-Revision-Date: 2017-11-13 14:37+0100\n"
"Last-Translator: Frans Spiesschaert <Frans.Spiesschaert@yucom.be>\n"
"Language-Team: Debian Dutch l10n Team <debian-l10n-dutch@lists.debian.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Gtranslator 2.91.7\n"

#. type: Content of: <chapter><title>
#: en/redistributing.dbk:8
msgid "Redistributing &debian; in a commercial product"
msgstr "&debian; verspreiden in een commercieel product"

#. type: Content of: <chapter><section><title>
#: en/redistributing.dbk:9
msgid "Can I make and sell Debian CDs?"
msgstr "Kan ik Debian CD's maken en verkopen?"

#. type: Content of: <chapter><section><para>
#: en/redistributing.dbk:11
msgid ""
"Go ahead.  You do not need permission to distribute anything we have "
"<emphasis>released</emphasis>, so that you can master your CD as soon as the "
"beta-test ends.  You do not have to pay us anything.  Of course, all CD "
"manufacturers must honor the licenses of the programs in Debian.  For "
"example, many of the programs are licensed under the GPL, which requires you "
"to distribute their source code."
msgstr ""
"Ga uw gang. Uw hoeft geen toestemming te vragen om iets wat we "
"<emphasis>uitgebracht</emphasis> hebben, te verspreiden, zodat u uw CD kunt "
"samenstellen van zodra de beta-testen achter de rug zijn. U hoeft ons niet "
"te betalen. Uiteraard moeten alle fabrikanten van CD's de licenties van de "
"programma's in Debian respecteren. Zo hebben veel van de programma's "
"bijvoorbeeld een GPL-licentie, die vereist dat u de broncode ervan "
"verspreidt."

#. type: Content of: <chapter><section><para>
#: en/redistributing.dbk:19
msgid ""
"Also, we will publish a list of CD manufacturers who donate money, software, "
"and time to the Debian project, and we will encourage users to buy from "
"manufacturers who donate, so it is good advertising to make donations."
msgstr ""
"Ook publiceren we een lijst van CD--fabrikanten die geld, software en tijd "
"doneren aan het Debian-project en moedigen we gebruikers aan hun aankopen te "
"doen bij fabrikanten die doneren. Doneren is dus een goede "
"advertentiestrategie."

#. type: Content of: <chapter><section><title>
#: en/redistributing.dbk:25
msgid "Can Debian be packaged with non-free software?"
msgstr "Kan Debian samen met niet-vrije software verpakt worden?"

#. type: Content of: <chapter><section><para>
#: en/redistributing.dbk:27
msgid ""
"Yes.  While all the main components of Debian are free software, we provide "
"a non-free directory for programs that are not freely redistributable."
msgstr ""
"Ja. Hoewel alle belangrijke componenten van Debian vrije software zijn, "
"stellen we ook een map 'non-free' ter beschikking voor programma's die niet "
"vrij mogen verspreid worden."

#. type: Content of: <chapter><section><para>
#: en/redistributing.dbk:31
msgid ""
"CD manufacturers <emphasis>may</emphasis> be able to distribute the programs "
"we have placed in that directory, depending on the license terms or their "
"private arrangements with the authors of those software packages.  CD "
"manufacturers can also distribute the non-free software they get from other "
"sources on the same CD.  This is nothing new: free and commercial software "
"are distributed on the same CD by many manufacturers now.  Of course we "
"still encourage software authors to release the programs they write as free "
"software."
msgstr ""
"<emphasis>Mogelijk</emphasis> verkeren CD-fabrikanten in de mogelijkheid de "
"programma's die we in die map geplaatst hebben, te verspreiden, afhankelijk "
"van de bepalingen in de licentie of hun eigen overeenkomst met de auteurs "
"van deze softwarepakketten. CD-fabrikanten kunnen ook de niet-vrije software "
"die ze uit andere bronnen betrekken, op dezelfde CD verspreiden. Dat is "
"niets nieuws: vrije en commerciële software wordt nu reeds door veel "
"fabrikanten op dezelfde CD verspreid. Uiteraard blijven we auteurs van "
"software aanmoedigen om de programma's die ze schrijven uit te brengen als "
"vrije software."

#. type: Content of: <chapter><section><title>
#: en/redistributing.dbk:41
msgid ""
"I am making a special Linux distribution for a \"vertical market\". Can I "
"use &debian; for the guts of a Linux system and add my own applications on "
"top of it?"
msgstr ""
"I maak een speciale Linux-distributie voor een \"verticale markt\". Kan ik "
"&debian; gebruiken als Linux-basissysteem met daarbovenop mijn eigen "
"toepassingen?"

#. type: Content of: <chapter><section><para>
#: en/redistributing.dbk:43
msgid ""
"Yes.  Debian-derived distributions are being created both in close "
"cooperation with the Debian project itself and by external parties.  One can "
"use the <ulink url=\"https://www.debian.org/blends/\">Debian Pure Blends</"
"ulink> framework to work together with Debian; <ulink url=\"https://wiki."
"debian.org/DebianEdu/\">DebianEdu/Skolelinux</ulink> is one such project."
msgstr ""
"Ja. Van Debian afgeleide distributies worden zowel in nauwe samenwerking met "
"het Debian-project zelf gebouwd als door externe partijen. Men kan het "
"raamwerk voor <ulink url=\"https://www.debian.org/blends/"
"\">Doelgroepspecifieke uitgaves van Debian (Debian Pure Blends)</ulink> "
"gebruiken om met Debian samen te werken. Het project <ulink url=\"https://"
"wiki.debian.org/DebianEdu/\">DebianEdu/Skolelinux</ulink> is een dergelijk "
"project."

#. type: Content of: <chapter><section><para>
#: en/redistributing.dbk:51
msgid ""
"There are several other Debian-derived distributions already on the market, "
"such as grml, LMDE (Linux Mint Debian Edition), Knoppix and Ubuntu, that are "
"targeted at a different kind of audience than the original &debian; is, but "
"use most of our components in their product."
msgstr ""
"Er zijn reeds verschillende van Debian afgeleide distributies op de markt, "
"zoals grml, LMDE (Linux Mint Debian Edition), Knoppix en Ubuntu, die zich "
"tot een ander type doelgroep richten dan het originele &debian;, maar de "
"meeste van onze componenten gebruiken in hun product."

#. type: Content of: <chapter><section><para>
#: en/redistributing.dbk:57
msgid ""
"Debian also provides a mechanism to allow developers and system "
"administrators to install local versions of selected files in such a way "
"that they will not be overwritten when other packages are upgraded.  This is "
"discussed further in the question on <xref linkend=\"divert\"/>."
msgstr ""
"Debian beschikt ook over een mechanisme dat ontwikkelaars en "
"systeembeheerders toelaat om van specifieke bestanden een lokale versie te "
"installeren zodanig dat zie niet overschreven worden wanneer andere "
"pakketten opgewaardeerd worden. Dit wordt verder besproken onder het thema "
"<xref linkend=\"divert\"/>."

#. type: Content of: <chapter><section><title>
#: en/redistributing.dbk:64
msgid ""
"Can I put my commercial program in a Debian \"package\" so that it installs "
"effortlessly on any Debian system?"
msgstr ""
"Kan ik mijn commercieel programma in een Debian-\"pakket\" verpakken, zodat "
"het probleemloos geïnstalleerd kan worden op een Debian-systeem?"

#. type: Content of: <chapter><section><para>
#: en/redistributing.dbk:66
msgid ""
"Go right ahead.  The package tool is free software; the packages may or may "
"not be free software, it can install them all."
msgstr ""
"Ga uw gang. Het pakketgereedschap is vrije software. Pakketten kunnen al dan "
"niet vrije software zijn, maar het pakketgereedschap zal beide installeren."
