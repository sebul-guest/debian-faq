<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="compat"><title>Zagadnienia związane z kompatybilnością</title>
<section id="arches"><title>Na jakim sprzęcie można uruchomić system Debian GNU/Linux?</title>
<para>
Debian GNU/Linux zawiera kompletny kod źródłowy dla wszystkich zawartych w
nim aplikacji, więc powinien działać na wszystkich platformach, które są
wspierane przez jądro Linux; szczegóły można znaleźć w <ulink
url="http://en.tldp.org/FAQ/Linux-FAQ/intro.html#DOES-LINUX-RUN-ON-MY-COMPUTER">Linux
FAQ</ulink>
</para>
<para>
Obecne wydanie systemu Debian GNU/Linux 9, zawiera kompletną binarną
dystrybucję dla następujących architektur:
</para>
<para>
<emphasis>i386</emphasis>: obejmuje komputery pracujące z procesorami Intela
lub kompatybilnymi z Intelem, czyli Intelowskie 386, 486, Pentium, Pentium Pro,
Pentium II (zarówno Klamath jak i Celeron), Pentium III, i większość
kompatybilnych procesorów AMD, Cyrix i innych.
</para>
<para>
<emphasis>m68k</emphasis>: obejmuje komuptery Amiga i Atari posiadające
procesory Motorola 680x0 dla x>=2; z MMU.
</para>
<para>
<emphasis>alpha</emphasis>: systemy Alpha produkowane przez Compaq/Digital
</para>
<para>
<emphasis>sparc</emphasis>: obejmuje systemy Sun'a: SPARC i większość
UltraSPARC.
</para>
<para>
<emphasis>powerpc</emphasis>: obejmuje maszyny PowerPC IBM/Motorola,
wlicząjąc w to CHRP, PowerMac i maszyny PReP.
</para>
<para>
<emphasis>arm</emphasis>: maszyny ARM i StrongARM.
</para>
<para>
<emphasis>mips</emphasis>: systemy big-endian MIPS SGI: Indy i Indiago2;
<emphasis>mipsel</emphasis>: maszyny little-endian MIPS, Digital DECstations.
</para>
<para>
<emphasis>hppa</emphasis>: maszyny Hawlett-Packard'a PA-RISC (712, C3000,
L2000, A500).
</para>
<para>
<emphasis>ia64</emphasis>: komputery Intel IA-64 ("Itanium").
</para>
<para>
<emphasis>s390</emphasis>: systemy IBM S/390 mainframe.
</para>
<para>
Binarna dystrybucja Debiana dla architektury Sparc64 (natywny UltraSPARC) jest
obecnie w trakcie rozwoju.
</para>
<para>
Więcej informacji dotyczących uruchamiania systemu, partycjonowania Twoich
dysków, obsługi urządzeń PCMCIA (PC Card) i podobnych zagadnień można
znaleźć w Installation Manual, który jest dostępny na Naszej stronie WWW
<ulink
url="http://www.debian.org/releases/stable/installmanual">http://www.debian.org/releases/stable/installmanual</ulink>.
</para>
</section>

<section id="otherdistribs"><title>Jak bardzo kompatybilny jest Debian z innymi dystrybucjami Linuksa?</title>
<para>
Deweloperzy Debiana komunikują się z twórcami innych dystrybucji Linuksa w
celu zachowania binarnej kompatybilności pomiędzy dystrybucjami Linuksa.
Większość linuksowych, komercyjnych produktów działa równie dobrze pod
kontrolą Debiana jak i pod kontrolą systemów dla których zostały
stworzone.
</para>
<para>
Debian GNU/Linux przestrzega <ulink url="http://www.pathname.com/fhs/">Linux
Filesystem Hierarchy Standard</ulink>.  Jednak standard dopuszcza własną
interpretację niektórych zasad w nim opisanych, więc mogą istnieć
nieznaczne różnice pomiędzy Debianem a pozostałymu systemami Linuksowymi.
</para>
</section>

<section id="otherunices"><title>Jak bardzo kompatybilny jest kod źródłowy Debiana z kodem źródłowym innych systemów Uniksowych?</title>
<para>
Dla większości aplikacji kod źródłowy Linuksa jest kompatybilny z innymi
systemami Uniksowymi.  Linux wspiera prawie wszystko co jest dostępne w
Uniksach System V i darmowowych oraz komercyjnych pochodnych BSD.  Jednak w
obszarze biznesu związanego z Uniksem takie twierdzenie nie ma prawie żadnego
znaczenia, ponieważ nie ma sposobu aby je udowodnić.  W przemyśle związanym
z tworzeniem oprogramowania wymagana jest całkowita kompatybilność, a nie
kompatybilność "w prawie" wszystkich przypadkach.  Z tego powodu przez lata
narastała potrzeba stworzenia standardu, i dzisiaj takim głównym standardem
kompatybilności kodu źródłowego w Uniksowych systemach operacyjnych jest
POSIX.1 (IEEE Standard 1003.1-1990).
</para>
<para>
Linux w zamierzeniach ma przestrzegać POSIX.1, ale certyfikat POSIX.1 (i FIPS
151-2) jest dosyć drogi; stąd praca na zasadach zgodnych z POSIX jest dla
deweloperów Linuksa trudniejsza.  Koszty certyfikatu sprawiają, że zdobycie
przez Debiana certyfikatu, nawet w przypadku przejścia procesu sprawdzania,
jest mało prawdopodobne.  (Proces ten jest obecnie przeprowadzany za darmo,
więc oczekuje się, że więcej ludzi będzie pracować zgodnie z wytycznymi
POSIX.1)
</para>
<para>
Unifix GmbH (Braunschweig, Niemcy) stworzył system Linuksowy, który dostał
certyfikat jako system zgodny z FIPS 151-2 (rozszerzenie POSIX.1).  Ta
technologia jest dostępna we własnej dystrybucji Unifix nazwanej Unifix Linux
2.0 i w Linux-FT Lasermoon'a.
</para>
</section>

<section id="otherpackages"><title>Czy mogę używać pakietów Debiana (plików ".deb") w dystrybucji Linuksa, której używam (RedHat/Slackware/...)? Czy mogę używać pakietów RedHata (plików "rpm") w systemie Debian GNU/Linux?</title>
<para>
Różne dystrybucje Linuksa używają różnych formatów pakietów i różnych
programów do zarządzania tymi pakietami.
</para>
<variablelist>
<varlistentry>
<term><emphasis role="strong">Prawdopodobnie możesz:</emphasis></term>
<listitem>
<para>
Jest dostępny program, który rozpakuje pakiet Debiana na komputerze, gdzie
zainstalowana jest inna dystrybucja Linuksa, i generalnie będzie działał, w
sensie takim, że pliki będą rozpakowane.  Odwrotna sytuacja prawdopodobnie
też zachodzi, to znaczy program rozpakowujący pakiety RedHata albo
Slackware'a na komputerze z dystrybucją opartą o system Debian GNU/Linux
prawdopodobnie rozpakuje pakiet i umieści większość plików w zamierzonych
katalogach.  W większości jest to konsekwencja istnienia (i ogólnego
przestrzegania zasad) Linux Filesystem Hierarchy Standard.  Pakiet <ulink
url="http://packages.debian.org/alien">Alien</ulink> jest używany do konwersji
pomiędzy różnymi typami pakietów.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis role="strong">Prawdopodobnie nie chcesz:</emphasis></term>
<listitem>
<para>
Większość menadżerów pakietów zapisuje swoje pliki administracyjne kiedy
są używane do rozpakowywania archiwum.  Te pliki administracyjne zwykle nie
są ustandaryzowane.  Dlatego rozpakowanie pakietu Debiana na 'obcym'
komputerze będzie miało nieprzewidywalne efekty (a zwłaszcza nie użyteczne)
dla działania menadżera pakietów używanego na tym komputerze.
Analogicznie, programy do zarządzania pakietami z innych dystrybucji mogą
skutecznie rozpakować swoje archiwa na komputerze z zainstalowanym Debianem,
ale prawopodobnie spowoduje to, że menadżer pakietów Debiana nie będzie
poprawnie działać, gdy trzeba będzie zaktualizować lub usunąć pakiety,
lub nawet podczas tworzenia listy pakietów zainstalowanych w systemie.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis role="strong">Lepsze rozwiązanie:</emphasis></term>
<listitem>
<para>
Linux File System Standard (a więc i Debian GNU/Linux) wymaga, aby podkatalogi
w <literal>/usr/local/</literal>, były w pełni dostępne dla użytkowników.
Dlatego użytkownicy mogą rozpakować 'obce' pakiety w tym katalogu,
zarządzać ich konfiguracją, aktualizować i usuwać zależnie od potrzeby.
</para>
</listitem>
</varlistentry>
</variablelist>
</section>

<section id="a.out"><title>Czy w Debianie mogę uruchamiać moje bardzo stare programy "a.out"?</title>
<para>
Czy na pewno ciągle masz takie programy?  :-)
</para>
<para>
Aby <emphasis>uruchomić</emphasis> program, który ma format
<literal>a.out</literal> (np., QMAGIC lub ZMAGIC),
</para>
<itemizedlist>
<listitem>
<para>
Bądź pewny, że Twoje jądro wspiera uruchamianie programów wykonywalnych
<literal>a.out</literal>, albo poprzez wkompilowanie tego bezpośrednio do
jądra (CONFIG_BINFMT_AOUT=y) albo jako moduł (CONFIG_BINFMT_AOUT=m).  (Pakiet
z obrazem jądra Debiana zawiera moduł <literal>binfmt_aout</literal>.)
</para>
<para>
Jeżeli jądro Twojego Debiana obsługuje uruchamianie plików wykonywalnych
<literal>a.out</literal> za pośrednictwem modułu, wtedy musisz być pewny,
że moduł <literal>binfmt_aout</literal> jest załadowany.  Możesz to zrobić
w czasie procesu startu sytemu poprzez dodanie lini
<literal>binfmt_aout</literal> do pliku <literal>/etc/modules</literal>.
Możesz to także zrobić z lini poleceń, poprzez wpisanie polecenia
<literal>insmod DIRNAME/binfmt_aout.o</literal> gdzie
<literal>DIRNAME</literal> jest nazwą katalogu, gdzie jest przechowywany
moduł zbudowany dla tej wersji jądra, która w danej chwili jest używana.  W
systemie z jądrem w wersji 2.2.17 <literal>DIRNAME</literal> będzie
<literal>/lib/modules/2.2.17/fs/</literal>.
</para>
</listitem>
<listitem>
<para>
zainstaluj pakiet <systemitem role="package">libc4</systemitem>, który możesz
znaleźć w jednej z dystrybucji wydanej przed 2.0 (ponieważ wtedy usunięto
ten pakiet).  Możesz spróbować jakiegoś starego CD-ROMu z Debianem (Debian
1.3.1 nadal zawiera ten pakiet) lub zobacz <ulink
url="ftp://archive.debian.org/debian-archive/">ftp://archive.debian.org/debian-archive/</ulink>.
</para>
</listitem>
<listitem>
<para>
Jeśli program, który chcesz uruchomić jest klientem X-ów
<literal>a.out</literal>, zainstaluj pakiet <systemitem
role="package">xcompat</systemitem> (zobacz powyżej w celu sprawdzenia
dostępności).
</para>
</listitem>
</itemizedlist>
<para>
Jeśli posiadasz komercyjny program w formacie <literal>a.out</literal>, to
dobry czas na poproszenie autorów o aktualizację w formacie
<literal>ELF</literal>.
</para>
</section>

<section id="libc5"><title>Czy mogę na Debianie uruchomić moje stare programy libc5?</title>
<para>
Tak.  Zainstaluj po prostu wymagane biblioteki <systemitem
role="package">libc5</systemitem> z części <literal>oldlibs</literal>
(zawierającej stare pakiety właśnie dla zgodności ze starymi aplikacjami).
</para>
</section>

<section id="libc5-compile"><title>Czy mogę na Debianie skompilować stare programy libc5?</title>
<para>
Tak.  Zainstaluj <systemitem role="package">libc5-altdev</systemitem> i
<systemitem role="package">altgcc</systemitem> (z części
<literal>oldlibs</literal>).  Możesz znaleźć odpowiednie, skompilowane z
libc5 <command>gcc</command> i <command>g++</command> w katalogu
<literal>/usr/i486-linuxlibc1/bin</literal>.  Umieść je w zmiennej $PATH tak
by <command>make</command> i inne programy uruchamiały je jako pierwsze.
</para>
<para>
Jeśli musisz skompilować klienta X-ów libc5, zainstaluj pakiety <systemitem
role="package">xlib6</systemitem> i <systemitem
role="package">xlib6-altdev</systemitem>.
</para>
<para>
Uważaj, bo środowisko libc5 nie jest już w pełni obsługiwane przez nasze
pakiety.
</para>
</section>

<section id="non-debian-programs"><title>Jak mogę zainstalować nie-Debianowy program?</title>
<para>
Pliki znajdujące się w katalogu <literal>/usr/local/</literal> nie są pod
kontrolą systemu zarządzania pakietami Debiana.  Dlatego dobrą praktyką
jest umieszczanie źródeł swoich programów w
<literal>/usr/local/src/</literal>.  Dla przykładu możesz rozpakować pliki z
archiwum "cośtam.tar" do katalogu <literal>/usr/local/src/foo</literal>.  Po
kompilacji binaria przenieś do <literal>/usr/local/bin/</literal>, biblioteki
do <literal>/usr/local/lib/</literal>, a wszelkie pliki konfiguracyjne do
<literal>/usr/local/etc/</literal>.
</para>
<para>
Jeśli Twoje programy i/lub pliki naprawdę muszą być umieszczone w jakichś
innych lokalizacja to nadal możesz trzymać je w
<literal>/usr/local/</literal> i utworzyć odpowiednie dowiązania symboliczne
z wymaganych lokalizacji do <literal>/usr/local/</literal>.  Np.  możesz
zrobić dowiązanie
</para>
<screen>
ln -s /usr/local/bin/cośtam /usr/bin/cośtam
</screen>
<para>
W każdym przypadku, jeśli licencja programu pozwala na redystrybucję
powinieneś zastanowić się nad stworzeniem pakietu i przesłaniu go do
Debiana.  Przewodniki jak stać się opiekunem pakietu są załączone w
podręczniku Polityki Debiana (zobacz <xref linkend="debiandocs"/>).
</para>
</section>

<section id="xlib6"><title>Dlaczego otrzymuję błędy "Can't find libX11.so.6" przy uruchamianiu <literal>czegośtam</literal>?</title>
<para>
Taki błąd oznaczać może, że program jest złączony z bibliotekami X11 w
wersji <literal>libc5</literal>.  W takim przypadku musisz zainstalować pakiet
<systemitem role="package">xlib6</systemitem> z części
<literal>oldlibs</literal>.
</para>
<para>
Możesz otrzymywać podobne komunikaty o błędach odnoszące się do pliku
libXpm.so.4.  W takim przypadku należy zainstalować bibliotekę XPM w wersji
libc5 z pakietu <systemitem role="package">xpm4.7</systemitem> z części
<literal>oldlibs</literal>.
</para>
</section>

<section id="termcap"><title>Dlaczego nie mogę skompilować programów używających libtermcap?</title>
<para>
Zamiast bazy oraz biblioteki <literal>termcap</literal> Debian używa bazy
<literal>terminfo</literal> oraz biblioteki <literal>ncurses</literal>
opisującej interfejsy terminali.  Użytkownicy kompilujący programy, które
wymagają pewnej wiedzy o interfejsie terminali powinni podmienić odwołania
do biblioteki <literal>libtermcap</literal> na <literal>libncurses</literal>.
</para>
<para>
By obsługiwać binaria, które zostały już połączone z biblioteką
<literal>termcap</literal> i do których nie posiadasz źródeł, Debian
udostępnia pakiet <systemitem role="package">termcap-compat</systemitem>.  W
nim zawarte są <literal>libtermcap.so.2</literal> i
<literal>/etc/termcap</literal>.  Zainstaluj ten pakiet jeśli program nie
uruchamia się wypisując komunikat "can't load library 'libtermcap.so.2'" lub
wspomina o brakującym pliku <literal>/etc/termcap</literal>.
</para>
</section>

<section id="accelx"><title>Dlaczego nie mogę zainstalować AccelX?</title>
<para>
AccelX używa do instalacji biblioteki <literal>termcap</literal>.  Zobacz
<xref linkend="termcap"/>.
</para>
</section>

<section id="motifnls"><title>Dlaczego moje stare aplikacje Motif XFree 2.1 nie działają?</title>
<para>
Musisz zainstalować pakiet <systemitem role="package">motifnls</systemitem>,
który dostarcza pliki konfiguracyjne dla XFree-2.1 pozwalające aplikacjom
Motifa skompilowanym pod XFree-2.1 uruchamiać się pod XFree-3.1.
</para>
<para>
Bez tych plików niektóre aplikacje Motifa skompilowane na innych komputerach
(takich jak Netscape) mogą się zawieszać przy próbie kopiowania i wklejania
do pól tekstowych.  Mogą występować również inne problemy.
</para>
</section>

</chapter>

