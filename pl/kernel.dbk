<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="kernel"><title>Debian i jądro systemu</title>
<section id="non-debian-kernel"><title>Czy mogę skompilować i zainstalować jądro systemu bez dostosowywania go do specyfiki Debiana?</title>
<para>
Tak.
</para>
<para>
Z jednym tylko zastrzeżeniem: biblioteki języka C w systemie Debian są
budowane z wykorzystaniem najnowszych <emphasis>stabilnych</emphasis> wersji
plików nagłówkowych <emphasis role="strong">jądra systemu</emphasis>.
Jeśli chcesz skompilować program z wykorzystaniem nowszych plików
nagłówkowych jądra niż te ze stabilnej wersji, stoisz przed wyborem
pomiędzy uaktualnieniem pakietu zawierającego pliki nagłówkowe (<systemitem
role="package">libc6-dev</systemitem>), a użyciem nowych plików
nagłówkowych, które znajdują się w katalogu z rozpakowanymi plikami
źródłowymi jądra.  Jeżeli pliki źródłowe jądra znajdują się w
katalogu <filename>/usr/src/linux/</filename>, wtedy przy kompilowaniu do
polecenia musisz dodać opcję <literal>-I/usr/src/linux/include/</literal>.
</para>
</section>

<section id="customkernel"><title>Jakich narzędzi dostarcza system Debian w celu budowania własnych wersji jądra systemu?</title>
<para>
Użytkownicy którzy pragną zbudować własną wersję jądra systemu mogą
pobrać i zainstalować pakiet <systemitem
role="package">kernel-package</systemitem>.  Zawiera on skrypt wykonujący
kompilację i umożliwia stworzenie pakietu Debiana z gotowym jądrem przy
pomocy polecenia
</para>
<screen>
make-kpkg kernel_image
</screen>
<para>
, które należy wpisać znajdując się w katalogu z plikami źródłowymi
jądra.  Więcej informacji można uzyskać przy pomocy polecenia
</para>
<screen>
make-kpkg --help
</screen>
<para>
lub podręcznika systemowego
<citerefentry><refentrytitle>make-kpkg</refentrytitle><manvolnum>1</manvolnum></citerefentry>.
</para>
<para>
Ponieważ opisany pakiet nie zawiera plików źródłowych jądra, użytkownicy
muszą pobrać je oddzielnie z wybranego przez siebie serwera lub w postaci
pakietu Debiana jeśli pakiet kernel-source-wersja jest dostępny (gdzie wersja
to numer pożądanej wersji jądra).
</para>
<para>
Szczegółowe instrukcje dotyczące korzystania z pakietu <systemitem
role="package">kernel-package</systemitem> można znaleźć w pliku
<filename>/usr/share/doc/kernel-package/README.gz</filename>.  W skrócie
powinno się:
</para>
<itemizedlist>
<listitem>
<para>
Rozpakować pliki źródłowe jądra i przenieść się do nowo powstałego
katalogu przy pomocy polecenia <literal>cd</literal>.
</para>
</listitem>
<listitem>
<para>
Dostosować konfigurację jądra do własnych potrzeb przy pomocy jednego z
poniższych poleceń:
</para>
<itemizedlist>
<listitem>
<para>
<literal>make config</literal> (interfejs tekstowy zadający kolejno pytania).
</para>
</listitem>
<listitem>
<para>
<literal>make menuconfig</literal> (interfejs tekstowy wykorzystujący
bibliotekę ncurses z opcjami pogrupowanymi w menu).  Aby móc korzystać z
tego polecenia musisz mieć zainstalowany pakiet <systemitem
role="package">libncurses5-dev</systemitem>.
</para>
</listitem>
<listitem>
<para>
<literal>make xconfig</literal> (interfejs graficzny X11).  Korzystanie z tej
opcji wymaga zainstalowania istotnych pakietów związanych z X11 i Tcl/Tk.
</para>
</listitem>
</itemizedlist>
<para>
Wykorzystanie któregokolwiek z powyższych poleceń spowoduje utworzenie
nowego pliku <literal>.config</literal> w katalogu z plikami źródłowymi.
</para>
</listitem>
<listitem>
<para>
Wprowadzić polecenie: <literal>make-kpkg -rev TwojaNazwa.N
kernel_image</literal>, przy czym N oznacza nadany przez Ciebie numer.
Stworzony w wyniku tak przeprowadzonej kompilacji pakiet z jądrem będzie
posiadał unikatowy stworzony przez Ciebie symbol wersji TwojaNazwa.1, np.:
<literal>kernel-image-2.2.14_TwojaNazwa.1_i386.deb</literal> dla jądra w
wersji 2.2.14.
</para>
</listitem>
<listitem>
<para>
Zainstalować stworzony w ten sposób pakiet.
</para>
<itemizedlist>
<listitem>
<para>
Uruchom <literal>dpkg --install
/usr/src/kernel-image-VVV_Custom.N.deb</literal> aby zainstalować jądro.
Skrypt instalacyjny:
</para>
<itemizedlist>
<listitem>
<para>
uruchomi program ładujący system, LILO (jeśli ten jest zainstalowany),
</para>
</listitem>
<listitem>
<para>
zainstaluje skompilowane przez Ciebie jądro w katalogu /boot pod nazwą
vmlinuz_VVV-TwojaNazwa.N i stworzy odpowiednie dowiązania symboliczne.
</para>
</listitem>
<listitem>
<para>
zapyta użytkownika czy wykonać dyskietkę startową.  Dyskietka taka zawiera
jedynie plik z jądrem.  Aby dowiedzieć się więcej zobacz <xref
linkend="custombootdisk"/>.
</para>
</listitem>
</itemizedlist>
</listitem>
<listitem>
<para>
Aby wykorzystać jeden z pozostałych programów ładujących system takich jak
<systemitem role="package">grub</systemitem> czy <literal>loadlin</literal>,
skopiuj ten plik w inne miejsce (np.  do katalogu /boot/grub lub na partycję z
systemem plików <literal>MS-DOS</literal>).
</para>
</listitem>
</itemizedlist>
</listitem>
</itemizedlist>
</section>

<section id="custombootdisk"><title>W jaki sposób mogę wykonać własną wersję dyskietki startowej?</title>
<para>
Do tego celu możesz z powodzeniem posłużyć się pakietem <systemitem
role="package">boot-floppies</systemitem>, który znajduje się w sekcji
<literal>admin</literal> zasobów FTP Debiana.  Skrypt powłoki znajdujący
się w tym pakiecie tworzy dyskietki startowe w formacie
<literal>SYSLINUX</literal>.  Są to dyskietki z systemem plików
<literal>MS-DOS</literal>, których główny rekord startowy został
zmodyfikowany tak aby bezpośrednio ładował jądro Linuksa (lub innego
systemu w zależności od definicji zawartej w pliku syslinux.cfg znajdującym
się na dyskietce).  Pozostałe skrypty w tym pakiecie służą do tworzenia
dyskietek awaryjnych lub instalacyjnych.
</para>
<para>
Więcej informacji znajdziesz w pliku
<literal>/usr/doc/boot-floppies/README</literal> po zainstalowaniu pakietu
<systemitem role="package">boot-floppies</systemitem>.
</para>
</section>

<section id="modules"><title>Jakie narzędzia udostępnia Debian do obsługi modułów?</title>
<para>
Pakiet Debiana <systemitem role="package">modconf</systemitem> udostępnia
skrypt powłoki (<literal>/usr/sbin/modconf</literal>), który może być
wykorzystywany do dostosowywania konfiguracji modułów dla własnych potrzeb.
Skrypt ten oferuje interfejs oparty o menu, który pyta użytkownika o
szczegóły dotyczące modułów sterowników urządzeń w systemie.  Uzyskane
odpowiedzi służą do stworzenia odpowiednich wpisów w pliku
<literal>/etc/modules.conf</literal> (który zawiera listę aliasów/skrótów
i parametry które zostaną użyte przy ładowaniu modułów).  Są również
konieczne do stworzenia plików znajdujących się w katalogu
<literal>/etc/modutils/</literal> oraz zawartości pliku
<literal>/etc/modules</literal> (który zawiera listę modułów ładowanych w
czasie startu systemu).
</para>
<para>
Podobnie jak (nowe) pliki Configure.help, które teraz pomagają przy tworzeniu
własnych wersji jądra, pakiet modconf zawiera pliki pomocy (znajdujące się
w katalogu <literal>/usr/lib/modules_help/</literal>), które dostarczają
szczegółowych informacji na temat parametrów właściwych dla
poszczególnych modułów.
</para>
</section>

<section id="removeoldkernel"><title>Czy mogę bezpiecznie odinstalować pakiet ze starym jądrem?</title>
<para>
Tak.  Skrypt <literal>kernel-image-NNN.prerm</literal> sprawdza, czy jądro
które chcesz usunąć nie jest tym, którego właśnie używasz.  W każdym
przypadku możesz usunąć pakiety z niechcianym jądrem używając polecenia
</para>
<screen>
dpkg --purge --force-remove-essential kernel-image-VVV
</screen>
<para>
(zastępując oczywiście "VVV" numerem wersji jądra)
</para>
</section>

</chapter>

