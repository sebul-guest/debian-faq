<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="compat"><title>Questões sobre compatibilidade.</title>
<section id="arches"><title>Em quais arquiteturas de hardware o Debian GNU/Linux roda?</title>
<para>
O Debian GNU/Linux inclui os códigos-fonte completos para todos os programas
inclusos, portanto, eles devem rodar em qualquer sistema que seja suportado
pelo kernel Linux; veja a <ulink
url="http://www.linuxdoc.org/FAQ/Linux-FAQ/">FAQ do Linux</ulink> para
detalhes.
</para>
<para>
A versão atual do Debian GNU/Linux , a 9, contém uma completa distribuição
binária para as seguintes arquiteturas:
</para>
<para>
<emphasis>i386</emphasis>: essa cobre os PCs baseados em Intel e processadores
compatíveis, incluindo Intel 386, 486, Pentium, Pentium Pro, Pentium II (tanto
Klamath quanto Celeron), e Pentium III, e os processadores compatíveis da AMD,
Cyrix e outros, compatíveis com estes.
</para>
<para>
<emphasis>m68k</emphasis>: essa cobre as máquinas Amiga e Atari que possuem
processadores Motorola 680x0 (para x>=2); com MMU.
</para>
<para>
<emphasis>alpha</emphasis>: sistemas Alpha da Compaq/Digital.
</para>
<para>
<emphasis>sparc</emphasis>: essa cobre os sistemas SPARC da Sun e a maioria dos
sistemas UltraSPARC.
</para>
<para>
<emphasis>powerpc</emphasis>: essa cobre algumas máquinas IBM/Motorola
PowerPC, incluindo máquinas CHRP, PowerMac e PReP.
</para>
<para>
<emphasis>arm</emphasis>: máquinas ARM e StrongARM.
</para>
<para>
O desenvolvimento de distribuições binárias do Debian para as arquiteturas
Sparc64 (UltraSPARC nativo) e MIPS estão em andamento.
</para>
<para>
Para informações adicionais na carga (boot), particionamento do drive,
habilitando dispositivos PCMCIA (PC Card) e questões similares, por favor,
siga as instruções contidas no Manual de Instalação, o qual está
disponível no nosso site web em <ulink
url="http://www.br.debian.org/releases/stable/i386/install">http://www.br.debian.org/releases/stable/i386/install</ulink>.
</para>
</section>

<section id="otherdistribs"><title>Quão compatível é o Debian com outras distribuições de Linux?</title>
<para>
Desenvolvedores Debian se comunicam com outros criadores de distribuições
Linux para manter compatibilidade binária entre as distribuições.  A maior
parte dos produtos comerciais para Linux roda tão bem no Debian quanto no
sistema sobre o qual foram construídos.
</para>
<para>
O Debian GNU/Linux adere à <ulink url="http://www.pathname.com/fhs/">Estrutura
de Sistema de Arquivos do Linux (Linux File System Structure)</ulink>.  Apesar
disso, existe espaço para interpretações em algumas das regras dentro desse
padrão, então podem existir diferenças entre um sistema Debian e outros
sistemas Linux.  A mais nova versão desse padrão FSSTND é chamada de FHS e
planejamos mudar para ela em pouco tempo.
</para>
</section>

<section id="otherunices"><title>Quão compatível, em nível de código fonte, o Debian é em relação a outros sistemas Unix?</title>
<para>
Para a maior parte dos aplicativos, o código fonte do Linux é compatível com
outros sistemas Unix.  Ele tem suporte a praticamente tudo o que está
disponível em sistemas Unix System V e derivados livres e comerciais do BSD.
No mundo Unix, porém, essa afirmação praticamente não tem valor, pois não
há como prová-la.  Na área de desenvolvimento de software é necessário
haver compatibilidade completa ao invés de compatibilidade na "maioria" dos
casos.  Por causa disso, anos atrás surgiu a necessidade de padrões, e hoje
em dia o POSIX.1 (IEEE Standard 1003.1-1990) é um dos principais padrões para
compatibilidade em nível de código fonte entre sistemas operacionais estilo
Unix.
</para>
<para>
O Linux pretende aderir ao POSIX.1, mas os padrões POSIX custam muito dinheiro
e o certificado POSIX.1 (e FIPS 151-2) é realmente caro; isso dificulta o
trabalho de conformidade completa com o POSIX por parte dos desenvolvedores do
Linux.  Os custos da certificação fazem com que seja improvável que a Debian
receba um certificado de conformidade oficial mesmo que ela passe completamente
pelo conjunto de validação.  (O conjunto de validação agora está
disponível livremente, portanto espera-se que mais pessoas trabalhem em
questões acerca do POSIX.1.)
</para>
<para>
A Unifix GmbH (Braunschweig, Alemanha) desenvolveu um sistema Linux que recebeu
certificação de conformidade ao FIPS 151-2 (um superconjunto do POSIX.1).
Essa tecnologia esteve disponível na distribuição da própria Unifix,
chamada Unifix Linux 2.0, e no Linux-FT da Lasermoon.
</para>
</section>

<section id="otherpackages"><title>Posso usar pacotes Debian (arquivos ".deb") em meu sistema Red Hat/Slackware/...? Posso usar pacotes Red Hat (arquivos ".rpm") em meu sistema Debian GNU/Linux?</title>
<para>
Diferentes distribuições Linux usam diferentes formatos de pacotes e
programas de gerenciamento de pacotes.
</para>
<variablelist>
<varlistentry>
<term><emphasis role="strong">Você provavelmente pode:</emphasis></term>
<listitem>
<para>
Existe um programa para descompactar um pacote Debian num computador com outra
distribuição do Linux, e normalmente irá funcionar no sentido de que os
arquivos vão ser descompactados.  A recíproca provavelmente também é
verdadeira, ou seja, um programa para descompactar um pacote do Red Hat ou
Slackware num computador baseado no Debian Linux provavelmente conseguirá
descompactar o pacote e colocar a maior parte dos arquivos nos lugares
corretos.  Isso é conseqüência da existência do (e grande conformidade com
o) Padrão de Hierarquia para o Sistema de Arquivos do Linux (Linux File System
Standard).
</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis role="strong">Você provavelmente não quer:</emphasis></term>
<listitem>
<para>
a maior parte dos gerenciadores de pacotes escrevem arquivos de administração
quando são usados para descompactar um arquivo.  Esses arquivos de
administração geralmente não são padronizados.  Portanto, o efeito de
descompactar um pacote Debian em um computador "estranho" irá gerar efeitos
imprevisíveis (certamente improdutivos) no gerenciador de pacotes daquele
sistema.  Do mesmo modo, utilitários de outras distribuições podem conseguir
descompactar seus arquivos em sistemas Debian, mas provavelmente farão o
sistema de gerenciamento de pacotes da Debian falhar quando chegar a hora de
atualizar ou remover alguns pacotes, ou simplesmente listar exatamente quais
pacotes estão presentes em um sistema.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis role="strong">Uma maneira melhor:</emphasis></term>
<listitem>
<para>
o Padrão de Hierarquia para o Sistema de Arquivos do Linux (e portanto Debian
GNU/Linux) exige que subdiretórios sob <filename>/usr/local/</filename> fiquem
completamente sob a responsabilidade do usuário.  Portanto, os usuários podem
descompactar pacotes "estranhos" dentro desse diretório, e gerenciar sua
própria configuração, atualização e remoção individualmente.
</para>
</listitem>
</varlistentry>
</variablelist>
</section>

<section id="a.out"><title>O Debian pode rodar meus antiqüíssimos programas "a.out"?</title>
<para>
Você ainda tem um programa assim?  :-)
</para>
<para>
Para <emphasis>executar</emphasis> um programa cujo binário esteja em formato
<literal>a.out</literal> (isto é, QMAGIC ou ZMAGIC),
</para>
<itemizedlist>
<listitem>
<para>
Assegure-se que seu kernel tenha suporte a <literal>a.out</literal>, seja
diretamente (CONFIG_BINFMT_AOUT=y) ou como módulo (CONFIG_BINFMT_AOUT=m).  (O
pacote kernel-image do Debian possui o módulo <literal>binfmt_aout</literal>)
</para>
<para>
Se seu kernel suporta binários <literal>a.out</literal> por módulo, garanta
que o módulo <literal>binfmt_aout</literal> esteja carregado.  Você pode
fazer isso durante a inicialização incluindo a linha
<literal>binfmt_aout</literal> no arquivo <literal>/etc/modules</literal>.
Você pode fazer isso a partir da linha de comando executando o comando
<literal>insmod NOMEDIR/binfmt_aout.o</literal> onde <literal>NOMEDIR</literal>
é o nome do diretório onde os módulos que foram construídos para a versão
da kernel sendo rodada estão armazenados.  Em um sistema com a versão 2.0.36
da kernel, <literal>NOMEDIR</literal> provavelmente é
<literal>/lib/modules/2.2.17/fs/</literal>.
</para>
</listitem>
<listitem>
<para>
Instale o pacote <systemitem role="package">libc4</systemitem>, encontrado em
versões anteriores a versão 2.0 (porque naquele momento nós removemos o
pacote).  Você pode querer dar uma olhada num CD-ROM antigo do Debian (o
Debian 1.3.1 ainda tinha esse pacote), ou veja <ulink
url="ftp://archive.debian.org/debian-archive/">ftp://archive.debian.org/debian-archive/</ulink>
na internet.
</para>
</listitem>
<listitem>
<para>
Se o programa que você quer executar for um cliente X em
<literal>a.out</literal>, então instale o pacote <systemitem
role="package">xcompat</systemitem> (veja acima sobre a disponibilidade).
</para>
</listitem>
</itemizedlist>
<para>
Se você possui um aplicativo comercial em <literal>a.out</literal>, este seria
um bom momento para pedir que enviem uma atualização em
<literal>ELF</literal>.
</para>
</section>

<section id="libc5"><title>O Debian consegue rodar meus antigos programas em libc5?</title>
<para>
Sim.  Basta instalar as bibliotecas <systemitem
role="package">libc5</systemitem> necessárias, da seção
<literal>oldlibs</literal> (contendo pacotes antigos incluídos para
compatibilidade com aplicativos antigos).
</para>
</section>

<section id="libc5-compile"><title>O Debian pode ser usado para compilar programas em libc5?</title>
<para>
Sim.  Instale os pacotes <systemitem role="package">libc5-altdev</systemitem> e
<systemitem role="package">altgcc</systemitem> (da seção
<literal>oldlibs</literal>).  Você pode achar os programas
<command>gcc</command> e <command>g++</command> compilados em libc5 no
diretório <literal>/usr/i486-linuxlibc1/bin</literal>.  Coloque-os em sua
variável $PATH para que o <command>make</command> e outros programas o
executem primeiro.
</para>
<para>
Se você precisa compilar clientes X em libc5, instale os pacotes <systemitem
role="package">xlib6</systemitem> e <systemitem
role="package">xlib6-altdev</systemitem>.
</para>
<para>
Esteja ciente de que o ambiente libc5 não é mais totalmente suportado por
nossos outros pacotes.
</para>
</section>

<section id="non-debian-programs"><title>Como devo instalar um pacote não-Debian?</title>
<para>
Arquivos sob o diretório <literal>/usr/local/</literal> não estão sob
controle do sistema de gerenciamentos de pacotes Debian.  Assim sendo, é boa
prática colocar o código fonte de seu programa em /usr/local/src/.  Por
exemplo, você pode extrair os arquivos de um pacote chamado "foo.tar" dentro
do diretório <literal>/usr/local/src/foo</literal>.  Depois de compilá-lo,
coloque os binários em <literal>/usr/local/bin/</literal>, as bibliotecas em
<literal>/usr/local/lib</literal>, e os arquivos de configuração em
<literal>/usr/local/etc</literal>.
</para>
<para>
Se seus programas e/ou arquivos realmente precisam ser colocados em algum outro
diretório, você ainda pode mantê-los em <literal>/usr/local/</literal> e
fazer as ligações simbólicas apropriadas a partir do lugar necessário para
sua localização em <literal>/usr/local/</literal>.  Por exemplo, você pode
fazer a ligação
</para>
<screen>
ln -s /usr/local/bin/foo /usr/bin/foo
</screen>
<para>
De qualquer modo, se você obtiver um pacote cujo copyright permita
redistribuição, você deve pensar em fazer um pacote Debian a partir dele, e
enviá-lo para o sistema Debian.  Guias sobre como se tornar um desenvolvedor
de pacotes estão incluídos no Debian Policy manual (veja <xref
linkend="debiandocs"/>).
</para>
</section>

<section id="xlib6"><title>Por que aparece "Can't find libX11.so.6" ("Impossível encontrar libX11.so.6") quando tento rodar <literal>foo</literal>?</title>
<para>
Essa mensagem de erro pode significar que o programa está ligado à versão
<literal>libc5</literal> das bibliotecas do X11.  Nesse caso você precisa
instalar o pacote <systemitem role="package">xlib6</systemitem>, da seção
<literal>oldlibs</literal>.
</para>
<para>
Você pode obter mensagens de erro semelhantes que se referem ao arquivo
libXpm.so.4, nesse caso, você precisa instalar a versão libc5 da biblioteca
XPM, do pacote <systemitem role="package">xpm4.7</systemitem>, também na
seção <literal>oldlibs</literal>.
</para>
</section>

<section id="termcap"><title>Por que não consigo compilar programas que exigem a libtermcap?</title>
<para>
A Debian usa o banco de dados <literal>terminfo</literal> e a biblioteca de
rotas de interfaces de terminal <literal>ncurses</literal>, ao invés do banco
de dados <literal>termcap</literal> e a biblioteca <literal>termcap</literal>.
Os usuários que compilam programas que necessitam de algum conhecimento da
interface de terminal devem substituir referências a
<literal>libtermcap</literal> por referências a <literal>libncurses</literal>.
</para>
<para>
Para dar suporte a binários que já foram ligados à biblioteca
<literal>termcap</literal> e para os quais você não possui o código fonte, a
Debian oferece um pacote chamado <literal>termcap-compat</literal>.  Ele possui
os arquivos <literal>libtermcap.so.2</literal> e
<literal>/etc/termcap</literal>.  Instale esse pacote caso o programa falhe com
a mensagem de erro "can't load library 'libtermcap.so.2'" ("impossível
carregar biblioteca 'libtermcap.so.2'"), ou caso ele reclame da falta do
arquivo <literal>/etc/termcap</literal>.
</para>
</section>

<section id="accelx"><title>Por que não consigo instalar o AccelX?</title>
<para>
O AccelX usa a biblioteca <literal>termcap</literal> para a instalação.  Veja
<xref linkend="termcap"/> acima.
</para>
</section>

<section id="motifnls"><title>Por que minhas velhas aplicações XFree 2.1 Motif travam?</title>
<para>
Você precisa instalar o pacote <systemitem
role="package">motifnls</systemitem>, que provê os arquivos de configuração
do XFree-2.1 necessários para permitir aplicativos Motif compilados sobre o
XFree-2.1 rodar sobre o XFree-3.1.
</para>
<para>
Sem esses arquivos, Alguns aplicativos Motif compilados em outras máquinas
(como o Netscape) podem travar quando tentarem copiar ou colar de um ou para um
campo texto, e podem também exibir outros problemas.
</para>
</section>

</chapter>

