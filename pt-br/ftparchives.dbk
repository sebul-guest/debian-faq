<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="ftparchives"><title>Os repositórios FTP do Debian.</title>
<section id="dirtree"><title>O que são todos aqueles diretórios nos repositórios FTP do Debian?</title>
<para>
Os programas que foram empacotados para o Debian GNU/Linux estão disponíveis
em várias árvores de diretórios em cada espelho do Debian.
</para>
<para>
O diretório <literal>dists</literal> contém as "distribuições", e é o modo
canônico de acessar as versões Debian atualmente disponíveis (e versões
antigas).
</para>
</section>

<section id="dists"><title>Quantas distribuições Debian existem no diretório <literal>dists</literal>?</title>
<para>
Normalmente existem três distribuições, a distribuição "stable"
(estável), a distribuição "testing" (teste) e a distribuição "unstable"
(instável).  Às vezes, também há a distribuição "frozen" (paralizada).
</para>
</section>

<section id="codenames"><title>O que são todos esses nomes, como "slink", "potato", etc.?</title>
<para>
São apenas "codinomes".  Quando uma distribuição Debian está em estágio de
desenvolvimento, ela não possui um número de versão, e sim um codinome.  O
objetivo desses codinomes é facilitar o espelhamento das distribuições
Debian (se um diretório de verdade como <literal>unstable</literal> de repente
mudasse seu nome para <literal>stable</literal>, muita coisa teria que ser
baixada novamente).
</para>
<para>
Atualmente, <literal>stable</literal> é uma ligação simbólica para
<literal>potato</literal> (isto é, Debian GNU/Linux 9),
<literal>testing</literal> é uma ligação simbólica para
<literal>woody</literal> e <literal>unstable</literal> é uma ligação
simbólica para <literal>sid</literal>, o que significa que
<literal>potato</literal> é a atual distribuição estável e
<literal>woody</literal> é a atual distribuição teste.
<literal>sid</literal> é sempre a distribuição instável (veja <xref
linkend="sid"/>).
</para>
</section>

<section id="frozen"><title>E sobre a "frozen" (paralizada)?</title>
<para>
Quando a distribuição teste está madura o suficiente, ela é paralizada,
significando que nenhum código novo é admitido, somente ajustes e consertos
no código, se necessários.  Além disso, uma nova árvore teste é criada no
diretório <literal>dists</literal>, com um novo codinome.  A distribuição
paralisada passa por alguns meses de testes, com atualizações intermitentes e
ciclos de testes.  Mantemos um registro de bugs na distribuição "frozen" que
pode impedir que um pacote seja lançado.  Uma vez que o número de bugs desça
até níveis aceitáveis, a distribuição "frozen" torna-se estável e é
lançada, e a distribuição estável anterior torna-se obsoleta (e é movida
para o arquivo).
</para>
</section>

<section id="oldcodenames"><title>Que outros codinomes foram usados no passado?</title>
<para>
Outros codinomes que já foram usados são: <literal>buzz</literal> para a
versão 1.1, <literal>rex</literal> para a versão 1.2, <literal>bo</literal>
para as versões 1.3.x, <literal>hamm</literal> para a versão 2.0, e
<literal>slink</literal> para a versão 2.1.
</para>
</section>

<section id="sourceforcodenames"><title>De onde vieram esses codinomes?</title>
<para>
Até agora eles vieram de personagens do filme "Toy Story" da Pixar.
</para>
<itemizedlist>
<listitem>
<para>
<emphasis>buzz</emphasis> (Buzz Lightyear) era o astronauta;
</para>
</listitem>
<listitem>
<para>
<emphasis>rex</emphasis> era o tiranossauro;
</para>
</listitem>
<listitem>
<para>
<emphasis>bo</emphasis> (Bo Peep) era a menina que cuidava da ovelha;
</para>
</listitem>
<listitem>
<para>
<emphasis>hamm</emphasis> era o porquinho-cofre;
</para>
</listitem>
<listitem>
<para>
<emphasis>slink</emphasis> (Slinky Dog) era o cachorro de brinquedo;
</para>
</listitem>
<listitem>
<para>
<emphasis>potato</emphasis> era, claro, o Mr. Potato (Sr.  Batata);
</para>
</listitem>
<listitem>
<para>
<emphasis>woody</emphasis> era o vaqueiro.
</para>
</listitem>
</itemizedlist>
</section>

<section id="sid"><title>E sobre a "sid"?</title>
<para>
A <emphasis>sid</emphasis> ou <emphasis>unstable</emphasis> (instável) é o
lugar onde a maioria dos pacotes é inicialmente inserida à distribuição.
Ela nunca será lançada diretamente, porque pacotes que estão para serem
lançados, terão primeiro que ser incluídos na <emphasis>testing</emphasis>,
de maneira a ser lançada na <emphasis>stable</emphasis>.  A sid contém
pacotes para as arquiteturas lançadas e as que ainda não foram oficialmente
lançadas.
</para>
<para>
O nome "sid" também veio do desenho animado "Toy Story": Sid era o garoto na
casa ao lado, que destruía brinquedos :-)
</para>
<section id="sid-history"><title>Notas históricas sobre a "sid".</title>
<para>
Quando a sid ainda não existia, a organização do site de FTP tinha uma falha
grave: havia uma presunção de quando uma arquitetura fosse criada na
unstable, ela seria lançada quando aquela distribuição se tornasse a nova
stable.  Para muitas arquiteturas esse não era o caso, de forma que aqueles
diretórios tinham que ser movidos na hora do lançamento.  Isso não era
prático, pois essa tarefa diminuía a largura de banda.
</para>
<para>
Os administradores trabalharam sobre esse problema por vários anos, colocando
binários de versões ainda não lançadas em diretórios especiais chamados
"sid".  Para aquelas arquiteturas que não haviam sido lançadas ainda, a
primeira vez que elas foram lançadas, isso se deu através de uma ligação da
stable atual com a sid, e a partir desse momento, eles eram criados dentro da
árvore unstable normalmente.  Esse esquema era um tanto confuso para os
usuários.
</para>
<para>
Com o advento dos conjuntos de pacote (veja <xref linkend="pools"/>), pacotes
binários começaram a ser armazenados em um local canônico nesse conjunto,
qualquer que fosse a distribuição, portanto, o lançamento de uma
distribuição não causa mais o consumo de grande largura de banda nos
espelhos (há, na verdade, um consumo gradual da largura de banda através do
processo de desenvolvimento).
</para>
</section>

</section>

<section id="stable"><title>O que o diretório "stable" contém?</title>
<itemizedlist>
<listitem>
<para>
stable/main/: este diretório contém os pacotes que constituem formalmente a
versão mais recente do sistema Debian GNU/Linux.
</para>
<para>
Todos esses pacotes estão em conformidade com as <ulink
url="http://www.br.debian.org/social_contract#guidelines">Orientações sobre
Software Livre do Debian</ulink>, e são todos livremente utilizáveis e
distribuíveis.
</para>
</listitem>
<listitem>
<para>
stable/non-free/: este diretório contém pacotes cuja distribuição é
restrita de tal modo que requer que os distribuidores considerem cuidadosamente
as exigências de copyright especificadas.
</para>
<para>
Por exemplo, alguns pacotes têm licenças que proíbem distribuição
comercial.  Outros podem ser redistribuídos, mas são na verdade shareware, e
não freeware.  As licenças de cada um desses pacotes devem ser estudadas e
possivelmente negociada antes que os pacotes sejam incluídos em qualquer
redistribuição (por exemplo, em um CD-ROM).
</para>
</listitem>
<listitem>
<para>
stable/contrib/: este diretório contém pacotes que são DFSG-free e podem ser
<emphasis>livremente distribuídos</emphasis>, mas que por alguma razão
dependem de um pacote que <emphasis>não</emphasis> é livremente distribuível
e, portanto, disponível somente na seção non-free.
</para>
</listitem>
</itemizedlist>
</section>

<section id="testing"><title>O que o diretório "testing" contém?</title>
<para>
Pacotes são instalados no diretório "testing" depois que eles sofrem algum um
certo grau de teste na unstable.  Eles devem estar em sincronismo em todas as
arquiteturas para que eles foram construídos e não podem possuir
dependências que os tornem instáveis; eles também devem ter poucos bugs
críticos em relação as versões atualmente na "testing".  Desse modo,
esperamos que a "testing" esteja sempre perto de ser uma candidata a
lançamento.
</para>
</section>

<section id="unstable"><title>O que o diretório "unstable" contém?</title>
<para>
O diretório "unstable" contém uma imagem do atual sistema em desenvolvimento.
Usuários são bem vindos a usar e testar esses pacotes, mas são avisados
sobre seu estado incompleto.  A vantagem de se usar a distribuição "unstable"
é que você está sempre atualizado com os mais recentes lançamentos da
indústria GNU/Linux, mas se ele falhar: você terá que manter ambas as partes
:-)
</para>
<para>
Há também os subdiretórios main, contrib e non-free na "unstable", separados
pelos mesmos critérios da "stable".
</para>
</section>

<section id="archsections"><title>O que são todos aqueles diretórios dentro de <literal>dists/stable/main</literal>?</title>
<para>
Dentro de cada árvore de diretório principal
(<literal>dists/stable/main</literal>, <literal>dists/stable/contrib</literal>,
<literal>dists/stable/non-free</literal>, and
<literal>dists/unstable/main/</literal>, etc.), os pacotes binários residem em
subdiretórios cujos nomes indicam a arquitetura do processador para a qual
eles foram compilados:
</para>
<itemizedlist>
<listitem>
<para>
binary-all/, para pacotes que são independentes da arquitetura.  Isso inclui,
por exemplo, scripts Perl, ou documentação simples.
</para>
</listitem>
<listitem>
<para>
binary-i386/, para pacotes que são executados em máquinas PC 80x86.
</para>
</listitem>
<listitem>
<para>
binary-m68k/, para pacotes que são executados em máquinas baseadas no
processador Motorola 680x0.  Atualmente, isto é feita principalmente para
computadores Atari e Amiga, e também para algumas placas baseadas no padrão
industrial VME.
</para>
</listitem>
<listitem>
<para>
binary-sparc/, para pacotes que são executados em Sun SPARCStations.
</para>
</listitem>
<listitem>
<para>
binary-alpha/, para pacotes que são executados em máquinas DEC Alpha.
</para>
</listitem>
<listitem>
<para>
binary-powerpc/, para pacotes que são executados em máquinas PowerPC.
</para>
</listitem>
<listitem>
<para>
binary-arm/, para pacotes que são executados em máquinas ARM.
</para>
</listitem>
</itemizedlist>
<para>
Veja <xref linkend="arches"/> para mais informações.
</para>
</section>

<section id="source"><title>Onde está o código-fonte?</title>
<para>
O código fonte é incluído para tudo no sistema Debian.  Além disso, os
termos de licenças de muitos programas do sistema,
<emphasis>requerem</emphasis> que o código-fonte seja distribuído com os
programas, ou que uma oferta de obtenção do código-fonte acompanhe o
programa.
</para>
<para>
Normalmente o código-fonte é distribuído através do diretório "source",
que é paralelo a todos os diretórios binários específicos a cada
arquitetura, ou atualmente, no diretório <literal>pool</literal> (veja <xref
linkend="pools"/>).  Para obter o código-fonte sem ter que estar familiarizado
com a estrutura do repositório de FTP, tente um comando como <literal>apt-get
source MeuNomeDePacote</literal>.
</para>
<para>
Códigos-fonte podem ou não estar disponíveis para pacotes pertencentes ao
diretório "contrib" e "non-free", que não são partes formais do sistema
Debian.
</para>
</section>

<section id="pools"><title>O que existe no diretório <literal>pool</literal>?</title>
<para>
Historicamente, pacotes foram mantidos nos subdiretórios correspondentes do
diretório <literal>dists</literal>.  Isso demonstrou ser a causa de vários
problemas, como o grande consumo de largura de banda nos espelhos quando
grandes mudanças eram feitas.
</para>
<para>
Pacotes agora são mantidos em grandes "repositórios", estruturados de acordo
com o nome do pacote fonte.  Para fazer isso administrável, o repositório é
subdividido por seções ("main", "contrib" e "non-free") e pela primeira letra
do nome do pacote fonte.  Esses diretórios contém vários arquivos: os
arquivos binários para cada arquitetura, e os pacotes fonte de onde os pacotes
binários são gerados.
</para>
<para>
Você pode descobrir onde cada pacote é colocado executando um comando como
<literal>apt-cache showsrc NomeDoMeuPacote</literal> e olhando na linha
"Directory:".  Por exemplo, os pacotes <literal>apache</literal> são
armazenados em <literal>pool/main/a/apache/</literal>.  Como existem tantos
pacotes <literal>lib*</literal>, estes são tratados especialmente: por
exemplo, pacotes <systemitem role="package">libpaper</systemitem> são
armazenados em <literal>pool/main/libp/libpaper/</literal>.
</para>
<para>
Os diretórios <literal>dists</literal> ainda são usados para os arquivos
índices usados por programas como <literal>apt</literal>.  Também, na hora de
escrever, velhas distribuições não foram convertidas para o uso de
repositórios, deste modo, você verá caminhos contendo distribuições como
"potato" ou "woody" no campo de cabeçalho "Filename".
</para>
<para>
Normalmente, você não terá que se preocupar com isso, já que o
<literal>apt</literal> e provavelmente o <literal>dpkg-ftp</literal> (veja
<xref linkend="howtocurrent"/>) resolverá isso automaticamente.  Se você
deseja mais informações, veja o <ulink
url="http://people.debian.org/~joeyh/poolfaq">FAQ dos repositórios de pacotes
Debian</ulink>.
</para>
</section>

<section id="otherdirs"><title>Ok, e sobre os outros diretórios, que não estão dentro de <literal>dists</literal> ou <literal>pool</literal>?</title>
<para>
Os seguintes diretórios adicionais existem:
</para>
<variablelist>
<varlistentry>
<term><emphasis>/tools/</emphasis>:</term>
<listitem>
<para>
Utilitários DOS para a criação de discos de boot, particionamento de seu
disco rígido, compressão/descompressão de arquivos, e inicializar o Linux.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis>/doc/</emphasis>:</term>
<listitem>
<para>
Documentação Debian, FAQ do Debian, instruções de como enviar relatórios
de bugs.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis>/indices/</emphasis>:</term>
<listitem>
<para>
Vários índices, mantenedores, arquivos Packages-master, arquivos ignorados.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis>/project/</emphasis>:</term>
<listitem>
<para>
Muito material somente para desenvolvedores, como:
</para>
<variablelist>
<varlistentry>
<term><emphasis>project/experimental/</emphasis>:</term>
<listitem>
<para>
Este diretório contém pacotes e ferramentas que ainda estão sendo
desenvolvidas, e ainda estão do estágio alpha de testes.  Usuários não
devem usar pacotes daqui, porque eles podem ser perigosos e prejudiciais até
mesmo para pessoas experientes.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis>project/orphaned/</emphasis>:</term>
<listitem>
<para>
Pacotes que foram abandonados por seus desenvolvedores, e retirados da
distribuição.
</para>
</listitem>
</varlistentry>
</variablelist>
</listitem>
</varlistentry>
</variablelist>
</section>

</chapter>

