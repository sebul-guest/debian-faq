<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="pkg-basics"><title>Fundamentos do Sistema de Gerenciamento de Pacotes Debian</title>
<section id="package"><title>O que é um pacote Debian?</title>
<para>
De modo geral, pacotes contêm todos os arquivos necessários para implementar
um conjunto de recursos ou comandos relacionados.  Existem dois tipos de
pacotes Debian:
</para>
<itemizedlist>
<listitem>
<para>
<emphasis>Pacotes binários</emphasis>, que contêm arquivos executáveis, de
configuração, páginas de manual/info, informações de copyright e outras
documentações.  Esses pacotes são distribuídos em um formato de arquivo
específico do Debian (veja <xref linkend="deb-format"/>); eles são
normalmente caracterizados pela extensão '.deb'.  Pacotes binários podem ser
descompactados utilizando o utilitário <literal>dpkg</literal>; detalhes são
dados em sua página de manual.
</para>
</listitem>
<listitem>
<para>
<emphasis>Pacotes fonte</emphasis>, que consistem em um arquivo
<literal>.dsc</literal> que descreve o pacote fonte (incluindo os nomes dos
seguintes arquivos), um arquivo <literal>.orig.tar.gz</literal> que contém o
código fonte original sem modificações, em formato tar comprimido com gzip,
e normalmente um arquivo <literal>.diff.gz</literal> que contém as mudanças
específicas para o Debian em relação ao código fonte original.  O
utilitário <literal>dpkg-source</literal> empacota e desempacota pacotes fonte
Debian; os detalhes são fornecidos em sua página de manual.
</para>
</listitem>
</itemizedlist>
<para>
A instalação de software pelo sistema de pacotes usa "dependências" que são
cuidadosamente designadas pelos mantenedores dos pacotes.  Essas dependências
são documentadas no <literal>arquivo de controle</literal> associado a cada
pacote.  Por exemplo, o pacote que contém o compilador GNU C (<systemitem
role="package">gcc</systemitem>) "depende" do pacote <systemitem
role="package">binutils</systemitem>, que inclui o ligador e o montador
assembler.  Se um usuário tentar instalar o <systemitem
role="package">gcc</systemitem> sem antes instalar o <systemitem
role="package">binutils</systemitem>, o sistema de gerenciamento de pacotes
(dpkg) mostrará uma mensagem de erro indicando que o usuário também precisa
do <systemitem role="package">binutils</systemitem>, e irá deixar de instalar
o <systemitem role="package">gcc</systemitem>.  (Apesar disso, esse recurso
pode ser desativado por usuários insistentes, veja
<citerefentry><refentrytitle>dpkg</refentrytitle><manvolnum>8</manvolnum></citerefentry>).
Veja mais em <xref linkend="depends"/> abaixo.
</para>
<para>
As ferramentas de empacotamento do Debian podem ser usadas para:
</para>
<itemizedlist>
<listitem>
<para>
manipular e administrar pacotes ou partes de pacotes;
</para>
</listitem>
<listitem>
<para>
ajudar o usuário na divisão de pacotes que precisam ser transmitidos através
de mídia limitada em tamanho como disquetes;
</para>
</listitem>
<listitem>
<para>
ajudar desenvolvedores na construção de pacotes; e
</para>
</listitem>
<listitem>
<para>
ajudar usuários na instalação de pacotes que estejam em um servidor FTP
remoto.
</para>
</listitem>
</itemizedlist>
</section>

<section id="deb-format"><title>Qual é o formato de um pacote binário Debian?</title>
<para>
Um "pacote" Debian, ou um repositório de arquivos Debian contém os arquivos
executáveis, bibliotecas e documentação associada a um conjunto particular
de programas.  Normalmente, o arquivo Debian possui um nome que termina em
<literal>.deb</literal>.
</para>
<para>
O formato interno desses pacotes binários Debian é descrito na página de
manual
<citerefentry><refentrytitle>deb</refentrytitle><manvolnum>5</manvolnum></citerefentry>.
Esse formato interno está sujeito a mudanças (entre as principais versões do
Debian GNU/Linux), portanto, use sempre
<citerefentry><refentrytitle>dpkg-deb</refentrytitle><manvolnum>1</manvolnum></citerefentry>
para manipular arquivos <literal>.deb</literal>.
</para>
</section>

<section id="pkgname"><title>Por que os nomes de pacotes Debian são tão longos?</title>
<para>
Os nomes de pacotes de binários Debian seguem a seguinte convenção:
&lt;foo&gt;_&lt;NúmerodeVersão&gt;-&lt;NúmerodaRevisãoDebian&gt;.deb
</para>
<para>
Perceba que <literal>foo</literal> é supostamente o nome do pacote.  Para
verificação, pode-se descobrir o nome do pacote associado a um arquivo Debian
particular (.deb) através de um dos seguintes meios:
</para>
<itemizedlist>
<listitem>
<para>
inspecionar o arquivo "Packages" no diretório onde ele foi armazenado em um
site FTP do Debian.  Esse arquivo contém uma entrada descrevendo cada pacote;
o primeiro campo em cada entrada é formalmente o nome do pacote.
</para>
</listitem>
<listitem>
<para>
usar o comando <literal>dpkg --info foo_VVV-RRR.deb</literal> (onde VVV e RRR
são respectivamente os números da versão e da revisão do pacote em
questão).  Isso mostra entre outras coisas, o nome do pacote correspondente ao
arquivo sendo desempacotado.
</para>
</listitem>
</itemizedlist>
<para>
O componente <literal>VVV</literal> é o número de versão especificado pelo
desenvolvedor original do programa.  Não existem padrões aqui, então o
número de versão pode ter formatos tão diferentes quanto "19990513" e
"1.3.8pre1".
</para>
<para>
O componente <literal>RRR</literal> é o número da revisão Debian, e é
especificado pelo desenvolvedor Debian (ou um usuário individual, se ele
próprio decidir construir o pacote).  Esse número corresponde ao nível de
revisão do pacote Debian, portanto, um novo nível de revisão geralmente
significa mudanças no Makefile do Debian (<literal>debian/rules</literal>), no
arquivo de controle Debian (<literal>debian/control</literal>), nos scripts de
instalação e remoção (<literal>debian/p*</literal>), ou nos arquivos de
configuração usados com o pacote.
</para>
</section>

<section id="controlfile"><title>O que é um arquivo de controle Debian?</title>
<para>
Detalhes sobre o conteúdo de um arquivo de controle Debian podem ser
encontrados no "Debian Packaging Manual", no capítulo 4, veja <xref
linkend="debiandocs"/>.
</para>
<para>
Resumidamente, um arquivo de controle de exemplo é mostrado abaixo para o
pacote Debian "hello":
</para>
<screen>
Package: hello
Priority: optional
Section: devel
Installed-Size: 45
Maintainer: Adam Heath &lt;doogie@debian.org&gt;
Architecture: i386
Version: 1.3-16
Depends: libc6 (>= 2.1)
Description: The classic greeting, and a good example
 The GNU hello program produces a familiar, friendly greeting.  It
 allows nonprogrammers to use a classic computer science tool which
 would otherwise be unavailable to them.
 .
 Seriously, though: this is an example of how to do a Debian package.
 It is the Debian version of the GNU Project's `hello world' program
 (which is itself an example for the GNU Project).
</screen>
<para>
O campo "Package" indica o nome do pacote.  Esse é o nome pelo qual o pacote
pode ser manipulado pelas ferramentas de pacote, e é normalmente semelhante
para, mas não necessariamente igual a primeira parte do nome do arquivo do
pacote Debian.
</para>
<para>
O campo "Version" indica tanto o número de versão do desenvolvedor original
quanto o nível de revisão (na última parte) do pacote Debian deste programa,
como explicado em <xref linkend="pkgname"/>.
</para>
<para>
O campo "Architecture" especifica o processador para o qual este binário em
particular foi compilado.
</para>
<para>
O campo "Depends" apresenta uma lista de pacotes que devem estar instalados
para que este pacote seja instalado com sucesso.
</para>
<para>
O campo "Installed-Size" indica quanto espaço em disco o pacote instalado
consumirá.  Este campo é usado por front-ends de instalação, a fim de
mostrar se há espaço suficiente disponível para instalar o programa.
</para>
<para>
A linha "Section" indica a seção onde esse pacote Debian é armazenado nos
sites FTP do Debian.  Esse é um nome de subdiretório (dentro de um dos
diretórios principais, veja <xref linkend="dirtree"/>) onde o pacote é
armazenado.
</para>
<para>
O campo "Priority" indica quão importante é este pacote para instalação, de
forma que softwares semi-inteligentes como o dselect ou o apt possa ordenar o
pacote dentro de uma categoria de, por exemplo, pacotes opcionais instalados.
Veja <xref linkend="priority"/>.
</para>
<para>
O campo "Maintainer" indica o endereço eletrônico da pessoa responsável pela
manutenção deste pacote.
</para>
<para>
O campo "Description" mostra um breve resumo das características do pacote.
</para>
<para>
Para mais informações sobre todos os campos possíveis que um pacote pode
ter, por favor, veja o "Debian Packaging Manual", seção 4, "Control files and
their fields".
</para>
</section>

<section id="conffile"><title>O que é um conffile Debian?</title>
<para>
Conffiles é uma lista de arquivos de configuração (normalmente colocados em
<literal>/etc</literal>) que o sistema de gerenciamento de pacotes não
sobrescreverá quando o pacote for atualizado.  Isso assegura que valores
locais para os conteúdos desses arquivos serão preservados, e isso é uma
característica crítica que permite a atualização de pacotes enquanto o
sistema está funcionando.
</para>
<para>
Para determinar exatamente que arquivos são preservados durante uma
atualização, rode:
</para>
<screen>
dpkg --status package
</screen>
<para>
E olhe sob "Conffiles:".
</para>
</section>

<section id="maintscripts"><title>O que são os scripts Debian preinst, postinst, prerm e postrm?</title>
<para>
Esses arquivos são scripts executáveis que são executados automaticamente
antes ou depois de um pacote ser instalado.  Juntamente com um arquivo chamado
<literal>control</literal>, todos esses arquivos são parte da seção
"control" de um arquivo Debian.
</para>
<para>
Os arquivos individuais são:
</para>
<variablelist>
<varlistentry>
<term>preinst</term>
<listitem>
<para>
Este script é executado antes que o pacote seja desempacotado de seu arquivo
Debian (".deb").  Muitos scripts 'preinst' param serviços de pacotes que
estejam sendo atualizados até que sua instalação ou atualização esteja
completa (após a execução correta do script 'postinst').
</para>
</listitem>
</varlistentry>
<varlistentry>
<term>postinst</term>
<listitem>
<para>
Este script normalmente completa qualquer configuração exigida pelo pacote
<literal>foo</literal> depois que <literal>foo</literal> tenha sido
desempacotado de seu arquivo Debian (".deb").  Freqüentemente, scripts
'postinst' pedem informações ao usuário, e/ou avisam-no que se ele aceitar
valores padrões, deve lembrar-se de reconfigurar o pacote conforme a
necessidade.  Muitos scripts 'postint' executam quaisquer comandos necessários
para iniciar ou reiniciar um serviço uma vez que o novo pacote tenha sido
instalado ou atualizado.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term>prerm</term>
<listitem>
<para>
Este script geralmente para quaisquer daemons que estejam associados a um
pacote.  Ele é executado antes da remoção de arquivos associados ao pacote.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term>postrm</term>
<listitem>
<para>
Este script normalmente modifica ligações ou outros arquivos associados a
<literal>foo</literal>, e/ou remove arquivos criados pelo pacote.  (Veja
também <xref linkend="virtual"/>).
</para>
</listitem>
</varlistentry>
</variablelist>
<para>
Atualmente todos os arquivos de controle podem ser encontrados em
<literal>/var/lib/dpkg/info</literal>.  Os arquivos relevantes ao pacote
<literal>foo</literal> começam com o nome "foo", e possuem extensões
"preinst", "postinst", etc., conforme apropriado.  O arquivo
<literal>foo.list</literal> naquele diretório lista todos os arquivos que
foram instalados com o pacote <literal>foo</literal>.  (Perceba que a
localização desses arquivos é um aspecto interno do dpkg; você não deve
confiar nisso).
</para>
</section>

<section id="priority"><title>O que é um pacote Required/Important/Standard/Optional/Extra?</title>
<para>
A cada pacote Debian é atribuída uma <emphasis>prioridade</emphasis>
designada pelos mantenedores da distribuição, para auxiliar o sistema de
gerenciamento de pacotes.  As prioridades são:
</para>
<itemizedlist>
<listitem>
<para>
<emphasis role="strong">Required</emphasis>: pacotes que são necessários para
o funcionamento correto do sistema.
</para>
<para>
Isto inclui todas as ferramentas necessárias para o conserto de defeitos do
sistema.  Você não deve remover esses pacotes ou seu sistema poderá ficar
inoperante e você provavelmente não conseguirá nem mesmo usar o dpkg para
colocar tudo em seu devido lugar.  Sistemas que possuam apenas os pacotes
obrigatórios provavelmente não têm utilidade, mas têm funcionalidade
suficiente para permitir que o administrador dê boot e instale mais software.
</para>
</listitem>
<listitem>
<para>
<emphasis role="strong">Important</emphasis>: pacotes que devem estar presentes
em qualquer sistema estilo Unix.
</para>
<para>
Outros pacotes sem os quais o sistema não será útil ou não funcionará
corretamente estarão aqui.  Isto <emphasis>NÃO</emphasis> inclui Emacs, X11,
TeX ou quaisquer outros grandes aplicativos.  Estes pacotes constituem apenas a
infra-estrutura básica.
</para>
</listitem>
<listitem>
<para>
<emphasis role="strong">Standard</emphasis>: pacotes que são comuns em
qualquer sistema Linux, incluindo um sistema em modo caractere razoavelmente
pequeno, mas não muito limitado.
</para>
<para>
Isto é o que será instalado normalmente se o usuário não selecionar mais
nada.  Não inclui muitos aplicativos grandes, mas inclui Emacs (este é mais
uma peça de infra-estrutura do que um aplicativo) e uma parte razoável do TeX
e LaTeX (se isso for possível sem o X).
</para>
</listitem>
<listitem>
<para>
<emphasis role="strong">Optional</emphasis>: pacotes que incluem todos aqueles
que você normalmente gostaria de instalar mesmo se não soubesse o que eram,
ou não tivesse necessidades especiais.
</para>
<para>
Isto inclui o X11, uma distribuição completa do TeX e muitos aplicativos.
</para>
</listitem>
<listitem>
<para>
<emphasis role="strong">Extra</emphasis>: pacotes que entram em conflito com
outros de maior prioridade, ou provavelmente são úteis apenas se você sabe o
que são ou se possuem necessidades especiais que os tornam inadequados para
serem "optional".
</para>
</listitem>
</itemizedlist>
</section>

<section id="virtual"><title>O que é um pacote virtual?</title>
<para>
Um pacote virtual é um nome genérico que se aplica a qualquer elemento de um
grupo de pacotes, onde todos oferecem funcionalidade básica similar.  Por
exemplo, ambos os programas <literal>tin</literal> e <literal>trn</literal>
são leitores de news, e devem então satisfazer qualquer dependência de um
programa que exija um leitor de news em um sistema para funcionar ou ser útil.
Diz-se que ambos oferecem o "pacote virtual" chamado
<literal>news-reader</literal> (leitor de news).
</para>
<para>
Analogamente, <literal>smail</literal> e <literal>sendmail</literal> oferecem a
funcionalidade de um "mail transport agent" (agente de transporte de correio).
Diz-se então que ambos oferecem o pacote virtual "mail transport agent".  Se
qualquer um deles está instalado, programas que dependam de um
<literal>mail-transport-agent</literal> serão satisfeitos pela existência
deste pacote virtual.
</para>
<para>
O Debian fornece um mecanismo que, se mais de um pacote que oferece o mesmo
pacote virtual estiver instalado em um sistema, então os administradores podem
selecionar um deles como o pacote preferencial.  O comando pertinente é
<literal>update-alternatives</literal>, que é descrito adiante na seção
<xref linkend="diverse"/>.
</para>
</section>

<section id="depends"><title>O que significa dizer que um arquivo Depends/Recommends/Suggests/Conflicts/Replaces/Provides (Depende/Recomenda/Sugere/Conflita/Substitui/Oferece) outro pacote?</title>
<para>
O sistema de pacotes Debian possui uma gama de "dependências" entre pacotes
que são planejadas para indicar (em um único campo) o nível no qual o
Programa A pode operar independentemente da existência do Programa B em um
dado sistema:
</para>
<itemizedlist>
<listitem>
<para>
O Pacote A <emphasis>depende</emphasis> ("Depends") do Pacote B,se B deve
necessariamente estar instalado para que A possa ser executado.  Em alguns
casos, A depende não apenas de B, mas de uma versão de B.  Neste caso, a
dependência na versão é normalmente um limite mínimo, no sentido de que A
depende de qualquer versão de B mais recente que uma versão específica.
</para>
</listitem>
<listitem>
<para>
O Pacote A <emphasis>recomenda</emphasis> ("Recommends") o Pacote B, se o
mantenedor do pacote julga que a maioria dos usuários não usariam A sem ter
também a funcionalidade oferecida por B.
</para>
</listitem>
<listitem>
<para>
O Pacote A <emphasis>sugere</emphasis> ("Suggests") o Pacote B se B contém
arquivos que estão relacionados com (e geralmente melhoram) a funcionalidade
de A.
</para>
</listitem>
<listitem>
<para>
O Pacote A <emphasis>conflita</emphasis> ("Conflicts") com o Pacote B quando A
não funciona se B está instalado no sistema.  Normalmente, conflitos são
casos onde A contém arquivos que são melhorias em relação aos de B.
"Conflitos" geralmente são combinados com "substituições" ("replaces").
</para>
</listitem>
<listitem>
<para>
O Pacote A <emphasis>substitui</emphasis> ("Replaces") o Pacote B quando
arquivos instalados por B são removidos e (em alguns casos) sobrescritos por
arquivos de A.
</para>
</listitem>
<listitem>
<para>
O Pacote A <emphasis>oferece</emphasis> ("Provides") o Pacote B quando todos os
arquivos e a funcionalidade de B estão incorporados em A.  Este mecanismo
oferece um modo pelo qual os usuários com espaço em disco limitado tenham
apenas a parte do pacote A que eles realmente precisam.
</para>
</listitem>
</itemizedlist>
<para>
Informações mais detalhadas sobre o uso desses termos podem ser encontradas
no Packaging Manual e no Policy Manual.
</para>
</section>

<section id="pre-depends"><title>O que significa Pré-Dependência?</title>
<para>
"Pré-Dependência" é uma dependência especial.  No caso da maioria dos
pacotes, o <literal>dpkg</literal> descompactará seu arquivo (ou seja, seu
arquivo <literal>.deb</literal>) independentemente se os arquivos dos quais ele
depende existem ou não no sistema.  De forma simples, descompactar significa
que o <literal>dpkg</literal> irá extrair o conteúdo do arquivo que deveria
ser instalado em seu sistema de arquivos, e o colocará no seu lugar correto.
Se aquele pacote <emphasis>depende</emphasis> da existência de outros pacotes
do seu sistema, o <literal>dpkg</literal> se negará a completar a instalação
(executando sua ação "configure") até que os outros pacotes sejam
instalados.
</para>
<para>
Contudo, para alguns pacotes, o <literal>dpkg</literal> se negará a
descompactá-los até que certas dependências sejam resolvidas.  Diz-se que
tais pacotes "pré-dependem" ("Pre-Depends") da presença de outros pacotes.  O
projeto Debian ofereceu este mecanismo para dar suporte a atualização segura
de sistemas de formato <literal>a.out</literal> para o formato
<literal>ELF</literal>, onde a <emphasis>ordem</emphasis> em que os pacotes
eram descompactados era crítica.  Existem outras situações de atualização
onde este método é útil, por exemplo, os pacotes de prioridade "required",
que dependem da libC.
</para>
<para>
Como antes, mais informações detalhadas sobre isto podem ser encontradas no
Packaging Manual.
</para>
</section>

<section id="pkgstatus"><title>O que significa unknown/install/remove/purge/hold (desconhecido/instalar/remover/remover por completo/conservar) no estado do pacote?</title>
<para>
Estes "flags" indicam o que o usuário gostaria de fazer com o pacote (como
indicado pelas ações tomadas na seção "Select" do
<literal>dselect</literal>, ou pela chamada direta ao <literal>dpkg</literal>).
</para>
<para>
Seus significados são:
</para>
<itemizedlist>
<listitem>
<para>
unknown (desconhecido) - o usuário nunca indicou se ele quer o pacote;
</para>
</listitem>
<listitem>
<para>
install (instalar) - o usuário deseja o pacote instalado ou atualizado;
</para>
</listitem>
<listitem>
<para>
remove (remover) - o usuário deseja o pacote removido, mas não deseja excluir
qualquer arquivo de configuração existente;
</para>
</listitem>
<listitem>
<para>
purge (remover por completo) - o usuário deseja que o pacote seja removido por
completo, incluindo seus arquivos de configuração;
</para>
</listitem>
<listitem>
<para>
hold (conservar) - o usuário não deseja que esse pacote seja processado, ou
seja, ele deseja manter a atual versão que está instalada em seu sistema,
qualquer que ela seja.
</para>
</listitem>
</itemizedlist>
</section>

<section id="puttingonhold"><title>Como coloco um pacote em "hold"?</title>
<para>
Existem duas maneiras de colocar um pacote em "hold", com dpkg, ou com dselect.
</para>
<para>
Com o dpkg, você deverá apenas exportar a lista de seleções de pacotes,
com:
</para>
<screen>
dpkg --get-selections > selections.txt
</screen>
<para>
E então, editar o arquivo resultante <filename>selections.txt</filename>,
altere a linha que contém o pacote que você deseja manter, por exemplo,
<systemitem role="package">libc6</systemitem>,disso:
</para>
<screen>
libc6                                           install
</screen>
<para>
para isto:
</para>
<screen>
libc6                                           hold
</screen>
<para>
Salve o arquivo, e o recarregue na base de dados do dpkg, com:
</para>
<screen>
dpkg --set-selections < selections.txt
</screen>
<para>
Com o dselect, você deverá apenas entrar na tela de seleção de pacotes,
achar o pacote que você deseja manter em seu devido estado, e pressionar a
chave `=' (ou `H').  A mudanças aconteceram imediatamente após você sair da
tela de seleção de pacotes.
</para>
</section>

<section id="sourcepkgs"><title>Como instalo um pacote de fontes?</title>
<para>
Pacotes Debian de fontes não são "instalados" de fato, eles são apenas
descompactados no diretório em que você quiser construir os pacotes de
binários que eles produzem.  Pacotes de código fonte são distribuídos em um
diretório chamado <literal>source</literal>, e você pode baixá-los
manualmente, ou usar
</para>
<screen>
apt-get source foo
</screen>
<para>
para pegá-los (veja a página de manual
<citerefentry><refentrytitle>apt-get</refentrytitle><manvolnum>8</manvolnum></citerefentry>
em como configurar o APT para fazer isso).
</para>
</section>

<section id="sourcebuild"><title>Como construo pacotes binários a partir de um pacote de códigos fonte?</title>
<para>
Você precisará dos arquivos foo_*.dsc, foo_*.tar.gz e foo_*.diff.gz para
compilar o código fonte (atenção: não há .diff.gz para um pacote Debian
nativo).
</para>
<para>
Uma vez que você os tenha, se você possuir o pacote <systemitem
role="package">dpkg-dev</systemitem> instalado, o comando:
</para>
<screen>
dpkg-source -x foo_version-revision.dsc
</screen>
<para>
extrairá o pacote no diretório chamado <literal>foo-versão</literal>.
</para>
<para>
Se você deseja apenas compilar o pacote, deve ir para o diretório
<literal>foo-versão</literal> e executar o comando
</para>
<screen>
debian/rules build
</screen>
<para>
para construir o programa, depois
</para>
<screen>
debian/rules binary
</screen>
<para>
como root, para construir o pacote, e em seguida
</para>
<screen>
dpkg -i ../foo_versão-revisão_arq.deb
</screen>
<para>
para instalar o pacote recentemente construído.
</para>
</section>

<section id="creatingdebs"><title>Como eu crio pacotes Debian?</title>
<para>
Para descrições mais detalhadas sobre isso, leia o New Maintainers' Guide,
disponível no pacote <systemitem role="package">maint-guide</systemitem>, ou
em <ulink
url="ftp://ftp.debian.org/debian/doc/package-developer/maint-guide.html.tar.gz">ftp://ftp.debian.org/debian/doc/package-developer/maint-guide.html.tar.gz</ulink>.
</para>
</section>

</chapter>

