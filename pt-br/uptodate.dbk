<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="uptodate"><title>Mantendo seu Sistema Debian Atualizado.</title>
<para>
Uma das metas do Debian é prover um caminho consistente de atualização e um
processo seguro para realizar a mesma tarefa, e sempre fazemos o possível para
que as atualizações de versões anteriores possam ser feitas da melhor forma
possível.  No caso em que haja alguma nota importante para acrescentar ao
processo de atualização, o pacote irá alertar o usuário, e possivelmente
oferecer uma solução para possíveis problemas.
</para>
<para>
Você também deverá ler as Release Notes (Notas de lançamento), documento
que descreve os detalhes para atualizações específicas, enviado em todos os
CDs do Debian, e disponível na WWW em <ulink
url="http://www.debian.org/releases/stable/i386/release-notes/">http://www.debian.org/releases/stable/i386/release-notes/</ulink>.
</para>
<section id="libc5to6upgrade"><title>Como posso atualizar minha distribuição Debian 1.3.1 (ou anterior), baseada na libc5, para a 2.0 (ou posterior), baseada na libc6?</title>
<para>
Há vários modos de atualizar:
</para>
<itemizedlist>
<listitem>
<para>
Usando um simples script de shell chamado <literal>autoup.sh</literal>, que
atualiza os pacotes mais importantes.  Depois que o
<literal>autoup.sh</literal> terminou seu serviço, você pode usar o dselect
para instalar os pacotes restantes em massa.  Este é provavelmente o método
recomendado, mas não o único.
</para>
<para>
Atualmente, a última versão de <literal>autoup.sh</literal> pode ser
encontrada nos seguintes lugares:
</para>
<itemizedlist>
<listitem>
<para>
<ulink
url="http://www.br.debian.org/releases/2.0/autoup/">http://www.br.debian.org/releases/2.0/autoup/</ulink>
</para>
</listitem>
<listitem>
<para>
<ulink
url="http://www.taz.net.au/autoup/">http://www.taz.net.au/autoup/</ulink>
</para>
</listitem>
<listitem>
<para>
<ulink
url="http://csanders.vicnet.net.au/autoup/">http://csanders.vicnet.net.au/autoup/</ulink>
</para>
</listitem>
</itemizedlist>
</listitem>
<listitem>
<para>
Seguindo atentamente o <ulink
url="http://ftp.debian.org/debian/doc/libc5-libc6-Mini-HOWTO.txt">libc5-libc6-Mini-HOWTO</ulink>
e atualizando os pacotes mais importantes na mão.  O
<literal>autoup.sh</literal> é baseado neste Mini-HOWTO, logo este método
deve funcionar mais ou menos como o <literal>autoup.sh</literal>.
</para>
</listitem>
<listitem>
<para>
Usando um <literal>apt</literal> baseado em libc5.  APT significa "A Package
Tool" ("Uma Ferramenta de Pacotes"), e pode substituir o dselect algum dia.
Atualmente, ele funciona apenas como uma interface de linha de comando, ou como
um método de acesso para o dselect.  Você pode encontrar uma versão para
libc5 no diretório <literal>dists/slink/main/upgrade-older-i386</literal> nos
repositórios Debian.
</para>
</listitem>
<listitem>
<para>
Usando apenas o dselect, sem atualizar nenhum pacote à mão primeiro.  É
altamente recomendável que você NÃO use este método se puder evitar, porque
atualmente o dselect sozinho não instala os pacotes na ordem ótima.  O APT
funciona muito melhor e é mais seguro.
</para>
</listitem>
</itemizedlist>
</section>

<section id="howtocurrent"><title>Como posso manter meu sistema Debian atualizado?</title>
<para>
A pessoa poderia simplesmente fazer um ftp anônimo a um repositório Debian,
examinar os diretórios até achar o arquivo desejado, pegá-lo, e finalmente
instalá-lo usando o <literal>dpkg</literal>.  Observe que o
<literal>dpkg</literal> instalará os arquivos atualizados em seu lugar
correto, mesmo num sistema que esteja rodando.  Às vezes, um pacote revisado
precisa da instalação de uma versão revisada de outro pacote.  Nesse caso, a
instalação falhará a menos que o outro pacote seja instalado.
</para>
<para>
Muitas pessoas acham que essa abordagem consome muito tempo, já que o Debian
se desenvolve muito rapidamente - normalmente, uma dúzia ou mais pacotes são
atualizados toda semana.  Este número é maior logo antes do lançamento de
uma nova versão.  Para lidar com esta avalanche, muitas pessoas preferem usar
programas automatizados.  Vários pacotes diferentes estão disponíveis para
este propósito:
</para>
<section id="apt"><title>APT</title>
<para>
O APT é um sistema de administração de pacotes de software, isto é,
binários Debian e pacotes fonte.  O apt-get é a ferramenta de linha de
comando para trabalhar com pacotes, e o método APT do dselect é uma interface
para o APT através do <command>dselect</command>, ambos oferecem um modo mais
simples e seguro de instalar e atualizar pacotes.
</para>
<para>
Para usar o método APT do dselect, execute o <command>dselect</command>,
escolha a opção "0" ("Choose de access method to use" - "Escolha o método de
acesso a ser usado"), destaque a opção "apt" e especifique a fonte do APT.
Para fontes FTP e HTTP, você precisará do URL onde os diretórios Debian
residem, nome da versão (você pode usar os nomes stable/unstable) e a seção
da distribuição.
</para>
<para>
Se você quer usar os CDs para instalar os pacotes, insira cada CD do Debian no
CD-ROM e rode o apt-cdrom.  Então, quando selecionar o método APT você não
necessitará alterar a lista de fontes.
</para>
<para>
As características do APT "ordenação completa de instalação", "capacidade
de múltiplas fontes" e várias outras características únicas, veja o User
Guide em <literal>/usr/share/doc/apt/guide.html/index.html</literal>.
</para>
</section>

<section id="dpkg-ftp"><title>dpkg-ftp</title>
<para>
Este é um método antigo.  O APT é o método recomendado no momento.
</para>
<para>
Este é um método de acesso para o <command>dselect</command>.  Ele pode ser
chamado de dentro do <command>dselect</command>, permitindo assim que o
usuário baixe arquivos e os instale diretamente em um único passo.  Para
fazer isso, execute o programa <command>dselect</command>, escolha a opção
"0" ("Choose de access method to use" - "Escolha o método de acesso a ser
usado"), destaque a opção "ftp" e especifique o hostname e diretório remoto.
O <command>dpkg-ftp</command> baixará automaticamente os arquivos que forem
selecionados (tanto nesta sessão do <command>dselect</command>, quanto nas
anteriores).
</para>
<para>
Observe que, ao contrário do programa <command>mirror</command>, o
<command>dpkg-ftp</command> não pega tudo que está num site espelho.  Ao
invés disso, ele baixa apenas os arquivos que você escolheu (da primeira vez
que iniciou o <command>dpkg-ftp</command>), e que precisam ser atualizados.
</para>
</section>

<section id="mirror"><title>mirror</title>
<para>
Este script Perl, e seu (opcional) administrador de programas chamado
<command>mirror-master</command>, pode ser usado para buscar partes
específicas de uma árvore de diretórios de um host especificado
<emphasis>via</emphasis> FTP anônimo.
</para>
<para>
O <command>mirror</command> é particularmente útil para baixar grandes
volumes de softwares.  Depois da primeira vez em que os arquivos foram baixados
de um site, um arquivo chamado <literal>.mirrorinfo</literal> é armazenado no
host local.  Mudanças no filesystem remoto são detectadas automaticamente
pelo <command>mirror</command>, que compara esse arquivo a um arquivo similar
no sistema remoto e baixa somente os arquivos que sofreram mudanças.
</para>
<para>
O programa <command>mirror</command> é geralmente útil na atualização de
cópias locais de arvores de diretórios remotas.  Os arquivos procurados não
precisam ser arquivos Debian (Como o <command>mirror</command> é um script
Perl, ele pode também rodar em sistemas não Unix).  Embora o programa
<command>mirror</command> ofereça mecanismos para exclusão de nomes de
arquivos que correspondam as strings especificadas pelos usuários, esse
programa é muito útil quando o objetivo é baixar a árvore de diretórios
inteira, ao invés de pacotes selecionados.
</para>
</section>

<section id="dftp"><title>dftp</title>
<para>
Este script em Perl pode ser usado para buscar pacotes Debian de um host
específico.  Ele começa baixando os arquivos Packages.gz dos diretórios
especificados pelo usuário (por exemplo, stable, contrib, non-free) e mostra
uma lista de pacotes.  Estes são colocados em várias seções: novos
upgrades, downgrades, novos pacotes, upgrades ignorados e pacotes ignorados.
Daí, o usuário escolhe os pacotes desejados e o dftp os baixa e os instala.
Isto faz com que seja muito fácil ter seu sistema Debian 100% atualizado sem
ter que baixar os pacotes que você não vai instalar.
</para>
<para>
O <command>dftp</command> pode ser usado para chamar o
<command>dselect</command> (veja <xref linkend="dselect"/>), fornecendo assim
uma maneira integrada de buscar e atualizar os pacotes Debian de um sistema.
Após o término da instalação, outro comando <command>dftp</command> pode
ser usado para remover os arquivos de pacotes (".deb").  Mudanças no sistema
de arquivos remoto são rastreadas automaticamente pelo
<command>dftp</command>, que compara o arquivo Packages.gz local com os
arquivos no sistema remoto.
</para>
</section>

<section id="dpkg-mountable"><title>dpkg-mountable</title>
<para>
O dpkg-mountable adiciona um método de acesso chamado "muntable" a lista do
dselect, que permite você instalar de qualquer sistema de arquivos
especificado pelo /etc/fstab (por exemplo, uma partição de disco rígido
normal, ou um servidor NFS), o qual ele irá automaticamente montar ou
desmontar para você se necessário.
</para>
<para>
Ele também possui algumas características extras não encontradas nos
métodos padrão do dselect, como abastecimento de uma árvore de arquivos
local (tanto paralelo à distribuição principal, quanto totalmente separado),
e somente obtendo pacotes que são necessários, ao invés do demorado exame de
diretórios, como também registrando todas as ações do dpkg no método de
instalação.
</para>
</section>

</section>

<section id="upgradesingle"><title>Preciso mudar para o modo monousuário para atualizar um pacote?</title>
<para>
Não.  Os pacotes podem ser atualizados, até em sistemas em execução.  O
Debian tem um programa chamado <literal>start-stop-daemon</literal>, que é
chamado para parar, e depois reiniciar, se necessário, processos em
execução, durante a atualização de um pacote.
</para>
</section>

<section id="savedebs"><title>Preciso manter todos aqueles arquivos .deb em meu disco?</title>
<para>
Não.  Se você tiver baixado os arquivos para seu disco (que não é
absolutamente necessário, veja acima as descrições do dpkg-ftp ou do dftp),
então, após você ter instalado os pacotes, você pode removê-los de seu
sistema.
</para>
</section>

<section id="keepingalog"><title>Como posso manter um registro dos pacotes que adicionei ao sistema?</title>
<para>
O <command>dpkg</command> mantém um registro dos pacotes que foram
desempacotados, configurados, removidos, e/ou apagados, mas não mantém
(atualmente) um registro da atividade do terminal que ocorreu enquanto o pacote
estava sendo manipulado.  Alguns usuários superam esse problema simplesmente
usando <literal>tee</literal>, assim:
</para>
<screen>
dpkg -iGOEB -R main/binary non-free/binary contrib/binary | \
    tee -a /root/dpkg.log
</screen>
<para>
O mesmo comando escrito usando opções longas:
</para>
<screen>
dpkg --install --refuse-downgrade --selected-only \
  --skip-same-version --auto-deconfigure \
  --recursive main/binary non-free/binary contrib/binary | \
    tee -a /root/dpkg.log
</screen>
</section>

</chapter>

