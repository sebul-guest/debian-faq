<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="compat"><title>Питання сумісності</title>
<section id="arches"><title>На яких апаратних архітектурах та системах запускається Debian GNU/Linux?</title>
<para>
Debian GNU/Linux містить повні джерельні коди для
всіх включених в неї програм, тож вона
повинна працювати на всіх системах, що
підтримуються ядром Linux.  Зверніться до <ulink
url="http://en.tldp.org/FAQ/Linux-FAQ/intro.html#DOES-LINUX-RUN-ON-MY-COMPUTER">Linux
FAQ</ulink> для детальнішого розгляду цього
питання.
</para>
<para>
Поточна версія Debian GNU/Linux, 9, містить повні
двійкові збірки для наступних архітектур:
</para>
<para>
<emphasis>i386</emphasis>: це всі ПК, основані на
процесорах Intel та сумісних з ними,
включаючи Intel 386, 486, Pentium, Pentium Pro, Pentium II (Klamath і
Celeron), та Pentium III, та більшість сумісних
процесорів виробництва AMD, Cyrix та інших.
</para>
<para>
<emphasis>m68k</emphasis>: це машини Amiga та ATARI з
процесорами Motorola 680x0 для x>=2, з MMU.
</para>
<para>
<emphasis>alpha</emphasis>: системи Alpha виробництва
Compaq/Digital.
</para>
<para>
<emphasis>sparc</emphasis>: системи виробництва Sun, SPARC
та більшість систем UltraSPARC.
</para>
<para>
<emphasis>powerpc</emphasis>: деякі системи PowerPC
виробництва IBM/Motorola включно з CHRP, PowerMac і PReP.
</para>
<para>
<emphasis>arm</emphasis>: системи ARM і StrongARM.
</para>
<para>
<emphasis>mips</emphasis>: системи SGI MIPS, Indy та Indigo2;
<emphasis>mipsel</emphasis>: little-endian MIPS-системи, Digital
DECstation..
</para>
<para>
<emphasis>hppa</emphasis>: комп'ютери PA-RISC виробництва
Hewlett-Packard (712, C3000, L2000, A500).
</para>
<para>
<emphasis>ia64</emphasis>: комп'ютери Intel IA-64 („Itanium“).
</para>
<para>
<emphasis>s390</emphasis>: Великі системи IBM S/390.
</para>
<para>
Розглядається питання розробки двійкових
збірок Debian для архітектури Sparc64 (UltraSPARC).
</para>
<para>
З інших питань про завантаження, поділ
вашого диску на розділи, підключення
пристроїв PCMCIA та подібних, будь ласка,
користайтесь інструкціями, викладеними у
Підручнику по встановленню, що доступний в
тенетах за адресою <ulink
url="http://www.debian.org/releases/stable/installmanual">http://www.debian.org/releases/stable/installmanual</ulink>.
</para>
</section>

<section id="otherdistribs"><title>Наскільки Debian сумісний з іншими збірками Linux?</title>
<para>
Розробники Debian взаємодіють з авторами
інших збірок Linux щоб досягти двійкової
сумісності між дистрибутивами Linux.
Більшість комерційних програм під Linux
запускаються в Debian так само добре, як і в
системі, для котрої вони були розроблені.
</para>
<para>
Debian GNU/Linux дотримується <ulink
url="http://www.pathname.com/fhs/">стандарту ієрархії
файлової системи Linux</ulink>.  Оскільки деякі
положення стандарту можна трактувати
по-різному, існують відмінності між Debian та
іншими Linux-системами.
</para>
<para>
Debian GNU/Linux підтримує програмне
забезпечення, розроблене в рамках
стандарту <ulink url="http://www.linuxbase.org/">Linux Standard Base
(LSB)</ulink>.  LSB — це специфікація, що дозволяє
одному й тому ж двійковому пакунку
використовуватись в різних дистрибутивах.
Пакунки для випуску Debian Etch не повинні
протирічити вимогам LBS, версії 1.3.  На момент
написання цього тексту Debian GNU/Linux формально
не є LSB-сертифікованою.  Однак, деякі збірки,
похідні від Debian, є такими.  Дискусія та
координація зусиль з приводу приведення
Debian до відповідності вимогам LSB має місце в
<ulink url="http://lists.debian.org/debian-lsb/">списку розсилки
debian-lsb</ulink>.
</para>
</section>

<section id="otherunices"><title>Наскільки джерельні коди Debian сумісні з іншими Unix-системами?</title>
<para>
У більшості Linux-додатків джерельні коди
сумісні з іншими Unix-системами.  Вони
підтримуються майже усіма доступними
системами Unix System V та вільними і
комерційними BSD-подібними системами.  Проте
у світі Unix такі заяви можна робити лише
приблизно, оскільки немає способів
перевірити їх.  У площині розробки
програмного забезпечення потрібна повна
сумісність, а не „сумісність у більшості
випадків“.  Тож потреба у стандарті
виникла давно і зараз POSIX.1 (IEEE Standard 1003.1-1990) є
одним з основних стандартів сумісності
джерельних кодів у Unix-подібних операційних
системах.
</para>
<para>
Linux має намір дотримуватись стандарту POSIX.1,
але стандарти POSIX вартують неабияких
грошей і сертифікація POSIX.1 (та FIPS151-2) є
доволі дорогою; це значно ускладнює роботу
розробників Linux стосовно повної
відповідності стандартам POSIX.  Через
вартість сертифікації схоже, що Debian не
отримає офіційне сертифіковане
підтвердження, навіть якщо він повністю
пройде випробувальні тести.  З недавнього
часу перевірочний комплект знаходиться у
вільному доступі, тож очікується, що все
більше людей будуть писати програми у
відповідності до вимог POSIX.1.
</para>
<para>
Unifix GmbH (Брауншвайг, Німеччина) розробили
систему Linux, що була сертифікована FIPS 151-2
(надмножина POSIX.1).  Ця технологія була
доступна у власній збірці Unifix, що отримала
назву Unixfix Linux 2.0 та у Linux-FT виробництва Lasermoon.
</para>
</section>

<section id="otherpackages"><title>Чи можу я використовувати пакунки Debian (файли „.deb“) в моїй системі RedHat/Slackware/... Linux? Чи можу я використовувати пакунки RedHat (файли „.rpm“) у моїй Debian GNU/Linux?</title>
<para>
Різні збірки Linux використовують різні
формати пакунків та різні програми для
керування ними.
</para>
<variablelist>
<varlistentry>
<term><emphasis role="strong">Ви, ймовірно, зможете:</emphasis></term>
<listitem>
<para>
Якщо у „чужій“ збірці встановлена та
налаштована програма для розпаковування
пакунків Debian, то ви зможете розпакувати
пакунок в такій системі.  Зворотне
твердження також вірне, тобто ви зможете
розпакувати та правильно розташувати
файли з пакунку RedHat чи Slackware на комп'ютері з
Debian GNU/Linux за допомогою спеціальної
програми.  Це відбувається завдяки
існуванню та підтримці Стандарту ієрархії
файлової системи Linux.  Для перетворення між
різними форматами пакунків використовують
пакунок <ulink url="http://packages.debian.org/alien">alien</ulink>.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis role="strong">Ви, мабуть, не захочете:</emphasis></term>
<listitem>
<para>
Більшість програм для керування пакунками
використовують спеціальні керуючі файли
для стискання та пакування архівів.  Ці
файли не є стандартизованими.  А тому
результат розпаковування пакунку Debian на
чужорідній системі може мати
непередбачувані (як правило, руйнівні)
наслідки для керівника пакунків даної
системи.  Аналогічно, інструменти з інших
збірок можуть успішно розпакуватись в Debian,
але при цьому призведуть до помилок в
системі керування пакунками, коли буде
потрібно оновити систему чи видалити
якийсь інший пакунок або ж навіть просто
при виводі списку встановлених пакунків.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis role="strong">Кращий шлях:</emphasis></term>
<listitem>
<para>
Стандарт файлової системи Linux (а отже і Debian
GNU/Linux) вимагає, щоб підтеки в /usr/local/ були
віддані на розсуд користувача.  Таким чином
користувач може розпакувати чужорідні
пакунки сюди, а далі налаштовувати і
видаляти їх окремо.
</para>
</listitem>
</varlistentry>
</variablelist>
</section>

<section id="libc5"><title>Чи зможе Debian запустити мої старі libc5-програми?</title>
<para>
Так.  Просто встановіть необхідні <systemitem
role="package">libc5</systemitem> з секції <literal>oldlibs</literal>,
котра містить застарілі пакунки, що були
включені для сумісності з старими
програмами..
</para>
</section>

<section id="libc5-compile"><title>Чи можна використовувати Debian для компіляції libc5-програм?</title>
<para>
Так.  Встановіть пакунки <systemitem
role="package">libc5-altdev</systemitem> та <systemitem
role="package">altgcc</systemitem> з секції <literal>oldlibs</literal>.
В теці <literal>/usr/i486-linuxlibc1/bin</literal> ви знайдете
спеціальний <command>gcc</command> та <command>g++</command>.
Додайте її до вашої змінної $РАТН щоб make та
інші програми могли їх викликати.
</para>
<para>
Майте на увазі, що середовище libc5 не
повністю підтримується іншими нашими
пакунками.
</para>
</section>

<section id="non-debian-programs"><title>Як я можу встановити не-Debian програму?</title>
<para>
Файли в теці <literal>/usr/local</literal> не
контролюються системою керування пакунків
Debian.  Тому гарним вибором буде розміщення
джерельних кодів таких програм в
<literal>/usr/local/src/</literal>.  Наприклад, ви можете
розпакувати файли з архіву foo.tar до теки
<literal>/usr/local/src/foo/</literal>.  Після того, як ви
скомпілюєте їх, помістіть двійкові файли
до <literal>/usr/local/bin/</literal>, бібліотеки — до
<literal>/usr/local/lib/</literal>, а конфігураційні
файли — до <literal>/usr/local/etc/</literal>.
</para>
<para>
Якщо ваша програма та/або інші файли
справді мусять знаходитись у якомусь
іншому місці, ви все ж можете залишити їх в
<literal>/usr/local/</literal> та задати необхідний
символьне посилання з необхідного
місцезнаходження до <literal>/usr/local/</literal>,
наприклад:
</para>
<screen>
ln -s /usr/local/bin/foo /usr/bin/foo
</screen>
<para>
В будь-якому випадку, якщо ви отримали
пакунок, що дозволяє перезбірку, вам
потрібно задуматись над створенням
Debian-пакунку з нього, та завантаженням до
системи Debian.  Посібник для
розробника-початківця пакунків входить до
підручника „Політика Debian“ (див.  <xref
linkend="debiandocs"/>).
</para>
</section>

<section id="termcap"><title>Чому я не можу скомпілювати програми, що вимагають libtermcap?</title>
<para>
Debian радше використовує базу даних
<literal>terminfo</literal> та бібліотеку процедур
термінальних інтерфейсів <literal>ncurses</literal>,
аніж базу даних <literal>termcap</literal> та
бібліотеку <literal>libtermcap</literal>.  Користувачі,
що компілюють програми, яким потрібно щось
знати про інтерфейси терміналу, повинні
замінити звернення до <literal>libtermcap</literal>
зверненням до <literal>libtermcurses</literal>.
</para>
<para>
Для підтримки двійкових файлів, що вже
зв'язані з бібліотекою <literal>termcap</literal> і для
яких ви не маєте джерельних кодів, Debian
пропонує пакунок, що називається <systemitem
role="package">termcap-compat</systemitem> з файлами
<literal>/etc/termcap</literal> та <literal>libtermcap.so.2</literal>.
Встановіть цей пакунок, якщо програма
виходить з ладу з повідомленням „Не можу
завантажити бібліотеку libtermcap.so.2“ або
повідомляє про відсутній файл
<literal>/etc/termcap</literal>.
</para>
</section>

<section id="accelx"><title>Чому я не можу встановити AccelX?</title>
<para>
AccelX вимагає бібліотеки <literal>termcap</literal> для
роботи.  Див.  <xref linkend="termcap"/> вище.
</para>
</section>

<section id="motifnls"><title>Чому мої старі XFree 2.1 Motif-додатки призводять до збоїв у роботі?</title>
<para>
Вам потрібно встановити пакунок <systemitem
role="package">motifnls</systemitem>, котрий містить
конфігураційні файли XFree-2.1, що дозволяють
програмам Motif, скомпільованим для XFree-2.1
запускатись під XFree-3.1.
</para>
<para>
Без цих файлів деякі Motif-додатки,
скомпільовані на інших машинах (як
наприклад Netscape), можуть призводити до
аварії при спробах копіювання чи вставки у
текстове поле та спричиняти ряд інших
проблем.
</para>
</section>

</chapter>

