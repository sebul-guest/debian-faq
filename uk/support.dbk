<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="support"><title>Отримання підтримки для Debian GNU/Linux</title>
<section id="debiandocs"><title>Яка ще документація існує для системи Debian?</title>
<itemizedlist>
<listitem>
<para>
Інструкції по встановленню для поточного
випуску: перегляньте <ulink
url="http://www.debian.org/releases/stable/installmanual">http://www.debian.org/releases/stable/installmanual</ulink>.
</para>
</listitem>
<listitem>
<para>
Довідник Debian GNU/Linux торкається багатьох
аспектів системного адміністрування за
допомогою прикладів команд оболонки.
Базові навчальні програми, підказки та
інша інформація з багатьох різноманітних
тем від системного адміністрування до
програмування постачається у пакунку
<systemitem role="package">debian-reference</systemitem>, також
доступна за адресою <ulink
url="http://www.debian.org/doc/user-manuals#quick-reference">http://www.debian.org/doc/user-manuals#quick-reference</ulink>.
</para>
</listitem>
<listitem>
<para>
Підручник політики (Policy manual) документує
стратегічні вимоги збірки, як наприклад
структуру та вміст архіву Debian; деякі
проблеми дизайну операційної системи і т.п.
Він також включає технічні вимоги до
пакунку, котрих потрібно дотримуватись для
включення його в збірку, та документує
основні технічні аспекти двійкових та
джерельних пакунків Debian.
</para>
<para>
Його ви можете отримати з пакунку <systemitem
role="package">debian-policy</systemitem> або за адресою <ulink
url="http://www.debian.org/doc/devel-manuals#policy">http://www.debian.org/doc/devel-manuals#policy</ulink>.
</para>
</listitem>
<listitem>
<para>
Документація розроблена в рамках Проекту
документування Debian (Debian Documentation Project).  Вона
доступна за адресою <ulink
url="http://www.debian.org/doc/">http://www.debian.org/doc/</ulink> та
включає посібники користувача,
адміністратора та убезпечення операційної
системи Debian GNU/Linux.
</para>
</listitem>
<listitem>
<para>
Документація на встановлені пакунки Debian:
переважна більшість пакунків мають файли
документації, що встановлюються до
<literal>/usr/doc/ПАКУНОК</literal>.
</para>
</listitem>
<listitem>
<para>
Проект документування Linux: пакунок <systemitem
role="package">doc-linux</systemitem> встановлює всі
найсвіжіші версії документів HOWTOs та mini-HOWTOs
від <ulink url="http://www.tldp.org/">Linux Documentation Project</ulink>.
</para>
</listitem>
<listitem>
<para>
Сторінки підручника у стилі Unix: Переважна
більшість команд мають сторінки довідки в
стилі оригінальних man-файлів Unix.  Вони
поділені на секції у каталозі man: наприклад,
foo(3) вказує на сторінку підручника, котра
знаходиться у /usr/share/man/man3/, і може бути
викликана за допомогою команди: <literal>man 3
foo</literal>, або просто <literal>man foo</literal>, якщо
третя секція є першою, що містить сторінку
<literal>foo</literal>.
</para>
<para>
Які з підтек <literal>/usr/share/man/</literal> містять
задану сторінку можна дізнатись, виконавши
команду <literal>man -w foo</literal>.
</para>
<para>
Новачки Debian повинні пам'ятати, що сторінки
підручника багатьох загальносистемних
команд будуть недоступними, аж поки вони не
встановлять наступні пакунки:
</para>
<itemizedlist>
<listitem>
<para>
<literal>man-db</literal>, котрий містить саму
програму <literal>man</literal>, та інші програми для
маніпуляцій сторінками підручника;
</para>
</listitem>
<listitem>
<para>
<literal>manpages</literal>, що містить системні
сторінки підручника (див.  <xref linkend="nonenglish"/>).
</para>
</listitem>
</itemizedlist>
</listitem>
<listitem>
<para>
Інформаційні сторінки у стилі GNU:
Користувацька документація для багатьох
команд, особливо інструментів GNU доступна
не лише у man-сторінках, але й у файлах info,
котрі можуть бути прочитані інструментом
GNU <literal>info</literal>, запуском <literal>M-x info</literal> у
середовищі GNU Emacs, чи будь-яким іншим
переглядачем сторінок Info.
</para>
<para>
Їхньою головною перевагою над сторінками
підручника є те, що вони базуються на
гіпертекстовій системі.  Проте вони
<emphasis>не</emphasis> потребують тенет; сторінки
<literal>info</literal> можуть бути прочитані з
текстової консолі.  Ця система була
спроектована Річардом Столлменом та
передувала WWW.
</para>
</listitem>
</itemizedlist>
<para>
Зауважте, що ви можете знайти чимало
документації у своїй системі за допомогою
веб-переглядача та команд dwww, dhelp або doccentral
з відповідних пакунків.
</para>
</section>

<section id="onlineresources"><title>Чи є якісь мережеві ресурси для обговорення Debian?</title>
<para>
Так.  Фактично, основним методом підтримки,
що надає Debian своїм користувачам є
підтримка за допомогою електронної пошти.
</para>
<section id="s11.2.1"><title>Списки розсилки</title>
<para>
Існує велика кількість <ulink
url="http://www.debian.org/MailingLists/">пов'язаних з Debian
списків розсилки</ulink>.
</para>
<para>
В системі з встановленим пакунком <systemitem
role="package">doc-debian</systemitem> повний їх список
можна знайти у файлі
<filename>/usr/share/doc/debian/mailing-lists.txt</filename>.
</para>
<para>
Списки розсилки Debian отримують назви за
таким зразком:
debian-<replaceable>тема-списку</replaceable>.  Наприклад,
debian-announce, debian-user, debian-news.  Щоб підписатись до
будь-якого з них, відправте листа за
адресою
debian-<replaceable>тема-списку</replaceable>-request@lists.debian.org
з словом subscribe у полі Subject заголовку листа.
Не забувайте додавати <emphasis>-request</emphasis> до
адреси електронної пошти, якщо ви
використовуєте такий метод щоб
підписатись чи відписатись.  В іншому
випадку ваш лист потрапить до власне
розсилки, що може роздратувати, або ж
розгубити вас, в залежності від вашої точки
зору.
</para>
<para>
Якщо у вас є переглядач Тенет, що підтримує
форми, ви можете з їх допомогою <ulink
url="http://www.debian.org/MailingLists/subscribe">підписатись</ulink>
на списки розсилки або <ulink
url="http://www.debian.org/MailingLists/unsubscribe">відписатись</ulink>
від них.
</para>
<para>
Список керівників e-mail-адреса —
<email>listmaster@lists.debian.org</email>, сюди можна
звертатись у випадку виявлення будь-яких
проблем.
</para>
<para>
Списки розсилки є публічними форумами.  Всі
електронні листи, відправлені до списків,
копіюються до загальнодоступних архівів,
щоб будь-хто (навіть люди не підписані на
них) міг переглянути їх або здійснити пошук
по їх вмісту.  Будь ласка, впевніться що ви
не відсилаєте жодних конфіденційних чи
неліцензійних матеріалів до списку
розсилки.  Це стосується і таких речей, як,
наприклад, електронні адреси.  Зверніть
особливу увагу і на той факт, що електронні
адреси, з яких ви відсилаєте листи до наших
списків розсилки, можуть стати відомими
спамерам та використані ними для
зловживань.  Перегляньте <ulink
url="http://www.debian.org/MailingLists/#disclaimer">політику щодо
приватності в списках розсилки Debian</ulink> щоб
отримати більше інформації.
</para>
<para>
Архіви списків розсилки Debian доступні через
WWW за адресою <ulink
url="http://lists.debian.org/">http://lists.debian.org/</ulink>.
</para>
<section id="mailinglistconduct"><title>Як поводитись у списках розсилки?</title>
<para>
Спілкуючись у списках розсилки Debian,
дотримуйтесь, будь ласка, наступних правил:
</para>
<itemizedlist>
<listitem>
<para>
Не надсилайте спаму.  Перегляньте <ulink
url="http://www.debian.org/MailingLists/#ads">Політику Debian щодо
реклами в списках розсилки</ulink>.
</para>
</listitem>
<listitem>
<para>
Не флейміть — це неввічливо.  Всі
розробники Debian — добровольці, що
жертвують проекту свій час, енергію та
кошти, стараючись спільно принести
користь.
</para>
</listitem>
<listitem>
<para>
Не використовуйте нецензурну лексику;
окрім усього іншого деякі люди отримують
листи за допомогою пакетного радіо, де
лайливі вислови є неприпустимими.
</para>
</listitem>
<listitem>
<para>
Переконайтесь, що ви вказуєте правильну
назву списку.  <emphasis>Ніколи</emphasis> не
відправляйте ваші запити про підписку на
список розсилки або відмову під подальшої
участі в ньому безпосередньо у
списки<footnote><para> Для цього необхідно
вказувати адресу
debian-<replaceable>назва-списку</replaceable>-REQUEST@lists.debian.org.
</para> </footnote>.
</para>
</listitem>
<listitem>
<para>
Див.  <xref linkend="bugreport"/> щоб дізнатись про
відсилання повідомлень про помилки.
</para>
</listitem>
</itemizedlist>
</section>

</section>

<section id="s11.2.2"><title>Супроводжуючі</title>
<para>
Користувачі можуть адресувати свої
запитання до конкретних супроводжуючих
пакунків за допомогою електронної пошти.
Щоб зв'язатись з супроводжуючим пакунка xyz,
відішліть листа за адресою
<emphasis>xyz@packages.debian.org</emphasis>.
</para>
</section>

<section id="s11.2.3"><title>Групи новин</title>
<para>
Користувачі мають публікувати не
специфічні для Debian запитання до однієї з
груп новин, присвячених Linux, котрі
називаються comp.os.linux.* та linux.*.  Також є
кілька груп новин у тенетах, наприклад на
сайтах <ulink url="http://www.linux.org/docs/usenet.html">Linux
Online</ulink> та <ulink
url="http://www.linuxjournal.com/helpdesk.php">LinuxJournal</ulink>.
</para>
</section>

</section>

<section id="searchtools"><title>Чи є якийсь швидкий спосіб пошуку інформації про Debian GNU/Linux?</title>
<para>
Є набір пошукових систем, котрі
обслуговують документацію, присвячену Debian:
</para>
<itemizedlist>
<listitem>
<para>
<ulink url="http://search.debian.org/">Пошуковий веб-сайт
Debian</ulink>.
</para>
</listitem>
<listitem>
<para>
<ulink url="http://groups.google.com/">Google Groups</ulink>: пошукова
система для груп новин.
</para>
<para>
Наприклад, щоб дізнатись, про успіхи людей
у пошуку драйверів під Debian для контролерів
Promise, спробуйте здійснити пошук по фразі
<literal>Promise Linux driver</literal>.  Вам буде показано
всі повідомлення, що містять цей рядок,
тобто все, що люди обговорювали на цю тему.
Якщо до цього ланцюжка додати слово
<literal>Debian</literal>, ви побачите лише ті
повідомлення, що стосуються саме Debian.
</para>
</listitem>
<listitem>
<para>
Чим краще ви задаєте ключову фразу, тим
точніші результати видають стандартні
пошукові системи, як наприклад <ulink
url="http://www.altavista.com/">AltaVista</ulink> чи <ulink
url="http://www.google.com/">Google</ulink>.
</para>
<para>
Наприклад, пошук по ланцюжку cgi-perl дасть
значно детальніші пояснення про пакунок,
аніж коротке поле опису у його файлі control.
</para>
</listitem>
</itemizedlist>
</section>

<section id="buglogs"><title>Чи ведуться записи про відомі помилки?</title>
<para>
Звіти про невирішені (та закриті) питання є
загальнодоступними: Debian пообіцяв робити
так, заявляючи, що „Ми не будемо
приховувати проблеми“ в <ulink
url="http://www.debian.org/social_contract">Суспільному
договорі Debian</ulink>.
</para>
<para>
Збірка Debian GNU/Linux має систему
відслідковування помилок (bug tracking system — BTS),
котра зберігає деталі звітів про помилки,
надісланих користувачами та розробниками.
Кожній помилці присвоюється число і звіт
про неї зберігається до тих пір, поки
проблема не буде вирішеною.
</para>
<para>
Копія цієї інформації доступна за адресою
<ulink url="http://www.debian.org/Bugs/">http://www.debian.org/Bugs/</ulink>.
</para>
<para>
Поштовий сервер надає доступ до бази даних
системи відслідковування помилок за
допомогою електронної пошти.  Щоб отримати
детальніші інструкції, відправте листа на
адресу request@bugs.debian.org зі словом help.
</para>
</section>

<section id="bugreport"><title>Як мені відправити в Debian звіт про помилку?</title>
<para>
Якщо ви знайшли помилку, прочитайте, будь
ласка, інструкції щодо звітування про
помилки в Debian.  Ці інструкції можна
отримати кількома шляхами:
</para>
<itemizedlist>
<listitem>
<para>
Через анонімний FTP.  Дзеркальні сайти Debian
містять інструкції у файлі
<literal>doc/bug-reporting.txt</literal>.
</para>
</listitem>
<listitem>
<para>
З тенет.  Копія інструкцій доступна за
адресою <ulink
url="http://www.debian.org/Bugs/Reporting">http://www.debian.org/Bugs/Reporting</ulink>.
</para>
</listitem>
<listitem>
<para>
На будь-якій системі Debian зі встановленим
пакунком <systemitem role="package">doc-debian</systemitem>.
Інструкції знаходяться у файлі
<filename>/usr/doc/debian/bug-reporting.txt</filename>.
</para>
</listitem>
</itemizedlist>
<para>
Ви також можете відіслати звіт про помилку
за допомогою пакунку <systemitem
role="package">reportbug</systemitem>, котрий проведе вас
через процес створення звіту та надішле
листа електронною поштою за правильною
адресою, автоматично додавши деякі
відомості про вашу систему.  Він також
покаже вам список помилок, знайдених у
пакунку, про котрий ви звітуєте з тим щоб ви
могли надати додаткову інформацію до
існуючих звітів.
</para>
<para>
Якщо ви хочете відіслати звіт електронною
поштою, надсилайте його на
<email>submit@bugs.debian.org</email>.  Перший рядок звіту
має бути подібний до
</para>
<screen>
Package: package-name
</screen>
<para>
(замініть <replaceable>package-name</replaceable> назвою
пакунка).  Наступний рядок повинен подібним
чином вказувати на версію програми:
</para>
<screen>
Version: version-number
</screen>
<para>
Номер версії будь-якого встановленого у
вашій системі пакунку можна дізнатись за
допомогою команди
</para>
<screen>
dpkg -s <replaceable>package-name</replaceable>
</screen>
<para>
Ця частина відноситься до так званого
псевдо-заголовку.  Решта повідомлення
повинна містити опис помилки (робіть його
помірковано детальним, будь ласка), версію
вашої збірки Debian та версії інших пакунків,
що мають відношення до помилки.  Версію Debian
можна дізнатись за допомогою команди
</para>
<screen>
cat /etc/debian_version
</screen>
<para>
Ви маєте отримати автоматичне
підтвердження про отримання вашого звіту.
Йому також буде автоматично надано номер,
додано до журналу помилок та
перенаправлено до списку розсилки
debian-bugs-dist.
</para>
</section>

</chapter>

